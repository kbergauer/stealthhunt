﻿using UnityEngine;
using System.Collections;

public class HingeDoor : MonoBehaviour 
{
	public float ClosingDelay = 1.0f;
	public float ClosingSpeed = 0.5f;

	private Quaternion defaultRotation;
	private Collider2D lastCollider = null;

	void Start()
	{
		defaultRotation = transform.rotation;
	}

	void Update()
	{
		if(lastCollider == null)
		{
			if(transform.rotation != defaultRotation)
				StartCoroutine(CloseAfterDelay());
		}
	}

	void OnCollisionEnter2D(Collision2D _Other)
	{
		StopAllCoroutines();
		lastCollider = _Other.collider;
	}

	void OnCollisionExit2D(Collision2D _Other)
	{
		StartCoroutine(CloseAfterDelay());
	}

	private IEnumerator CloseAfterDelay()
	{
		yield return new WaitForSeconds(ClosingDelay);

		Quaternion startingRotation = transform.rotation;
		float startTime = GameManager.time;
		float intervall = 0.0f;
		while(intervall < 0.99f)
		{
			intervall = GameManager.time - startTime;
			intervall *= ClosingSpeed;
			transform.rotation = Quaternion.Lerp(startingRotation, defaultRotation, intervall);

			yield return new WaitForFixedUpdate();
		}

		transform.rotation = defaultRotation;
	}
}