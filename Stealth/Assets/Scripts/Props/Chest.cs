﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chest : MonoBehaviour, IInteractable
{
	public float[] Probabilities = new float[] { 0.6f, 0.4f, 0.2f };
	public AudioClip SearchClip;
	public AudioClip OpenClip;

	private Animator anim;
	private bool isClosed = true;
	private IUsable contents = null;
	private bool beingSearched = false;

	private int typeCount = 0;
	private float totalProbability = 0.0f;

	private Player searcher = null;
	private List<Player> playersInContact = new List<Player>();

	private AudioSource searchAudioSource = null;

	private enum EContent
	{
		Shuriken,
		Caltrops,
		Rag
	};

	public GameObject Reference
	{
		get { return gameObject; }
	}

	public float Duration
	{
		get { return 2.5f; }
	}
	public float Downtime 
	{
		get { return Random.Range(15.0f, 30.0f); }
	}
	public bool CanInteract 
	{
		get { return contents != null || (isClosed && !beingSearched); }
	}

	void Start()
	{
		anim = GetComponent<Animator>();
		typeCount = System.Enum.GetNames(typeof(EContent)).Length;
		for(int i = 0; i < typeCount; i++)
			totalProbability += Probabilities[i];
	}

	void OnCollisionEnter2D(Collision2D _Collision)
	{
		if(!_Collision.gameObject.CompareTag("Player"))
			return;

		Player player = _Collision.gameObject.GetComponent<Player>();
		if(player != null)
		{
			if(contents != null)
			{
				player.OnEquipUtility += RemoveContents;
				player.OfferUtility(contents);
			}
			else
				player.Interactable = this;
			
			playersInContact.Add(player);
		}
	}

	void OnCollisionExit2D(Collision2D _Collision)
	{
		if(!_Collision.gameObject.CompareTag("Player"))
			return;

		Player player = _Collision.gameObject.GetComponent<Player>();
		if(player != null)
		{
			player.Interactable = null;
			player.OnEquipUtility -= RemoveContents;

			playersInContact.Remove(player);
		}
	}

	public bool Prepare(Player _Player)
	{
		if(contents != null)
			return false;

		searcher = _Player;
		beingSearched = true;

		// Start search sound
		if(searchAudioSource == null)
		{
			searchAudioSource = SoundUtility.LoopAt(SearchClip, transform.position);
			if(searchAudioSource != null)
				searchAudioSource.transform.SetParent(transform);
		}
		else
		{
			searchAudioSource.Play();
		}

		return true;
	}

	public void Abort(Player _Player)
	{
		if(searcher == _Player)
		{
			beingSearched = false;

			if(searchAudioSource != null)
				searchAudioSource.Stop();
		}
	}

	public void Finish(Player _Player)
	{
		if(_Player != searcher)
			return;

		int index = 0;

		// Choose item randomly
		float chosen = Random.Range(0.0f, totalProbability);
		float currentProbability = 0.0f;
		for(int i = 0; i < typeCount; i++)
		{
			currentProbability += Probabilities[i];
			if(chosen <= currentProbability)
			{
				index = i;
				break;
			}
		}

		// Create item
		contents = null;
		switch((EContent)index)
		{
			case EContent.Shuriken:
				{
					contents = new Shuriken();
				} break;

			case EContent.Caltrops:
				{
					contents = new Caltrops();
				} break;

			case EContent.Rag:
				{
					contents = new Rag();
				} break;
		}

		// Give item to player
		_Player.OnEquipUtility += RemoveContents;
		_Player.OfferUtility(contents);
		isClosed = false;

		// Set flag
		beingSearched = false;

		// Play animation
		anim.SetBool("IsOpen", true);

		// Stop search sound and play open sound
		if(searchAudioSource != null)
			searchAudioSource.Stop();
		SoundUtility.PlayAt(OpenClip, transform.position);

		// Start closing coroutine
		StartCoroutine(Close());
	}

	private IEnumerator Close()
	{
		// Wait for refill
		yield return new WaitForSeconds(Downtime);

		// Reset animation and flag
		anim.SetBool("IsOpen", false);
		isClosed = true;

		RemoveContents();
	}

	private void RemoveContents()
	{
		contents = null;

		for(int i = 0; i < playersInContact.Count; i++)
		{
			if(playersInContact[i] != null && playersInContact[i].HUD != null)
				playersInContact[i].HUD.HidePickup(false);
		}
	}
}