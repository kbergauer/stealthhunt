﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlidingDoor : MonoBehaviour 
{
	public float CloseDelay = 1.0f;

	private Animator anim;
	private List<Collider2D> inRange;

	void Awake()
	{
		anim = GetComponent<Animator>();
		inRange = new List<Collider2D>();
	}

	void OnTriggerEnter2D(Collider2D _Other)
	{
		if(!_Other.CompareTag("Player"))
			return;

		inRange.Add(_Other);
		Open();
	}

	void OnTriggerExit2D(Collider2D _Other)
	{
		if(!_Other.CompareTag("Player"))
			return;

		inRange.Remove(_Other);
		if(inRange.Count == 0)
			StartCoroutine(DelayedClose());
	}

	private void Open()
	{
		StopAllCoroutines();
		anim.SetBool("IsOpen", true);
	}

	private void Close()
	{
		anim.SetBool("IsOpen", false);
	}

	private IEnumerator DelayedClose()
	{
		yield return new WaitForSeconds(CloseDelay);
		Close();
	}
}