﻿using UnityEngine;
using System.Collections;

public interface IKillable 
{
	bool IsInvincible { get; }
	void Kill(GameObject _Killer, Vector3 _Direction);
}