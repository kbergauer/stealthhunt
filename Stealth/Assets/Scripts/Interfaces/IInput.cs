﻿using UnityEngine;
using System.Collections;

public interface IInput 
{
	PlayerInput.EDevice Device { get; set; }

	float GetAxis(string _Axis);
	float GetAxisRaw(string _Axis);

	bool GetButton(string _Button);
	bool GetButtonDown(string _Button);
	bool GetButtonUp(string _Button);

	Vector2 GetMousePosition();

	void LockAxes(params string[] _Axes);
	void UnlockAxes(params string[] _Axes);

	void LockAllAxes();
	void UnlockAllAxes();

	void Lock();
	void Unlock();
}
