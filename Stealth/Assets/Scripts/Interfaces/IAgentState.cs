﻿using UnityEngine;
using System.Collections;

public interface IAgentState 
{
	/// <summary>
	/// Reference to the state's agent.
	/// </summary>
	AIAgent Agent { get; }

	/// <summary>
	/// String containing the state name.
	/// </summary>
	string Name { get; }

	/// <summary>
	/// String containing general information about the state.
	/// </summary>
	string Info { get; }

	/// <summary>
	/// Called once when entering the state.
	/// </summary>
	IAgentState Enter();

	/// <summary>
	/// Called by the associated agent to update the state.
	/// </summary>
	/// <returns>The state to transition to; <c>null</c> if no transition is possible</returns>
	IAgentState Update();

	/// <summary>
	/// Called once when exiting the state.
	/// </summary>
	void Exit();
}