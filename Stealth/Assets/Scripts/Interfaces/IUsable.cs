﻿using UnityEngine;
using System.Collections;

public interface IUsable 
{
	int Uses { get; }
	int UsesLeft { get; }
	Sprite Icon { get; }

	bool CanUse(Player _User);
	void Use(Player _User);
}