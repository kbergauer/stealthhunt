﻿using UnityEngine;
using System.Collections;

public interface IReferencable 
{
	GameObject Reference { get; }
}