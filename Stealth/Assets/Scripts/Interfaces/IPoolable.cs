﻿using UnityEngine;
using System.Collections;

public interface IPoolable : IReferencable
{
	void Activate();
	void Deactivate();
}