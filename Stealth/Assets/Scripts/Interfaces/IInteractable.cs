﻿using UnityEngine;
using System.Collections;

public interface IInteractable : IReferencable
{
	float Duration { get; }
	float Downtime { get; }
	bool CanInteract { get; }

	bool Prepare(Player _Player);
	void Abort(Player _Player);
	void Finish(Player _Player);
}