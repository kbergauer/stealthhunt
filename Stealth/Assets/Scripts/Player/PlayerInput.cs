﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInput : MonoBehaviour, IInput
{
	#region PUBLIC VARIABLES

	public enum EDevice
	{
		Any,
		Keyboard,
		Controller1,
		Controller2,
		Controller3,
		Controller4
	};

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private static PlayerInput instance;

	private EDevice device = EDevice.Keyboard;

	private string[] devicePrefix = new string[] { "", "KB_", "C1_", "C2_", "C3_", "C4_" };
	private bool ignoreAxes = false;
	private bool ignoreButtons = false;

	private List<string> lockedAxes = new List<string>();

	#endregion

	#region PROPERTIES

	public static PlayerInput This
	{
		get 
		{ 
			if(instance == null) 
				instance = FindObjectOfType(typeof(PlayerInput)) as PlayerInput;

			return instance; 
		}
	}

	public EDevice Device
	{
		get { return device; }
		set { device = value; }
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public void Lock()
	{
		LockAllAxes();
		LockAllButtons();

		//Debug.Log(this.name + "::CPlayerInput - Locked.");
	}

	public void Unlock()
	{
		UnlockAllAxes();
		UnlockAllButtons();

		//Debug.Log(this.name + "::CPlayerInput - Unlocked.");
	}

	public void LockAxes(params string[] _Axes)
	{
		for(int i = 0; i < _Axes.Length; i++)
		{
			if(!lockedAxes.Contains(devicePrefix[(int)Device] + _Axes[i]))
				lockedAxes.Add(devicePrefix[(int)Device] + _Axes[i]);
		}
	}

	public void LockAllAxes()
	{
		ignoreAxes = true;
	}

	public void UnlockAxes(params string[] _Axes)
	{
		for(int i = 0; i < _Axes.Length; i++)
		{
			if(lockedAxes.Contains(devicePrefix[(int)Device] + _Axes[i]))
				lockedAxes.Remove(devicePrefix[(int)Device] + _Axes[i]);
		}
	}

	public void UnlockAllAxes()
	{
		ignoreAxes = false;
		lockedAxes.Clear();
	}

	public void LockAllButtons()
	{
		ignoreButtons = true;
	}

	public void UnlockAllButtons()
	{
		ignoreButtons = false;
	}

	// TODO: Return mouse axis for keyboard control?
	public float GetAxis(string _Axis)
	{
		float axis = 0.0f;

		if(!ignoreAxes && !lockedAxes.Contains(devicePrefix[(int)Device] + _Axis))
			axis = Input.GetAxis(devicePrefix[(int)Device] + _Axis);

		return axis;
	}

	// TODO: Return mouse axis for keyboard control?
	public float GetAxisRaw(string _Axis)
	{
		float axis = 0.0f;

		if(!ignoreAxes && !lockedAxes.Contains(devicePrefix[(int)Device] + _Axis))
			axis = Input.GetAxisRaw(devicePrefix[(int)Device] + _Axis);

		return axis;
	}

	public bool GetButton(string _Button)
	{
		return (ignoreButtons ? false : Input.GetButton(devicePrefix[(int)Device] + _Button));
	}

	public bool GetButtonDown(string _Button)
	{
		return (ignoreButtons ? false : Input.GetButtonDown(devicePrefix[(int)Device] + _Button));
	}

	public bool GetButtonUp(string _Button)
	{
		return (ignoreButtons ? false : Input.GetButtonUp(devicePrefix[(int)Device] + _Button));
	}

	public Vector2 GetMousePosition()
	{
		if(!ignoreAxes && !lockedAxes.Contains(devicePrefix[(int)Device] + "Vertical2") && !lockedAxes.Contains(devicePrefix[(int)Device] + "Horizontal2"))
			return Input.mousePosition;

		return Vector2.zero;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}
