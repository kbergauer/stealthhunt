﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	public PlayerController Target = null;
	public float MaximumDistance = 5.0f;

	private PlayerInput input;
	private Vector3 mouse = Vector3.zero;
	private Vector3 forward = Vector3.up;
	private Vector3 position = Vector3.zero;
	private float distance = 0.0f;

	void Start()
	{
		input = Target.GetComponent<PlayerInput>();
	}

	void Update()
	{
		if(Target == null)
			return;

		// Get looking direction and distance
		if(input.Device == PlayerInput.EDevice.Keyboard)
		{
			mouse = Camera.main.ScreenToWorldPoint(input.GetMousePosition());
			forward = mouse - Target.transform.position;
			distance = Mathf.Clamp(forward.magnitude, 0.0f, MaximumDistance);
		}
		else
		{
			forward.x = input.GetAxisRaw("Horizontal2");
			forward.y = input.GetAxisRaw("Vertical2");

			distance = forward.magnitude * MaximumDistance;
		}
		
		// Normalize direction
		forward.Normalize();

		// Calculate position
		position = Target.transform.position + forward * distance;
		position.z = transform.position.z;

		// Set position
		transform.position = position;
	}

	public void SetViewport(Rect _Viewport)
	{
		Camera[] cams = GetComponentsInChildren<Camera>();
		for(int i = 0; i < cams.Length; i++)
		{
			cams[i].rect = _Viewport;
		}
	}
}