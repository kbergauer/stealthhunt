﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	[Header("Movement")]
	public Camera BaseCamera = null;
	public float MovementSpeed = 5.0f;

	[Header("Combat")]
	public float AttackCooldown = 1.0f;
	public Collider2D WeaponCollider;
	public AudioClip[] AttackSounds;

	// References
	private Animator anim;
	private Rigidbody2D rbody;
	private IInput input;
	private Player player;
	private Collider2D[] players;

	private Vector2 velocity = Vector2.zero;
	private Vector2 lookDirection = Vector2.zero;
	private Vector3 mouse;

	private float currentMovementSpeed = 5.0f;

	private bool canAttack = true;
	private bool isAttacking = false;

	private bool fixedRotation = false;



	public bool CanAttack
	{
		get { return canAttack; }
	}



	void Start()
	{
		anim = GetComponent<Animator>();
		rbody = GetComponent<Rigidbody2D>();
		input = gameObject.GetInterface<IInput>();
		player = GetComponent<Player>();

		GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
		players = new Collider2D[objects.Length];
		for(int i = 0; i < objects.Length; i++)
		{
			players[i] = objects[i].GetComponent<Collider2D>();
			//Debug.Log(objects[i].name + ": " + players[i]);
		}

		currentMovementSpeed = MovementSpeed;
	}

	void OnEnable()
	{
		canAttack = true;
		isAttacking = false;
		fixedRotation = false;
		currentMovementSpeed = MovementSpeed;
	}

	void Update()
	{
		if(canAttack && input.GetButtonDown("Action1"))
			isAttacking = true;

		if(input.GetButtonDown("Action2"))
			player.UseUtility();

		if(input.GetAxisRaw("Action3") < -0.1f)
			player.Interact();

		if(input.GetButtonDown("Select"))
		{
			if(player.HUD != null)
				player.HUD.ShowScoreTable(true);
		}
		else if(input.GetButtonUp("Select"))
		{
			if(player.HUD != null)
				player.HUD.ShowScoreTable(false);
		}
	}

	void FixedUpdate()
	{
		if(rbody != null)
		{
			// Get raw movement input
			velocity.x = input.GetAxisRaw("Horizontal");
			velocity.y = input.GetAxisRaw("Vertical");

			// Move player according to input
			rbody.velocity = velocity.normalized * currentMovementSpeed;
		}

		anim.SetFloat("Speed", velocity.magnitude);

		// Rotate player towards mouse/by stick input
		if(!fixedRotation)
		{
			Vector3 target = Vector3.zero;
			if(input.Device == PlayerInput.EDevice.Keyboard)
			{
				if(BaseCamera != null)
					target = BaseCamera.ScreenToWorldPoint(input.GetMousePosition());
			}
			else
			{
				Vector2 newInput = Vector2.zero;
				newInput.x = input.GetAxisRaw("Horizontal2");
				newInput.y = input.GetAxisRaw("Vertical2");
				newInput.Normalize();

				if(newInput.x != 0.0f || newInput.y != 0.0f)
				{
					lookDirection = newInput;
				}

				target = transform.position + new Vector3(lookDirection.x, lookDirection.y, 0.0f) * 5.0f;
			}
			float angle = Vector2.Angle(Vector3.up, target - transform.position);
			if(target.x > transform.position.x)
				angle = -angle;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}

		// Execute an attack
		if(isAttacking)
		{
			anim.SetTrigger("Attack");
			ExecuteAttack();
			StartCoroutine(DisableAttack());
		}
		isAttacking = false;
	}

	private IEnumerator DisableAttack()
	{
		canAttack = false;
		yield return new WaitForSeconds(AttackCooldown);
		canAttack = true;
	}

	public void ExecuteAttack()
	{
		// Check if any player got hit
		for(int i = 0; i < players.Length; i++)
		{
			// Ignore self
			if(players[i] == null || players[i].gameObject == gameObject)
				continue;

			if(WeaponCollider.IsTouching(players[i]))
			{
				IKillable killable = players[i].gameObject.GetInterface<IKillable>();
				if(killable != null && !killable.IsInvincible)
					killable.Kill(gameObject, players[i].transform.position - transform.position);
			}
		}

		// Play sounds
		for(int i = 0; i < AttackSounds.Length; i++)
		{
			SoundUtility.PlayAt(AttackSounds[i], transform.position);
		}
	}

	public void SetMovementSpeed(float _Speed)
	{
		currentMovementSpeed = Mathf.Max(0.0f, _Speed);
	}

	public float GetMovementSpeed()
	{
		return currentMovementSpeed;
	}

	public void LookAt(Vector3 _Position)
	{
		fixedRotation = true;

		float angle = Vector2.Angle(Vector3.up, _Position - transform.position);
		if(_Position.x > transform.position.x)
			angle = -angle;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}

	public void FreeLook()
	{
		fixedRotation = false;
	}

	public Quaternion GetLookDirection()
	{
		Vector3 direction = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * Vector3.up;
		Vector3 position = transform.position + direction * 2.0f;
		float angle = Vector2.Angle(Vector3.up, position - transform.position);
		if(position.x > transform.position.x)
			angle = -angle;
		
		return Quaternion.AngleAxis(angle, Vector3.forward);
	}
}