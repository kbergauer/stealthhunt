﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour, IKillable
{
	public delegate void EventHandler();
	public event EventHandler OnLeftStep;
	public event EventHandler OnRightStep;
	public event EventHandler OnEquipUtility;
	public event EventHandler OnInteractionNear;

	[Header("Visuals")]
	public Color PersonalColor = Color.gray;
	public GameObject Head;
	public SpriteRenderer HeadRenderer;

	[Header("Sounds")]
	public AudioClip[] DeathSounds;

	private Animator anim;

	public PlayerController CONTROLLER
	{
		get;
		private set;
	}
	public IInput INPUT
	{ 
		get;
		private set;
	}
	public PlayerHUD HUD
	{
		get;
		private set;
	}
	public CameraController CAM
	{
		get;
		private set;
	}

	public int MovementAdjusters
	{
		get;
		private set;
	}

	private IUsable offeredPickup = null;
	private IUsable equippedUsable = null;
	public IUsable Usable
	{
		get 
		{ 
			return equippedUsable; 
		}
		set
		{
			equippedUsable = value;
			if(OnEquipUtility != null)
				OnEquipUtility();
		}
	}

	private IInteractable currentInteractable;
	public IInteractable Interactable
	{
		get
		{
			return currentInteractable;
		}
		set
		{
			currentInteractable = value;
			if(OnInteractionNear != null)
				OnInteractionNear();
		}
	}

	public bool IsInteracting
	{
		get;
		private set;
	}

	public float InteractionProgress
	{
		get;
		private set;
	}

	public bool IsInvincible
	{
		get;
		private set;
	}

	void Awake()
	{
		anim = GetComponent<Animator>();
	}

	void Start()
	{
		if(HeadRenderer != null)
			HeadRenderer.color = PersonalColor;

		CONTROLLER = GetComponent<PlayerController>();
		INPUT = gameObject.GetInterface<IInput>();
		if(transform.parent != null)
		{
			HUD = transform.parent.gameObject.GetComponentInChildren<PlayerHUD>();
			CAM = transform.parent.gameObject.GetComponentInChildren<CameraController>();
		}

		OnInteractionNear += RefuseOffer;
	}

	void OnEnable()
	{
		offeredPickup = null;
		Usable = null;

		Interactable = null;
		IsInteracting = false;
		InteractionProgress = 0.0f;

		MovementAdjusters = 0;

		if(INPUT != null)
			INPUT.Unlock();

		anim.SetFloat("Speed", 0.0f);
		anim.ResetTrigger("Attack");
		anim.Play("Stand");
		
		if(HUD != null)
			HUD.ShowScoreTable(false);

		StartCoroutine(SpawnImmunity(GameManager.InvincibilityDuration));
	}

	void OnDisable()
	{
		if(HUD != null)
		{
			HUD.HideOverlay();
			HUD.HidePickup(false);
			HUD.SetInteraction(false);
		}

		BloodTrailCreator[] blood = GetComponents<BloodTrailCreator>();
		for(int i = 0; i < blood.Length; i++)
			Destroy(blood[i]);

		FootprintTrailCreator[] trail = GetComponents<FootprintTrailCreator>();
		for(int i = 0; i < trail.Length; i++)
			Destroy(trail[i]);

		offeredPickup = null;
		Usable = null;

		if(Interactable != null)
			Interactable.Abort(this);
		Interactable = null;
		IsInteracting = false;
		InteractionProgress = 0.0f;

		MovementAdjusters = 0;
		CONTROLLER.SetMovementSpeed(CONTROLLER.MovementSpeed);

		if(Head != null)
			Head.transform.localPosition = Vector3.zero;

		StopAllCoroutines();
	}

	void OnDestroy()
	{
		OnInteractionNear -= RefuseOffer;
	}

	public void Kill(GameObject _Killer, Vector3 _Direction)
	{
		// Instantiate death sprite
		if(PoolManager.Corpses.Usable)
		{
			// Instantiate random prefab
			GameObject sprite = PoolManager.Corpses.Instantiate(); //Instantiate(DeathSprites[Random.Range(0, DeathSprites.Length)]);
			sprite.transform.position = transform.position;

			// Rotate in attack direction
			float angle = Vector2.Angle(Vector3.up, _Direction);
			if((transform.position + _Direction).x > transform.position.x)
				angle = -angle;
			sprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

			// Color all necessary sprites in player color
			Transform child = null;
			SpriteRenderer childRend = null;
			for(int i = 0; i < sprite.transform.childCount; i++)
			{
				child = sprite.transform.GetChild(i);
				if(child.CompareTag("Colorable"))
				{
					childRend = child.GetComponent<SpriteRenderer>();
					if(childRend != null)
						childRend.color = new Color(PersonalColor.r, PersonalColor.g, PersonalColor.b, childRend.color.a);
				}
			}
		}
		else
		{
			Debug.LogError(this.name + ": Unable to show corpse; PoolManager.Corpses is not usable!");
		}

		// Play death sound
		SoundUtility.PlayAt(DeathSounds[Random.Range(0, DeathSounds.Length)], transform.position);

		// Register kill with game manager
		GameManager.RegisterKill(_Killer.GetComponent<Player>(), this);
		//GameManager.RequestRespawn(gameObject);

		// Disable self until respawn
		gameObject.SetActive(false);

		// Show scoreboard
		if(HUD != null)
			HUD.ShowScoreTable(true);
	}

	public void LeftStep()
	{
		if(OnLeftStep != null)
			OnLeftStep();
	}

	public void RightStep()
	{
		if(OnRightStep != null)
			OnRightStep();
	}

	public void ScaleMovementSpeed(float _Factor)
	{
		// Reset speed
		if(_Factor == 1.0f)
		{
			MovementAdjusters--;
			if(MovementAdjusters == 0)
			{
				CONTROLLER.SetMovementSpeed(CONTROLLER.MovementSpeed);
			}
		}
		// Set speed
		else
		{
			if(CONTROLLER.GetMovementSpeed() > CONTROLLER.MovementSpeed * _Factor)
			{
				CONTROLLER.SetMovementSpeed(CONTROLLER.MovementSpeed * _Factor);
			}
			MovementAdjusters++;
		}
	}

	public void OfferUtility(IUsable _Pickup)
	{
		if(Usable != null)
		{
			offeredPickup = _Pickup;
			HUD.ShowPickup(_Pickup);
		}
		else
		{
			Usable = _Pickup;
		}
	}

	private void AcceptOffer()
	{
		if(offeredPickup != null)
		{
			if(Usable != null)
				HUD.HidePickup(true);

			Usable = offeredPickup;
			offeredPickup = null;
		}
	}

	private void RefuseOffer()
	{
		if(offeredPickup != null)
		{
			offeredPickup = null;
			HUD.HidePickup(false);
		}
	}

	public void UseUtility()
	{
		// Accept offer if available
		if(offeredPickup != null)
		{
			AcceptOffer();
		}
		// Otherwise use current item if possible
		else
		{
			if(Usable == null || !Usable.CanUse(this))
				return;

			Usable.Use(this);
			if(Usable.UsesLeft > 0)
				HUD.SubtractUtility();
			else
			{
				Usable = null;
			}
		}
	}

	public void Interact()
	{
		// Abort if player is already searching
		if(IsInteracting)
			return;

		// Abort if there is no valid container nearby
		if(Interactable == null || !Interactable.CanInteract)
			return;

		// Start searching
		StartCoroutine(ExecuteInteraction());
	}

	private IEnumerator ExecuteInteraction()
	{
		if(!Interactable.Prepare(this))
			yield break;

		IsInteracting = true;
		InteractionProgress = 0.0f;

		float maxDuration = Interactable.Duration;
		float duration = 0.0f;

		// Retrieve and lock axis input
		INPUT.LockAxes("Horizontal", "Vertical", "Horizontal2", "Vertical2");

		// Lock rotation
		CONTROLLER.LookAt(Interactable.Reference.transform.position);

		while(duration < maxDuration)
		{
			// Check input
			if(INPUT.GetAxisRaw("Action3") >= -0.1f)
			{
				IsInteracting = false;
				INPUT.UnlockAxes("Horizontal", "Vertical", "Horizontal2", "Vertical2");
				CONTROLLER.FreeLook();

				if(Interactable != null)
					Interactable.Abort(this);
				yield break;
			}

			duration += GameManager.deltaTime;
			InteractionProgress = duration / maxDuration;
			//Debug.Log("Searching.. " + (InteractionProgress * 100.0f) + "%");
			yield return new WaitForEndOfFrame();
		}

		// Finish interaction
		if(Interactable != null)
			Interactable.Finish(this);
		IsInteracting = false;

		INPUT.UnlockAxes("Horizontal", "Vertical", "Horizontal2", "Vertical2");
		CONTROLLER.FreeLook();
		HUD.SetInteraction(false);
	}

	private IEnumerator SpawnImmunity(float _Duration)
	{
		IsInvincible = true;
		anim.SetBool("Flash", true);

		yield return new WaitForSeconds(_Duration);

		IsInvincible = false;
		anim.SetBool("Flash", false);
	}
}