﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CaltropsLogic : MonoBehaviour, IPoolable
{
	public float MovementFactor = 0.25f;
	public float DespawnTime = 10.0f;
	public float DespawnSpeed = 2.0f;

	private List<Player> slowedPlayers = null;

	private SpriteRenderer rend;
	private Collider2D coll;
	private Rigidbody2D rbody;

	private Collider2D userCollider = null;



	public GameObject Reference
	{
		get { return gameObject; }
	}

	public void Activate()
	{
		slowedPlayers = new List<Player>();

		gameObject.SetActive(true);
		StartCoroutine(WaitForDespawn());
	}

	public void Deactivate()
	{
		for(int i = 0; i < slowedPlayers.Count; i++)
		{
			slowedPlayers[i].ScaleMovementSpeed(1.0f);
		}
		slowedPlayers.Clear();

		StopAllCoroutines();
		gameObject.SetActive(false);

		if(userCollider != null)
		{
			Physics2D.IgnoreCollision(coll, userCollider, false);
		}
	}



	void Awake()
	{
		rend = GetComponent<SpriteRenderer>();
		coll = GetComponent<Collider2D>();
		rbody = GetComponent<Rigidbody2D>();
	}

	void OnTriggerEnter2D(Collider2D _Target)
	{
		Player player = _Target.GetComponent<Player>();
		if(player != null)
		{
			player.ScaleMovementSpeed(MovementFactor);
			slowedPlayers.Add(player);
		}
	}

	void OnTriggerExit2D(Collider2D _Target)
	{
		Player player = _Target.GetComponent<Player>();
		if(player != null)
		{
			player.ScaleMovementSpeed(1.0f);
			slowedPlayers.Remove(player);
		}
	}

	private IEnumerator WaitForDespawn()
	{
		yield return new WaitForSeconds(DespawnTime);
		StartCoroutine(Despawn());
	}

	private IEnumerator Despawn()
	{
		Color alpha = rend.color;
		while(alpha.a > 0.0f)
		{
			alpha.a -= GameManager.deltaTime * DespawnSpeed;
			rend.color = alpha;

			yield return new WaitForEndOfFrame();
		}

		Deactivate();
	}

	public void Throw(Collider2D _User)
	{
		// Ignore collision with user
		userCollider = _User;
		if(userCollider != null)
		{
			Physics2D.IgnoreCollision(coll, userCollider, true);
		}

		// Tint randomly
		if(rend != null)
		{
			float tint = Random.Range(0.3f, 1.0f);
			rend.color = new Color(tint, tint, tint, 1.0f);
		}

		// Start movement
		if(rbody != null)
		{
			rbody.velocity = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * Vector2.up * Random.Range(1.0f, 25.0f);
		}
	}
}