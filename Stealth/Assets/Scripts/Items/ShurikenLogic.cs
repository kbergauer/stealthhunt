﻿using UnityEngine;
using System.Collections;

public class ShurikenLogic : MonoBehaviour, IPoolable
{
	[Header("Shuriken")]
	public float MovementSpeed = 1.0f;
	public float RotationSpeed = 1.0f;
	public float DespawnTime = 30.0f;
	public float VanishSpeed = 1.0f;

	[Header("Bleeding")]
	public float BleedDuration = 10.0f;
	public Vector2 BleedDripIntervall = new Vector2(0.25f, 0.5f);

	private SpriteRenderer sprite;
	private Collider2D coll;
	private Rigidbody2D rbody;
	private Vector3 direction;
	private bool isMoving = true;

	private Transform poolParent = null;
	private Collider2D userCollider = null;



	public GameObject Reference
	{
		get { return gameObject; }
	}

	public void Activate()
	{
		rbody.isKinematic = false;
		coll.enabled = true;

		isMoving = false;

		transform.SetParent(poolParent);
		transform.position = Vector3.zero;

		gameObject.SetActive(true);
	}

	public void Deactivate()
	{
		if(poolParent == null)
		{
			poolParent = transform.parent;
		}

		if(userCollider != null)
		{
			Physics2D.IgnoreCollision(coll, userCollider, false);
		}
		userCollider = null;

		StopAllCoroutines();
		gameObject.SetActive(false);

		rbody.velocity = Vector3.zero;
		rbody.isKinematic = true;
		coll.enabled = false;

		Color c = sprite.color;
		c.a = 1.0f;
		sprite.color = c;
	}



	void Awake()
	{
		rbody = GetComponent<Rigidbody2D>();
		coll = GetComponent<Collider2D>();
		sprite = GetComponent<SpriteRenderer>();
	}

	void OnDisable()
	{
		Deactivate();
	}

	void FixedUpdate()
	{
		if(!isMoving)
			return;

		rbody.velocity = direction * MovementSpeed;
		transform.Rotate(Vector3.forward, -RotationSpeed);
	}

	void OnCollisionEnter2D(Collision2D _Collision)
	{
		// Start bleeding for hit player
		if(_Collision.gameObject.CompareTag("Player"))
		{
			BloodTrailCreator trail = _Collision.gameObject.AddComponent<BloodTrailCreator>();
			trail.Duration = BleedDuration;
			trail.DripIntervall = BleedDripIntervall;
		}

		// Stop movement
		isMoving = false;
		rbody.velocity = Vector2.zero;
		rbody.isKinematic = true;

		// Stop collisions
		coll.enabled = false;

		// Parent self to hit object
		transform.SetParent(_Collision.transform);
		if(_Collision.gameObject.GetComponent<HingeDoor>() != null)
			transform.position = transform.position - direction * 0.25f;

		// Despawn after specific intervall
		StartCoroutine(Despawn());
	}

	private IEnumerator Despawn()
	{
		yield return new WaitForSeconds(DespawnTime);

		Color alpha = sprite.color;
		while(alpha.a > 0.0f)
		{
			alpha.a -= GameManager.deltaTime * VanishSpeed;
			sprite.color = alpha;
			yield return new WaitForEndOfFrame();
		}

		transform.SetParent(poolParent);
		transform.position = Vector3.zero;
			
		Deactivate();
	}

	public void Throw(Collider2D _User)
	{
		userCollider = _User;
		if(userCollider != null)
		{
			Physics2D.IgnoreCollision(coll, userCollider, true);
		}

		isMoving = true;
		direction = transform.rotation * Vector3.up;
	}
}