﻿using UnityEngine;
using System.Collections;

public class RagLogic : MonoBehaviour, IPoolable
{
	public float DespawnTime = 1.0f;
	public float DespawnSpeed = 1.0f;

	public Player Self;

	private SpriteRenderer[] renderers;



	public GameObject Reference
	{
		get { return gameObject; }
	}

	public void Activate()
	{
		Color c;
		for(int i = 0; i < renderers.Length; i++)
		{
			c = renderers[i].color;
			c.a = 1.0f;
			renderers[i].color = c;
		}

		gameObject.SetActive(true);

		StartCoroutine(WaitForDespawn());
	}

	public void Deactivate()
	{
		StopAllCoroutines();
		gameObject.SetActive(false);

		Self = null;
	}

	

	void Awake()
	{
		renderers = GetComponentsInChildren<SpriteRenderer>();
	}

	void Start()
	{
		if(Self == null)
			return;

		FootprintTrailCreator trail = Self.GetComponent<FootprintTrailCreator>();
		if(trail != null && trail.Cleanable)
			Destroy(trail);
	}

	private IEnumerator WaitForDespawn()
	{
		yield return new WaitForSeconds(DespawnTime);
		StartCoroutine(Despawn());
	}

	private IEnumerator Despawn()
	{
		float alpha = 1.0f;
		Color temp;
		while(alpha > 0.0f)
		{
			alpha -= GameManager.deltaTime * DespawnSpeed;
			for(int i = 0; i < renderers.Length; i++)
			{
				temp = renderers[i].color;
				temp.a = alpha;
				renderers[i].color = temp;
			}

			yield return new WaitForEndOfFrame();
		}

		Deactivate();
	}
}