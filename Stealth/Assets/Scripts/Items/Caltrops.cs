﻿using UnityEngine;
using System.Collections;

public class Caltrops : IUsable 
{
	private int dropCount = 10;
	private float dropAngle = 30.0f;

	private int used = 0;
	private float usageTime = 0.0f;
	private float usageCooldown = 0.25f;

	private Sprite icon = null;

	public Caltrops()
	{
		icon = Resources.Load<Sprite>("Icons/Caltrops");
	}

	public int Uses
	{
		get { return 1; }
	}
	public int UsesLeft
	{
		get { return Uses - used; }
	}

	public Sprite Icon
	{
		get { return icon; }
	}

	public bool CanUse(Player _User)
	{
		return GameManager.time - usageTime >= usageCooldown && used < Uses;
	}

	public void Use(Player _User)
	{
		// Get user references
		Collider2D userColl = _User.GetComponent<Collider2D>();
		Quaternion forward = _User.CONTROLLER.GetLookDirection();

		// Scatter caltrops
		CaltropsLogic caltrop = null;
		for(int i = 0; i < dropCount; i++)
		{
			// Instantiate prefab
			caltrop = PoolManager.Caltrops.Instantiate().GetComponent<CaltropsLogic>();
			caltrop.transform.position = _User.transform.position;
			caltrop.transform.rotation = forward * Quaternion.AngleAxis(Random.Range(-(dropAngle / 2.0f), (dropAngle / 2.0f)), Vector3.forward);
			caltrop.Throw(userColl);
		}

		//Debug.Log("Dropped caltrops.");

		usageTime = GameManager.time;
		used++;
	}
}