﻿using UnityEngine;
using System.Collections;

public class Shuriken : IUsable
{
	private int used = 0;
	private float usageTime = 0.0f;
	private float usageCooldown = 0.25f;

	private Sprite icon = null;

	public Shuriken()
	{
		icon = Resources.Load<Sprite>("Icons/Shuriken");
	}

	public int Uses 
	{ 
		get { return 3; } 
	}
	public int UsesLeft
	{
		get { return Uses - used; }
	}

	public Sprite Icon
	{
		get { return icon; }
	}

	public bool CanUse(Player _User)
	{
		return GameManager.time - usageTime >= usageCooldown && used < Uses;
	}

	public void Use(Player _User)
	{
		// Instantiate shuriken
		ShurikenLogic obj = PoolManager.Shuriken.Instantiate().GetComponent<ShurikenLogic>();
		obj.transform.position = _User.transform.position;
		obj.transform.rotation = _User.transform.rotation;
		obj.Throw(_User.GetComponent<Collider2D>());

		usageTime = GameManager.time;
		used++;
	}
}