﻿using UnityEngine;
using System.Collections;

public class Rag : IUsable 
{
	private int used = 0;
	private float usageTime = 0.0f;
	private float usageCooldown = 0.25f;

	private Sprite icon = null;

	public Rag()
	{
		icon = Resources.Load<Sprite>("Icons/Rag");
	}

	public int Uses
	{
		get { return 1; }
	}
	public int UsesLeft
	{
		get { return Uses - used; }
	}

	public Sprite Icon
	{
		get { return icon; }
	}

	public bool CanUse(Player _User)
	{
		bool timeAndUse = GameManager.time - usageTime >= usageCooldown && used < Uses;
		if(timeAndUse)
		{
			FootprintTrailCreator trail = _User.GetComponent<FootprintTrailCreator>();
			return trail != null && trail.Cleanable;
		}

		return false;
	}

	public void Use(Player _User)
	{
		// Instantiate rag
		RagLogic rag = PoolManager.Rags.Instantiate().GetComponent<RagLogic>();
		rag.transform.position = _User.transform.position;
		rag.transform.rotation = Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.forward);
		rag.Self = _User;

		usageTime = GameManager.time;
		used++;
	}
}