﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleNavigation : MonoBehaviour 
{
	public enum ENavigationDirection
	{
		Up,
		Down,
		Left,
		Right
	}
	public ENavigationDirection Direction = ENavigationDirection.Down;

	public Selectable Active = null;
	public Selectable InActive = null;

	private Selectable self;
	private Navigation nav;

	void Start()
	{
		self = GetComponent<Selectable>();
		if(self == null)
			self = GetComponentInParent<Selectable>();
	}

	public void Toggle(bool _Value)
	{
		Selectable next = null;
		if(_Value)
		{
			next = Active;
		}
		else
		{
			next = InActive;
		}

		nav = self.navigation;

		switch(Direction)
		{
			case ENavigationDirection.Up:
				{
					nav.selectOnUp = next;
				} break;

			case ENavigationDirection.Down:
				{
					nav.selectOnDown = next;
				} break;

			case ENavigationDirection.Left:
				{
					nav.selectOnLeft = next;
				} break;

			case ENavigationDirection.Right:
				{
					nav.selectOnRight = next;
				} break;
		}

		self.navigation = nav;
	}
}