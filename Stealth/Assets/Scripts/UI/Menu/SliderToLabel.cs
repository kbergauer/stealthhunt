﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderToLabel : MonoBehaviour 
{
	public Slider Target = null;
	public string Suffix = "";

	private Text self;

	void Awake()
	{
		self = GetComponent<Text>();

		if(Target != null)
			self.text = Target.value + " " + Suffix;
	}

	public void OnValueChange(float _Value)
	{
		self.text = _Value + " " + Suffix;
	}
}