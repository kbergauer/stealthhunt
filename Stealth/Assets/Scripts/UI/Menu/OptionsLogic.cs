﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsLogic : MonoBehaviour 
{
	public ResolutionSwitcher Resolution;
	public Toggle Fullscreen;

	void Start()
	{
		if(Fullscreen != null)
		{
			Fullscreen.isOn = Screen.fullScreen;
		}
	}

	public void Apply()
	{
		int width = Screen.currentResolution.width;
		int height = Screen.currentResolution.height;
		if(Resolution != null)
		{
			width = Resolution.GetWidth();
			height = Resolution.GetHeight();
		}

		bool fullscreen = Screen.fullScreen;
		if(Fullscreen != null)
		{
			fullscreen = Fullscreen.isOn;
		}

		Screen.SetResolution(width, height, fullscreen);
	}
}