﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VolumeSliderParameters : MonoBehaviour 
{
	public Selectable Label;
	public Selectable ValueLabel;

	public string SaveKey = "";
	public AudioSource DirectSource = null;
}