﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(VolumeSliderParameters))]
public class VolumeSlider : Slider 
{
	private Selectable label;
	private Selectable valueLabel;

	private string saveKey = "";
	private AudioSource directSource = null;

	private Text valueLabelText = null;

	protected override void Awake()
	{
		base.Awake();

		VolumeSliderParameters parameters = GetComponent<VolumeSliderParameters>();
		label = parameters.Label;
		valueLabel = parameters.ValueLabel;
		saveKey = parameters.SaveKey;
		directSource = parameters.DirectSource;

		if(valueLabel != null)
			valueLabelText = valueLabel.GetComponent<Text>();

		onValueChanged.AddListener(ValueChanged);
	}

	protected override void OnDestroy()
	{
		onValueChanged.RemoveListener(ValueChanged);

		base.OnDestroy();
	}

	public override void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
	{
		base.OnSelect(eventData);

		if(label != null)
			label.OnSelect(eventData);

		if(valueLabel != null)
			valueLabel.OnSelect(eventData);
	}

	public override void OnDeselect(UnityEngine.EventSystems.BaseEventData eventData)
	{
		base.OnDeselect(eventData);

		if(label != null)
			label.OnDeselect(eventData);

		if(valueLabel != null)
			valueLabel.OnDeselect(eventData);
	}

	public void ValueChanged(float _Value)
	{
		if(!string.IsNullOrEmpty(saveKey))
		{
			PlayerPrefs.SetFloat(saveKey, _Value);
		}

		if(directSource != null)
		{
			directSource.volume = _Value;
		}

		if(valueLabelText != null)
		{
			valueLabelText.text = (_Value * 100.0f).ToString("0") + "%";
		}
	}
}