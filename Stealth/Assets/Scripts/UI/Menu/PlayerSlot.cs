﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerSlot : MonoBehaviour 
{
	public Image Panel;
	public Text Headline;
	public Text Subline;
}