﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Instructions : MonoBehaviour 
{
	public GameObject[] Pages;
	public Button Next;
	public Button Previous;

	private int pageID = 0;

	void OnEnable()
	{
		SetPage(0);
	}

	public void NextPage()
	{
		SetPage(pageID + 1);
	}

	public void PreviousPage()
	{
		SetPage(pageID - 1);
	}

	public void SetPage(int _ID)
	{
		if(_ID < 0 || _ID >= Pages.Length)
			return;

		Pages[pageID].SetActive(false);
		pageID = _ID;
		Pages[pageID].SetActive(true);

		if(pageID == 0)
		{
			Previous.interactable = false;
		}
		else if(!Previous.interactable)
		{
			Previous.interactable = true;
		}

		if(pageID >= Pages.Length - 1)
		{
			Next.interactable = false;
		}
		else if(!Next.interactable)
		{
			Next.interactable = true;
		}
	}
}