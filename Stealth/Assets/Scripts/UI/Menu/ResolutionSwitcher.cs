﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ResolutionSwitcher : Selectable 
{
	[Space(10.0f)]
	public Text Setting;

	private Selectable settingSelectable;
	private int res = 0;

	protected override void Awake()
	{
		base.Awake();

		// Get Setting selectable component for coloring
		settingSelectable = Setting.GetComponent<Selectable>();

	}

	protected override void Start()
	{
		base.Start();

		// Get current resolution
		for(int i = 0; i < Screen.resolutions.Length; i++)
		{
			if(Screen.currentResolution.width == Screen.resolutions[i].width
				&& Screen.currentResolution.height == Screen.resolutions[i].height)
			{
				res = i;
				break;
			}
		}

		//Debug.Log("Resolution: " + res + " of " + Screen.resolutions.Length);

		// Set text
		UpdateSetting();
	}

	public override void OnPointerDown(PointerEventData eventData)
	{
		base.OnPointerDown(eventData);

		//Debug.Log("OnPointerDown");

		res++;
		if(res >= Screen.resolutions.Length)
			res = 0;

		UpdateSetting();
	}

	public override void OnMove(AxisEventData eventData)
	{
		//Debug.Log("OnMove: " + eventData.moveDir);

		// Catch horizontal movement
		if(eventData.moveDir == MoveDirection.Left)
		{
			res--;
			if(res < 0)
				res = Screen.resolutions.Length - 1;

			UpdateSetting();
		}
		else if(eventData.moveDir == MoveDirection.Right)
		{
			res++;
			if(res >= Screen.resolutions.Length)
				res = 0;

			UpdateSetting();
		}
		// Use default vertical movement
		else
		{
			base.OnMove(eventData);
		}
	}

	public override void OnSelect(BaseEventData eventData)
	{
		base.OnSelect(eventData);

		if(settingSelectable != null)
		{
			settingSelectable.OnSelect(eventData);
		}
	}

	public override void OnDeselect(BaseEventData eventData)
	{
		base.OnDeselect(eventData);

		if(settingSelectable != null)
		{
			settingSelectable.OnDeselect(eventData);
		}
	}

	private void UpdateSetting()
	{
		//Debug.Log("Resolution: " + res);

		if(Setting != null)
		{
			Setting.text = "< " + Screen.resolutions[res].width + "x" + Screen.resolutions[res].height + " >";
		}
	}



	public int GetWidth()
	{
		return Screen.resolutions[res].width;
	}

	public int GetHeight()
	{
		return Screen.resolutions[res].height;
	}
}