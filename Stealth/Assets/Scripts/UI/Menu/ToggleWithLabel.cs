﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(ToggleWithLabelParameters))]
public class ToggleWithLabel : Toggle 
{
	private Selectable label;

	protected override void Awake()
	{
		base.Awake();

		ToggleWithLabelParameters parameters = GetComponent<ToggleWithLabelParameters>();
		label = parameters.Label;
	}

	public override void OnSelect(BaseEventData eventData)
	{
		base.OnSelect(eventData);

		if(label != null)
		{
			label.OnSelect(eventData);
		}
	}

	public override void OnDeselect(BaseEventData eventData)
	{
		base.OnDeselect(eventData);

		if(label != null)
		{
			label.OnDeselect(eventData);
		}
	}
}