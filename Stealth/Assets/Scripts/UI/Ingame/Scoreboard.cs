﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Scoreboard : MonoBehaviour 
{
	[Header("Display")]
	public bool Animate = true;
	public float Speed = 1.0f;
	public float Opacity = 1.0f;
	public GameObject Statistics;
	public ScoreboardEntry Entry;
	public Color Highlight = Color.red;

	[Header("Interaction")]
	public Button Restart;
	public Button Menu;

	private CanvasGroup group;
	private RectTransform rect;
	private List<ScoreboardEntry> entries;
	private bool initialized = false;
	private bool showing = false;

	private Player target;



	public bool IsShowing
	{
		get { return showing; }
	}



	void Awake()
	{
		group = GetComponent<CanvasGroup>();
		rect = GetComponent<RectTransform>();
	}

	void Update()
	{
		if(GameManager.IsRunning)
		{
			if(!initialized)
				CreateStatistics();

			if(IsShowing)
				UpdateStatistics();
		}
	}

	void OnDestroy()
	{
		Deactivate();
	}

	public void SetTarget(Player _Target)
	{
		target = _Target;
	}

	public void Show()
	{
		showing = true;

		if(Animate)
		{
			StopAllCoroutines();
			StartCoroutine(FadeIn(Speed));
		}
		else
		{
			Activate();
		}
	}

	public void Hide()
	{
		showing = false;

		if(Animate)
		{
			StopAllCoroutines();
			StartCoroutine(FadeOut(Speed));
		}
		else
		{
			Deactivate();
		}
	}

	private IEnumerator FadeIn(float _Speed)
	{
		while(group.alpha < Opacity)
		{
			group.alpha += Time.deltaTime * _Speed;
			yield return new WaitForEndOfFrame();
		}

		Activate();
	}

	private IEnumerator FadeOut(float _Speed)
	{
		while(group.alpha > 0.0f)
		{
			group.alpha -= Time.deltaTime * _Speed;
			yield return new WaitForEndOfFrame();
		}

		Deactivate();
	}

	private void Activate()
	{
		if(Restart == null && Menu == null)
			return;

		// Setup button actions
		if(Restart != null)
			Restart.onClick.AddListener(delegate { GameManager.SwitchScene(Application.loadedLevelName); });
		if(Menu != null)
			Menu.onClick.AddListener(GameManager.LoadMenu);

		// Activate buttons
		group.interactable = true;
		group.blocksRaycasts = true;

		// Enable controller input
		if(Restart != null)
			Restart.Select();
	}

	private void Deactivate()
	{
		if(Restart == null && Menu == null)
			return;

		// Remove button actions
		if(Restart != null)
			Restart.onClick.RemoveAllListeners();
		if(Menu != null)
			Menu.onClick.RemoveAllListeners();

		// Deactivate interactability
		group.interactable = true;
		group.blocksRaycasts = true;
	}

	private void CreateStatistics()
	{
		// Get statistics
		Dictionary<string, int> kills = GameManager.GetAllKills();
		Dictionary<string, int> deaths = GameManager.GetAllDeaths();
		if(kills == null || deaths == null)
			return;

		// Order kills descending
		var ordered = from pair in kills orderby pair.Value descending select pair;

		// Create stat lines
		entries = new List<ScoreboardEntry>();
		int i = 0;
		foreach(KeyValuePair<string, int> player in ordered)
		{
			// Create entry
			entries.Add(Instantiate(Entry) as ScoreboardEntry);

			// Increase window height
			rect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y + entries[i].Layout.preferredHeight);
			
			// Parent entry
			entries[i].transform.SetParent(Statistics.transform);
			entries[i].transform.localScale = Vector3.one;

			// Set name
			if(entries[i].NameLabel != null)
				entries[i].NameLabel.text = player.Key;

			// Set kills
			if(entries[i].KillsLabel != null)
				entries[i].KillsLabel.text = player.Value.ToString();

			// Set deaths
			if(entries[i].DeathsLabel != null)
				entries[i].DeathsLabel.text = deaths[player.Key].ToString();

			// Disable line for last entry
			if(i >= kills.Keys.Count - 1)
			{
				if(entries[i].Separator != null)
					entries[i].Separator.gameObject.SetActive(false);
			}

			// Highlight own entry
			if(target != null && player.Key == target.name)
			{
				entries[i].NameLabel.color = Highlight;
				if(entries[i].KillsLabel != null)
					entries[i].KillsLabel.color = Highlight;
				if(entries[i].DeathsLabel != null)
					entries[i].DeathsLabel.color = Highlight;
			}

			i++;
		}

		initialized = true;
	}

	public void UpdateStatistics()
	{
		// Get statistics
		Dictionary<string, int> kills = GameManager.GetAllKills();
		Dictionary<string, int> deaths = GameManager.GetAllDeaths();
		if(kills == null || deaths == null)
			return;

		bool reorder = false;

		// Update each entry
		for(int i = 0; i < entries.Count; i++)
		{
			// Ignore if there is no valid name label
			if(entries[i].NameLabel == null)
				continue;

			// Get corresponding player
			foreach(KeyValuePair<string, int> player in kills)
			{
				// Set kills and deaths if the correct player has been found
				if(player.Key == entries[i].NameLabel.text)
				{
					if(entries[i].KillsLabel != null)
					{
						if(entries[i].KillsLabel.text != player.Value.ToString())
						{
							entries[i].KillsLabel.text = player.Value.ToString();
							reorder = true;
						}
					}
					
					if(entries[i].DeathsLabel != null)
					{
						if(entries[i].DeathsLabel.text != deaths[player.Key].ToString())
						{
							entries[i].DeathsLabel.text = deaths[player.Key].ToString();
							reorder = true;
						}
					}

					break;
				}
			}
		}

		// Reorder entries if necessary
		if(reorder)
		{
			// Sort entries by kills descending
			entries.Sort();

			// Set sibling indexes
			for(int i = 0; i < entries.Count; i++)
			{
				entries[i].transform.SetSiblingIndex(i + 1);

				if(i < entries.Count - 1)
					entries[i].Separator.gameObject.SetActive(true);
				else
					entries[i].Separator.gameObject.SetActive(false);
			}
		}
	}
}