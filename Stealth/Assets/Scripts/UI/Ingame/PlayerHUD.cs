﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerHUD : MonoBehaviour 
{
	[Header("General")]
	public Text Message;
	public Text RespawnInfo;
	public Image InteractionInfo;
	public Image InteractionProgress;
	public Image EndOverlay;

	[Header("Utility Slot")]
	public Image UtilityIcon;
	public Text UtilityUses;

	[Header("Pickup Offer")]
	public CanvasGroup PickupPanel;
	public Image PickupIcon;
	public Text PickupUses;
	public Image PickupArrow;
	public Sprite MouseArrow;
	public Sprite ControllerArrow;

	[Header("Scoreboard")]
	public Scoreboard Scores;

	private Player target;
	private Animator itemAnimator;

	void OnDestroy()
	{
		if(target != null)
		{
			target.OnEquipUtility -= SetUtility;
			target.OnInteractionNear -= SetInteraction;
		}
	}

	void Update()
	{
		if(target == null)
		{
			gameObject.SetActive(false);
			return;
		}

		if(InteractionProgress != null)
			InteractionProgress.fillAmount = target.IsInteracting ? target.InteractionProgress : 0.0f;
	}

	public void SetTarget(Player _Target)
	{
		if(target != null)
		{
			target.OnEquipUtility -= SetUtility;
			target.OnInteractionNear -= SetInteraction;
		}

		target = _Target;

		if(target != null)
		{
			target.OnEquipUtility += SetUtility;
			target.OnInteractionNear += SetInteraction;

			IInput input = target.INPUT != null ? target.INPUT : target.gameObject.GetInterface<IInput>();
			if(input != null)
			{
				if(input.Device == PlayerInput.EDevice.Keyboard)
					PickupArrow.sprite = MouseArrow;
				else
					PickupArrow.sprite = ControllerArrow;
			}

			itemAnimator = GetComponentInChildren<Animator>();
		}
		else
		{
			itemAnimator = null;
		}

		Scores.SetTarget(target);
	}

	public void AnimateUtility()
	{
		if(target == null)
			return;

		if(target.Usable != null)
			itemAnimator.SetTrigger("Add");
		else
			itemAnimator.SetTrigger("Remove");
	}

	public void SetUtility()
	{
		if(target == null)
			return;

		if(target.Usable != null)
		{
			UtilityIcon.sprite = target.Usable.Icon;
			UtilityUses.text = "x" + target.Usable.UsesLeft;

			UtilityIcon.enabled = true;
			UtilityUses.enabled = true;
		}
		else
		{
			UtilityIcon.enabled = false;
			UtilityUses.enabled = false;
		}
	}

	public void SubtractUtility()
	{
		if(target == null || target.Usable == null)
			return;

		UtilityUses.text = "x" + target.Usable.UsesLeft.ToString();
		UtilityUses.enabled = true;
	}

	public void ShowPickup(IUsable _Pickup)
	{
		if(_Pickup != null)
		{
			PickupIcon.sprite = _Pickup.Icon;
			PickupUses.text = "x" + _Pickup.Uses;

			itemAnimator.SetTrigger("Show");
		}
	}

	public void HidePickup(bool _Accept)
	{
		if(itemAnimator.GetCurrentAnimatorStateInfo(0).IsName("ShowPickup")
			|| itemAnimator.GetCurrentAnimatorStateInfo(0).IsName("OpaquePickup"))
			itemAnimator.SetTrigger("Hide");
	}

	public void SetInteraction()
	{
		if(target == null)
			return;

		InteractionInfo.enabled = target.Interactable != null && target.Interactable.CanInteract;
	}

	public void SetInteraction(bool _Enable)
	{
		if(target == null)
			return;

		InteractionInfo.enabled = _Enable;
	}

	public void ShowOverlay(Color _Color)
	{
		Color color = _Color;
		color.a = EndOverlay.color.a;
		EndOverlay.color = color;

		StartCoroutine(FadeOverlay(1.0f));
	}

	public void HideOverlay()
	{
		StartCoroutine(FadeOverlay(-1.0f));
	}

	private IEnumerator FadeOverlay(float _Speed)
	{
		CanvasGroup group = EndOverlay.GetComponent<CanvasGroup>();
		if(group == null)
			yield break;

		float limit = _Speed > 0.0f ? 1.0f : 0.0f;

		if(limit == 0.0f)
		{
			while(group.alpha > limit)
			{
				group.alpha += GameManager.deltaTime * _Speed;
				yield return new WaitForEndOfFrame();
			}
		}
		else
		{
			while(group.alpha < limit)
			{
				group.alpha += GameManager.deltaTime * _Speed;
				yield return new WaitForEndOfFrame();
			}
		}
	}

	public void ShowMessage(string _Message, Color _Color)
	{
		if(Message != null)
			StartCoroutine(DisplayMessage(_Message, _Color));
	}

	private IEnumerator DisplayMessage(string _Message, Color _Color)
	{
		Message.text = _Message;
		Message.color = _Color;
		CanvasGroup group = Message.GetComponent<CanvasGroup>();

		while(group.alpha < 1.0f)
		{
			group.alpha += GameManager.deltaTime * 1.5f;
			yield return new WaitForEndOfFrame();
		}

		yield return new WaitForSeconds(1.0f);

		while(group.alpha > 0.0f)
		{
			group.alpha -= GameManager.deltaTime * 1.5f;
			yield return new WaitForEndOfFrame();
		}

		Message.text = "";
	}

	public void ShowScoreTable(bool _Show)
	{
		if(_Show && !Scores.IsShowing)
		{
			Scores.Show();
		}
		else if(!_Show && Scores.IsShowing)
		{
			Scores.Hide();
		}
	}

	public void ShowRespawn()
	{
		StartCoroutine("DisplayRespawn");
	}

	private IEnumerator DisplayRespawn()
	{
		CanvasGroup group = RespawnInfo.GetComponent<CanvasGroup>();

		while(group.alpha < 1.0f)
		{
			group.alpha += GameManager.deltaTime * 2.0f;
			yield return new WaitForEndOfFrame();
		}

		group.alpha = 1.0f;
	}

	private IEnumerator HideRespawn()
	{
		StopCoroutine("DisplayRespawn");

		CanvasGroup group = RespawnInfo.GetComponent<CanvasGroup>();

		while(group.alpha > 0.0f)
		{
			group.alpha -= GameManager.deltaTime * 2.0f;
			yield return new WaitForEndOfFrame();
		}

		group.alpha = 0.0f;
	}

	public void UpdateRespawn(float _Time)
	{
		if(RespawnInfo == null)
			return;

		if(_Time <= 0.0f)
		{
			StartCoroutine(HideRespawn());
		}
		else
		{
			RespawnInfo.text = "Respawning in " + _Time.ToString("0.0") + "s..";
		}
	}
}