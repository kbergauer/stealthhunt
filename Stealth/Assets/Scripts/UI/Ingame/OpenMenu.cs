﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenMenu : MonoBehaviour 
{
	public CanvasGroup Menu = null;
	public Selectable ActiveButton = null;

	public float Speed = 1.0f;

	private IInput[] inputs;
	private bool showing = false;

	void Update()
	{
		if(Menu == null)
		{
			this.enabled = false;
			return;
		}

		if(!GameManager.IsRunning)
			return;

		if(Input.GetButtonDown("Escape")
			|| Input.GetButtonDown("C1_Start")
			|| Input.GetButtonDown("C2_Start")
			|| Input.GetButtonDown("C3_Start")
			|| Input.GetButtonDown("C4_Start"))
		{
			StopAllCoroutines();

			if(showing)
			{
				GameManager.timeScale = 1.0f;

				Player[] players = FindObjectsOfType<Player>();
				for(int i = 0; i < players.Length; i++)
				{
					players[i].INPUT.Unlock();
				}

				showing = false;
				StartCoroutine(Hide());
			}
			else
			{
				GameManager.timeScale = 0.0f;

				Player[] players = FindObjectsOfType<Player>();
				for(int i = 0; i < players.Length; i++)
				{
					players[i].INPUT.Lock();
				}

				showing = true;
				StartCoroutine(Show());
			}
		}
	}

	private IEnumerator Show()
	{
		Menu.interactable = true;
		Menu.blocksRaycasts = true;
		ActiveButton.Select();

		while(Menu.alpha < 1.0f)
		{
			Menu.alpha += Time.deltaTime * Speed;
			yield return new WaitForEndOfFrame();
		}

		Menu.alpha = 1.0f;
	}

	private IEnumerator Hide()
	{
		Menu.interactable = false;
		Menu.blocksRaycasts = false;

		while(Menu.alpha > 0.0f)
		{
			Menu.alpha -= Time.deltaTime * Speed;
			yield return new WaitForEndOfFrame();
		}

		Menu.alpha = 0.0f;
	}

	public void CloseWindow()
	{
		StopAllCoroutines();

		GameManager.timeScale = 1.0f;

		showing = false;
		StartCoroutine(Hide());
	}

	public void LeaveGame()
	{
		GameManager.LoadMenu();
	}
}