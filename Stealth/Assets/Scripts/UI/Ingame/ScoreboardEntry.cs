﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ScoreboardEntry : MonoBehaviour, IComparable
{
	[Header("Elements")]
	public Text NameLabel;
	public Text KillsLabel;
	public Text DeathsLabel;
	public Image Separator;

	[Header("Size")]
	public LayoutElement Layout;



	public int CompareTo(object _Other)
	{
		if(_Other == null)
		{
			return -1;
		}
		else
		{
			ScoreboardEntry entry = _Other as ScoreboardEntry;

			int self = int.Parse(KillsLabel.text);
			int other = int.Parse(entry.KillsLabel.text);

			if(self > other)
				return -1;
			else if(self < other)
				return 1;
			else
			{
				self = int.Parse(DeathsLabel.text);
				other = int.Parse(entry.DeathsLabel.text);

				if(self < other)
					return -1;
				else if(self > other)
					return 1;
			}
		}

		return 0;
	}
}