﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavMeshNode 
{
	public struct Neighbor
	{
		public NavMeshNode Node;
		public Vector2[] Edge;

		public Vector2 EdgeCenter
		{
			get
			{
				if(Edge[0].x == Edge[1].x)
				{
					return new Vector2(Edge[0].x, Edge[0].y + (Edge[1].y - Edge[0].y) / 2.0f);
				}
				else
				{
					return new Vector2(Edge[0].x + (Edge[1].x - Edge[0].x) / 2.0f, Edge[0].y);
				}
			}
		}
	}



	public Rect Bounds;
	public List<Neighbor> Neighbors;



	public NavMeshNode()
	{
		Bounds = new Rect();
		Neighbors = new List<Neighbor>();
	}

	public NavMeshNode(Rect _Bounds)
	{
		Bounds = _Bounds;
		Neighbors = new List<Neighbor>();
	}
}