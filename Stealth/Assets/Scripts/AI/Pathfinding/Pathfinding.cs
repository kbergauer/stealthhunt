﻿using UnityEngine;
using System.Collections.Generic;

public class Pathfinding 
{
	struct PathNode
	{
		public NavMeshNode Parent;
		public int Edge;
		public float Cost;
		public float Heurisic;

		public float Total
		{
			get { return Cost + Heurisic; }
		}
	}

	static private Pathfinding instance;
	static public Pathfinding Instance
	{
		get
		{
			if(instance == null)
			{
				instance = new Pathfinding();
			}

			return instance;
		}
	}

	private NavMesh grid;

	private Pathfinding()
	{
		grid = Object.FindObjectOfType<NavMesh>();
		if(grid == null)
			Debug.LogWarning("Pathfinding: No grid found; pathfinding impossible!");
	}

	private Vector2[] FindShortestPath(Vector2 _Start, Vector2 _Goal)
	{
		if(grid == null)
			return null;

		NavMeshNode goal = grid.GetNode(_Goal);
		NavMeshNode current = grid.GetNode(_Start);

		if(current == null || goal == null)
			return null;

		Dictionary<NavMeshNode, PathNode> pathData = new Dictionary<NavMeshNode,PathNode>();

		List<NavMeshNode> open = new List<NavMeshNode>();
		List<NavMeshNode> closed = new List<NavMeshNode>();

		PathNode node = new PathNode();

		float startDistance = 0.0f;
		float goalDistance = 0.0f;
		float newCost = 0.0f;

		node.Parent = null;
		node.Cost = 0.0f;
		node.Heurisic = Vector2.Distance(current.Bounds.center, goal.Bounds.center);
		pathData.Add(current, node);

		open.Add(current);

		while(open.Count > 0)
		{
			// Get node with lowest cost
			current = open[0];

			if(current == goal)
			{
				// Reconstruct path
				//Debug.Log("Found path; reconstructing..");
				return ReconstructPath(_Start, _Goal, pathData[goal], pathData);
			}

			open.Remove(current);
			closed.Add(current);

			// Get neighbors
			for(int i = 0; i < current.Neighbors.Count; i++)
			{
				if(closed.Contains(current.Neighbors[i].Node))
					continue;

				startDistance = Vector2.Distance(current.Bounds.center, current.Neighbors[i].Node.Bounds.center);
				goalDistance = Vector2.Distance(current.Neighbors[i].Node.Bounds.center, goal.Bounds.center);
				newCost = (pathData[current].Cost + startDistance) + goalDistance;

				if(!open.Contains(current.Neighbors[i].Node) || newCost < pathData[current.Neighbors[i].Node].Total)
				{
					node = new PathNode();
					node.Parent = current;
					node.Edge = i;
					node.Cost = (pathData[current].Cost + startDistance);
					node.Heurisic = goalDistance;
					if(pathData.ContainsKey(current.Neighbors[i].Node))
						pathData[current.Neighbors[i].Node] = node;
					else
						pathData.Add(current.Neighbors[i].Node, node);

					if(!open.Contains(current.Neighbors[i].Node))
						open.Add(current.Neighbors[i].Node);
				}
			}

			// Sort open list by cost ascending
			open.Sort(delegate(NavMeshNode one, NavMeshNode two)
				{
					return pathData[one].Total.CompareTo(pathData[two].Total);
				}
			);
		}

		//Debug.Log("Found no path.");
		return null;
	}

	private Vector2[] ReconstructPath(Vector2 _Start, Vector2 _Goal, PathNode _Last, Dictionary<NavMeshNode, PathNode> _PathData)
	{
		List<Vector2> path = new List<Vector2>();
		path.Add(_Goal);

		PathNode node = _Last;
		while(node.Parent != null)
		{
			path.Add(node.Parent.Neighbors[node.Edge].EdgeCenter);
			if(node.Parent != null)
				node = _PathData[node.Parent];
		}

		path.Add(_Start);
		path.Reverse();

		return path.ToArray();
	}

	private Vector2 GetRandomPointOnNavMesh()
	{
		return grid != null ? grid.GetRandomPoint() : Vector2.zero;
	}

	private Vector2[] SimplifyPath(Vector2[] _Path, int _Iterations, float _Radius)
	{
		if(_Path == null || _Path.Length <= 2 || _Iterations <= 0)
		{
			return _Path;
		}

		//System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
		//watch.Start();

		List<Vector2> path = new List<Vector2>(_Path);
		RaycastHit2D middle;
		RaycastHit2D left;
		RaycastHit2D right;
		Vector2 direction;
		Vector2 normal;
		int node = 0;

		for(int iterator = 0; iterator < _Iterations; iterator++)
		{
			node = 0;
			while(path.Count > 2 && node < path.Count - 2)
			{
				direction = path[node + 2] - path[node];
				normal = Quaternion.AngleAxis(90.0f, Vector3.forward) * direction.normalized;

				middle = Physics2D.Raycast(path[node], direction.normalized, direction.magnitude);
				left = Physics2D.Raycast(path[node] + normal * _Radius, direction.normalized, direction.magnitude);
				right = Physics2D.Raycast(path[node] - normal * _Radius, direction.normalized, direction.magnitude);

				if((middle.collider != null && !middle.collider.CompareTag("Door"))
					|| (left.collider != null && !left.collider.CompareTag("Door"))
					|| (right.collider != null && !right.collider.CompareTag("Door")))
				{
					node++;
				}
				else
				{
					path.RemoveAt(node + 1); 
				}
			}
		}

		//watch.Stop();
		//System.TimeSpan ts = watch.Elapsed;
		//Debug.Log("Path simplification done (" + ts.Minutes.ToString("00") + ":" + ts.Seconds.ToString("00") + ":" + ts.Milliseconds.ToString("000") + "): " + (_Path.Length - path.Count) + " of " + _Path.Length + " nodes removed.");

		return path.ToArray();
	}



	static public bool IsReady()
	{
		return Instance.grid != null;
	}

	static public NavMesh GetGrid()
	{
		return Instance.grid;
	}

	static public void SetGrid(NavMesh _Grid)
	{
		Instance.grid = _Grid;
	}

	static public Vector2[] GetPath(Vector2 _Start, Vector2 _Goal)
	{
		return Instance.FindShortestPath(_Start, _Goal);
	}

	static public Vector2[] GetSimplifiedPath(Vector2 _Start, Vector2 _Goal, int _Iterations, float _Radius)
	{
		Vector2[] path = Instance.FindShortestPath(_Start, _Goal);
		return Instance.SimplifyPath(path, _Iterations, _Radius);
	}

	static public Vector2 GetRandomDestination()
	{
		return Instance.GetRandomPointOnNavMesh();
	}

	static public Vector2 GetRandomDestination(Vector2 _Position, float _MinimumDistance)
	{
		Vector2 result = _Position;

		while((result - _Position).magnitude < _MinimumDistance)
		{
			result = Instance.GetRandomPointOnNavMesh();
		}

		return result;
	}
}