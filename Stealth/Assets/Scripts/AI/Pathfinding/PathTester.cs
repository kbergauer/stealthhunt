﻿using UnityEngine;
using System.Collections;

public class PathTester : MonoBehaviour 
{
	public Transform Start;
	public Transform Goal;

	public bool Continuous = false;

	private Vector2[] path;
	private Vector2[] simplified;

	void OnDrawGizmos()
	{
		if(path != null && path.Length > 1)
		{
			Gizmos.color = Color.blue;
			for(int i = 0; i < path.Length - 1; i++)
			{
				Gizmos.DrawLine(path[i], path[i + 1]);
			}
		}

		if(simplified != null && simplified.Length > 1)
		{
			Gizmos.color = Color.cyan;
			for(int i = 0; i < simplified.Length - 1; i++)
			{
				Gizmos.DrawLine(simplified[i], simplified[i + 1]);
			}
		}
	}

	void Update()
	{
		if(!Continuous)
			return;

		GetPath();
	}

	public void GetPath()
	{
		if(Pathfinding.IsReady())
		{
			path = Pathfinding.GetPath(Start.position, Goal.position);
			simplified = Pathfinding.GetSimplifiedPath(Start.position, Goal.position, 2, 0.51f);
		}
	}
}