﻿using UnityEngine;
using System.Collections.Generic;

public class NavMesh : MonoBehaviour 
{
	private List<NavMeshNode> nodes;

	void OnDrawGizmosSelected()
	{
		for(int i = 0; i < nodes.Count; i++)
		{
			Gizmos.color = new Color(0.0f, 1.0f, 1.0f, 0.5f);
			Gizmos.DrawCube(nodes[i].Bounds.center, nodes[i].Bounds.size);
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireCube(nodes[i].Bounds.center, nodes[i].Bounds.size);
		}

		for(int i = 0; i < nodes.Count; i++)
		{
			if(nodes[i].Neighbors == null || nodes[i].Neighbors.Count == 0)
				continue;

			for(int j = 0; j < nodes[i].Neighbors.Count; j++)
			{
				Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
				Vector3 center = (nodes[i].Neighbors[j].Edge[0].x == nodes[i].Neighbors[j].Edge[1].x) ?
					new Vector3(nodes[i].Neighbors[j].Edge[0].x, nodes[i].Neighbors[j].Edge[0].y + ((nodes[i].Neighbors[j].Edge[1].y - nodes[i].Neighbors[j].Edge[0].y) / 2.0f), 0.0f) :
					new Vector3(nodes[i].Neighbors[j].Edge[0].x + ((nodes[i].Neighbors[j].Edge[1].x - nodes[i].Neighbors[j].Edge[0].x) / 2.0f), nodes[i].Neighbors[j].Edge[0].y, 0.0f);
				Gizmos.DrawCube(center, (nodes[i].Neighbors[j].Edge[0].x == nodes[i].Neighbors[j].Edge[1].x)
					? new Vector3(0.1f, nodes[i].Neighbors[j].Edge[1].y - nodes[i].Neighbors[j].Edge[0].y, 1.0f)
					: new Vector3(nodes[i].Neighbors[j].Edge[1].x - nodes[i].Neighbors[j].Edge[0].x, 0.1f, 1.0f));
			}
		}
	}

	void Awake()
	{
		if(Pathfinding.GetGrid() != this)
			Pathfinding.SetGrid(this);
	}




	public void SetNodes(List<NavMeshNode> _Nodes)
	{
		nodes = _Nodes;
	}

	public void UpdateNodeNeighbors()
	{
		NavMeshNode.Neighbor first;
		NavMeshNode.Neighbor second;

		bool ignoreNode = false;

		for(int i = 0; i < nodes.Count; i++)
		{
			Rect firstBounds = nodes[i].Bounds;
			firstBounds.xMin -= 0.1f;
			firstBounds.yMin -= 0.1f;
			firstBounds.xMax += 0.1f;
			firstBounds.yMax += 0.1f;

			for(int j = 0; j < nodes.Count; j++)
			{
				Rect secondBounds = nodes[j].Bounds;
				secondBounds.xMin -= 0.1f;
				secondBounds.yMin -= 0.1f;
				secondBounds.xMax += 0.1f;
				secondBounds.yMax += 0.1f;

				if(!firstBounds.Overlaps(secondBounds))
					continue;

				// Check if neighbor already processed
				ignoreNode = false;
				for(int k = 0; k < nodes[i].Neighbors.Count; k++)
				{
					if(nodes[i].Neighbors[k].Node == nodes[j])
					{
						ignoreNode = true;
						break;
					}
				}
				if(ignoreNode)
					continue;

				// Top
				if(nodes[i].Bounds.yMax == nodes[j].Bounds.yMin)
				{
					first = new NavMeshNode.Neighbor();
					first.Node = nodes[j];
					first.Edge = new Vector2[] { 
						new Vector2(Mathf.Max(nodes[i].Bounds.xMin, nodes[j].Bounds.xMin), nodes[i].Bounds.yMax), 
						new Vector2(Mathf.Min(nodes[i].Bounds.xMax, nodes[j].Bounds.xMax), nodes[i].Bounds.yMax)
					};
					nodes[i].Neighbors.Add(first);

					second = new NavMeshNode.Neighbor();
					second.Node = nodes[i];
					second.Edge = first.Edge;
					nodes[j].Neighbors.Add(second);
				}
				// Bottom
				else if(nodes[i].Bounds.yMin == nodes[j].Bounds.yMax)
				{
					first = new NavMeshNode.Neighbor();
					first.Node = nodes[j];
					first.Edge = new Vector2[] { 
						new Vector2(Mathf.Max(nodes[i].Bounds.xMin, nodes[j].Bounds.xMin), nodes[i].Bounds.yMin), 
						new Vector2(Mathf.Min(nodes[i].Bounds.xMax, nodes[j].Bounds.xMax), nodes[i].Bounds.yMin)
					};
					nodes[i].Neighbors.Add(first);

					second = new NavMeshNode.Neighbor();
					second.Node = nodes[i];
					second.Edge = first.Edge;
					nodes[j].Neighbors.Add(second);
				}
				// Left
				else if(nodes[i].Bounds.xMin == nodes[j].Bounds.xMax)
				{
					first = new NavMeshNode.Neighbor();
					first.Node = nodes[j];
					first.Edge = new Vector2[] { 
						new Vector2(nodes[i].Bounds.xMin, Mathf.Max(nodes[i].Bounds.yMin, nodes[j].Bounds.yMin)), 
						new Vector2(nodes[i].Bounds.xMin, Mathf.Min(nodes[i].Bounds.yMax, nodes[j].Bounds.yMax))
					};
					nodes[i].Neighbors.Add(first);

					second = new NavMeshNode.Neighbor();
					second.Node = nodes[i];
					second.Edge = first.Edge;
					nodes[j].Neighbors.Add(second);
				}
				// Right
				else if(nodes[i].Bounds.xMax == nodes[j].Bounds.xMin)
				{
					first = new NavMeshNode.Neighbor();
					first.Node = nodes[j];
					first.Edge = new Vector2[] { 
						new Vector2(nodes[i].Bounds.xMax, Mathf.Max(nodes[i].Bounds.yMin, nodes[j].Bounds.yMin)), 
						new Vector2(nodes[i].Bounds.xMax, Mathf.Min(nodes[i].Bounds.yMax, nodes[j].Bounds.yMax))
					};
					nodes[i].Neighbors.Add(first);

					second = new NavMeshNode.Neighbor();
					second.Node = nodes[i];
					second.Edge = first.Edge;
					nodes[j].Neighbors.Add(second);
				}
			}
		}
	}




	public NavMeshNode GetNode(Vector3 _Position)
	{
		for(int i = 0; i < nodes.Count; i++)
		{
			if(nodes[i].Bounds.Contains(_Position))
				return nodes[i];
		}

		return null;
	}

	public Vector2 GetRandomPoint()
	{
		if(nodes == null || nodes.Count < 1)
		{
			return Vector2.zero;
		}

		Rect area = nodes[Random.Range(0, nodes.Count)].Bounds;
		return new Vector2(Random.Range(area.xMin, area.xMax), Random.Range(area.yMin, area.yMax));
	}
}