﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Class used for temporarily saving AI knowledge on a per agent basis.
/// </summary>
[System.Serializable]
public class AIMemory 
{
	private List<Trail> knownTrails;



	public AIMemory()
	{
		knownTrails = new List<Trail>();
	}



	public void Update()
	{
		// Remove invalid trails
		for(int i = 0; i < knownTrails.Count; i++)
		{
			if(knownTrails[i] == null)
			{
				knownTrails.RemoveAt(i);
				i--;
			}
		}
	}



	public bool AddTrail(Trail _Trail)
	{
		if(!knownTrails.Contains(_Trail))
		{
			knownTrails.Add(_Trail);
			return true;
		}

		return false;
	}

	public void AddTrails(Trail[] _Trails)
	{
		for(int i = 0; i < _Trails.Length; i++)
		{
			if(!knownTrails.Contains(_Trails[i]))
			{
				knownTrails.Add(_Trails[i]);
			}
		}
	}

	public bool RemoveTrail(Trail _Trail)
	{
		if(knownTrails.Contains(_Trail))
		{
			knownTrails.Remove(_Trail);
			return true;
		}

		return false;
	}

	public bool Contains(Trail _Trail)
	{
		return knownTrails.Contains(_Trail);
	}
}