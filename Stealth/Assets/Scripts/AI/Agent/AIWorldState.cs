﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Class used by AI agents to request world state information.
/// </summary>
public class AIWorldState 
{
	/********************************************
	Static variables
	*********************************************/
	static private AIWorldState instance;



	/// <summary>
	/// Returns a currently valid <see cref="AIWorldState"/> instance.
	/// Creates a new instance if none is available and performs a validity check on existing instances.
	/// </summary>
	/// <returns>A valid <see cref="AIWorldState"/></returns>
	static public AIWorldState GetInstance()
	{
		if(instance == null)
			instance = new AIWorldState();
		else if(!instance.IsValid())
			instance.Initialize();

		return instance;
	}



	/********************************************
	Instance variables
	*********************************************/
	private int sceneID = -1;
	private Chest[] chests;
	private List<Trail> activeTrails;



	/// <summary>
	/// Parameterless constructor used for basic initialization.
	/// </summary>
	public AIWorldState()
	{
		//Debug.Log("Creating world state.");
		Initialize();
	}



	/// <summary>
	/// Checks if the currently active instance belongs to the loaded scene.
	/// </summary>
	/// <returns><c>true</c> if the instance was initialized for the current scene, otherwise <c>false</c></returns>
	public bool IsValid()
	{
		return sceneID == Application.loadedLevel;
	}



	/// <summary>
	/// Initializes the world state to a valid instance.
	/// </summary>
	public void Initialize()
	{
		//Debug.Log("Initializing world state for scene " + Application.loadedLevel + ".");

		// Get chest references
		chests = GameObject.FindObjectsOfType<Chest>();

		// Prepare trail list
		if(activeTrails != null)
			activeTrails.Clear();
		activeTrails = new List<Trail>();

		// Save current scene ID for validity checks
		sceneID = Application.loadedLevel;
	}



	/// <summary>
	/// Adds a trail to the tracked list.
	/// </summary>
	/// <param name="_Trail">The trail to add.</param>
	/// <returns><c>true</c> if the trail was added successfully, otherwise <c>false</c></returns>
	public bool AddTrail(Trail _Trail)
	{
		if(!activeTrails.Contains(_Trail))
		{
			activeTrails.Add(_Trail);
			return true;
		}

		return false;
	}



	/// <summary>
	/// Removes a trail from the tracked list.
	/// </summary>
	/// <param name="_Trail">The trail to remove.</param>
	/// <returns><c>true</c> if the trail was removed successfully, otherwise <c>false</c></returns>
	public bool RemoveTrail(Trail _Trail)
	{
		if(activeTrails.Contains(_Trail))
		{
			activeTrails.Remove(_Trail);
			return true;
		}

		return false;
	}



	/// <summary>
	/// Removes all trails originating of the player.
	/// </summary>
	/// <param name="_Player">The originator</param>
	public void RemoveTrails(Player _Player)
	{
		for(int i = 0; i < activeTrails.Count; i++)
		{
			if(activeTrails[i].GetOriginator() == _Player)
			{
				activeTrails.RemoveAt(i);
				i--;
			}
		}
	}



	/// <summary>
	/// Returns an array containing all current trails.
	/// </summary>
	/// <returns>An array of <see cref="Trail"/> references</returns>
	public Trail[] GetAllTrails()
	{
		return activeTrails.ToArray();
	}



	/// <summary>
	/// Returns all trails that are roughly in view
	/// </summary>
	/// <param name="_Agent">The agent reference</param>
	/// <param name="_ViewAngle">The agent's view angle</param>
	/// <param name="_ViewDistance">The agent's view distance</param>
	/// <returns>An array of <see cref="Trail"/> references</returns>
	public Trail[] GetTrailsInView(AIAgent _Agent, float _ViewAngle, float _ViewDistance)
	{
		List<Trail> inView = new List<Trail>();
		List<Vector2> path = null;

		// Get all trails in view
		for(int i = 0; i < activeTrails.Count; i++)
		{
			// Ignore owned trails
			if(activeTrails[i].GetOriginator() == _Agent.PLAYER)
				continue;

			// Ignore known trails
			if(_Agent.MEMORY.Contains(activeTrails[i]))
				continue;

			// Check if at least one trail node is in the current view
			path = activeTrails[i].GetTrail();
			for(int node = 0; node < path.Count; node++)
			{
				if(IsPointInFront(path[node], _Agent.transform.position, _Agent.transform.up, _ViewAngle)
					&& !IsPointOccluded(path[node], _Agent.transform.position, LayerMask.GetMask("Hidden", "ShadowLayer")))
				{
					inView.Add(activeTrails[i]);
				}
			}
		}

		return inView.ToArray();
	}



	/// <summary>
	/// Returns the youngest trail that is roughly in view
	/// </summary>
	/// <param name="_Agent">The agent reference</param>
	/// <param name="_ViewAngle">The agent's view angle</param>
	/// <param name="_ViewDistance">The agent's view distance</param>
	/// <returns>A <see cref="Trail"/> reference if one is in view, otherwise <c>null</c></returns>
	public Trail GetYoungestTrailInView(AIAgent _Agent, float _ViewAngle, float _ViewDistance)
	{
		int index = -1;
		float age = float.PositiveInfinity;
		List<Vector2> path = null;

		// Get all trails in view
		for(int i = 0; i < activeTrails.Count; i++)
		{
			// Ignore older or owned trails
			if(activeTrails[i].GetAge() >= age || activeTrails[i].GetOriginator() == _Agent.PLAYER)
				continue;

			// Ignore known trails
			if(_Agent.MEMORY.Contains(activeTrails[i]))
				continue;

			// Check if at least one trail node is in the current view
			path = activeTrails[i].GetTrail();
			for(int node = 0; node < path.Count; node++)
			{
				if(IsPointInFront(path[node], _Agent.transform.position, _Agent.transform.up, _ViewAngle)
					&& !IsPointOccluded(path[node], _Agent.transform.position, LayerMask.GetMask("Hidden", "ShadowLayer")))
				{
					index = i;
					age = activeTrails[i].GetAge();
					break;
				}
			}
		}

		return index >= 0 ? activeTrails[index] : null;
	}



	/// <summary>
	/// Returns the last index of the path node in the trail visible for the agent.
	/// </summary>
	/// <param name="_Trail">The trail to check</param>
	/// <param name="_Position">The agent's position</param>
	/// <param name="_ViewDirection">The agent's view direction</param>
	/// <param name="_ViewAngle">The agent's view angle</param>
	/// <param name="_ViewDistance">The agent's view distance</param>
	/// <returns><c>-1</c> if trail is invalid or out of view, otherwise the index</returns>
	public int GetLastSeenPointInTrail(Trail _Trail, Vector3 _Position, Vector3 _ViewDirection, float _ViewAngle, float _ViewDistance)
	{
		int index = -1;
		if(_Trail == null)
			return index;

		// Traverse path in reverse to find the furthest visible point
		List<Vector2> path = _Trail.GetTrail();
		for(int i = path.Count - 1; i >= 0; i--)
		{
			if(IsPointInFront(path[i], _Position, _ViewDirection, _ViewAngle)
				&& !IsPointOccluded(path[i], _Position, LayerMask.GetMask("Hidden", "ShadowLayer")))
			{
				index = i;
				break;
			}
		}

		return index;
	}



	/// <summary>
	/// Roughly checks if a point is within the given view triangle and is not blocked by colliders.
	/// </summary>
	/// <param name="_Point">The point to check</param>
	/// <param name="_Position">The agent's position</param>
	/// <param name="_ViewDirection">The agent's view direction</param>
	/// <param name="_ViewAngle">The agent's view angle</param>
	/// <param name="_ViewDistance">The agent's view distance</param>
	/// <returns><c>true</c> if the specified point is in view, otherwise <c>false</c></returns>
	public bool IsPointInViewTriangle(Vector3 _Point, Vector3 _Position, Vector3 _ViewDirection, float _ViewAngle, float _ViewDistance)
	{
		Vector3 left = _Position + Quaternion.AngleAxis(-_ViewAngle / 2.0f, Vector3.forward) * _ViewDirection * _ViewDistance;
		Vector3 right = _Position + Quaternion.AngleAxis(_ViewAngle / 2.0f, Vector3.forward) * _ViewDirection * _ViewDistance;
		Vector3 middle = _Position + _ViewDirection * _ViewDistance;

		// Split view in two triangles for higher precision; some areas at maximum view distance are still lost!
		if(IsPointInTriangle(_Point, _Position, left, middle) || IsPointInTriangle(_Point, _Position, middle, right))
		{
			// Cast ray to check colliders blocking the line of sight
			int layerMask = LayerMask.GetMask("Hidden", "ShadowLayer");
			RaycastHit2D hit = Physics2D.Raycast(_Position, (_Point - _Position).normalized, (_Point - _Position).magnitude, layerMask);
			if(hit.collider == null)
			{
				return true;
			}
		}

		return false;
	}



	/// <summary>
	/// Checks if a point is within a triangle using barycentric coordinates.
	/// </summary>
	/// <param name="_Point">The point to check</param>
	/// <param name="_A">The triangle's first corner</param>
	/// <param name="_B">The triangle's second corner</param>
	/// <param name="_C">The triangle's third corner</param>
	/// <returns><c>true</c> if the point is within the triangle, otherwise <c>false</c></returns>
	public bool IsPointInTriangle(Vector2 _Point, Vector2 _A, Vector2 _B, Vector2 _C)
	{
		// Get necessary vectors from first corner
		Vector2 AtoC = _C - _A;
		Vector2 AtoB = _B - _A;
		Vector2 AtoPoint = _Point - _A;

		// Get necessary dot products
		float dot00 = Vector2.Dot(AtoC, AtoC);
		float dot01 = Vector2.Dot(AtoC, AtoB);
		float dot02 = Vector2.Dot(AtoC, AtoPoint);
		float dot11 = Vector2.Dot(AtoB, AtoB);
		float dot12 = Vector2.Dot(AtoB, AtoPoint);

		// Get barycentric coordinates
		float denom = 1 / (dot00 * dot11 - dot01 * dot01);
		float u = (dot11 * dot02 - dot01 * dot12) * denom;
		float v = (dot00 * dot12 - dot01 * dot02) * denom;

		// Check if point is within the triangle
		return (u >= 0.0f) && (v >= 0.0f) && (u + v < 1.0f);
	}



	/// <summary>
	/// Checks whether a point is within a certain angle in the view direction.
	/// </summary>
	/// <param name="_Point">The point to check</param>
	/// <param name="_ViewDirection">The view's forward direction</param>
	/// <param name="_ViewAngle">The view's total angle</param>
	/// <returns><c>true</c> if the point is in front, otherwise <c>false</c></returns>
	public bool IsPointInFront(Vector2 _Point, Vector2 _Position, Vector2 _ViewDirection, float _ViewAngle)
	{
		Vector2 pointDirection = (_Point - _Position).normalized;
		return Mathf.Abs(Vector2.Angle(_ViewDirection, pointDirection)) < _ViewAngle / 2.0f;
	}



	/// <summary>
	/// Checks if a point is occluded from the point of view.
	/// </summary>
	/// <param name="_Point">The point to check</param>
	/// <param name="_Position">The agent's position</param>
	/// <param name="_LayerMask">The layer mask to use</param>
	/// <returns><c>false</c> if the point is occluded, otherwise <c>true</c></returns>
	public bool IsPointOccluded(Vector2 _Point, Vector2 _Position, int _LayerMask)
	{
		RaycastHit2D hit = Physics2D.Raycast(_Position, (_Point - _Position).normalized, (_Point - _Position).magnitude, _LayerMask);
		return hit.collider != null;
	}



	/// <summary>
	/// Returns an array of all existing chests.
	/// </summary>
	/// <returns>An array of <see cref="Chest"/> references</returns>
	public Chest[] GetAllChests()
	{
		return chests;
	}



	/// <summary>
	/// Returns all chests that are in the current view.
	/// </summary>
	/// <param name="_Position">The agent's position</param>
	/// <param name="_ViewDirection">The agent's view direction</param>
	/// <param name="_ViewAngle">The agent's view angle</param>
	/// <param name="_ViewDistance">The agent's view distance</param>
	/// <returns>An array of <see cref="Chest"/> references</returns>
	public Chest[] GetAllChestsInView(Vector3 _Position, Vector3 _ViewDirection, float _ViewAngle, float _ViewDistance)
	{
		List<Chest> inView = new List<Chest>();

		for(int i = 0; i < chests.Length; i++)
		{
			// Ignore chests out of reach
			if((chests[i].transform.position - _Position).magnitude > _ViewDistance)
				continue;

			// Check if closer chest is in view
			if(IsPointInFront(chests[i].transform.position, _Position, _ViewDirection, _ViewAngle)
				&& !IsPointOccluded(chests[i].transform.position, _Position, LayerMask.GetMask("Hidden", "ShadowLayer")))
			{
				inView.Add(chests[i]);
			}
		}

		return inView.ToArray();
	}



	/// <summary>
	/// Returns the closest chest in view.
	/// </summary>
	/// <param name="_Position">The agent's position</param>
	/// <param name="_ViewDirection">The agent's view direction</param>
	/// <param name="_ViewAngle">The agent's view angle</param>
	/// <param name="_ViewDistance">The agent's view distance</param>
	/// <returns>A <see cref="Chest"/> reference</returns>
	public Chest GetClosestChestInView(Vector3 _Position, Vector3 _ViewDirection, float _ViewAngle, float _ViewDistance)
	{
		int index = -1;
		float distance = float.PositiveInfinity;
		float minDistance = float.PositiveInfinity;
		for(int i = 0; i < chests.Length; i++)
		{
			// Get distance to chest
			distance = (chests[i].transform.position - _Position).magnitude;

			// Ignore chests out of reach
			if(distance > _ViewDistance || distance > minDistance)
				continue;

			// Check if closer chest is in view
			if(IsPointInFront(chests[i].transform.position, _Position, _ViewDirection, _ViewAngle))
			{
				index = i;
				minDistance = distance;
			}
		}

		return index >= 0 ? chests[index] : null;
	}
}