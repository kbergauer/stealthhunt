﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AIState_Wander : IAgentState
{
	public AIAgent Agent
	{
		get;
		private set;
	}

	public string Name
	{
		get { return "Wander"; }
	}

	public string Info
	{
		get
		{
			return "Wandering randomly.";
		}
	}

	public AIState_Wander(AIAgent _Agent)
	{
		Agent = _Agent;
	}

	public IAgentState Enter()
	{
		return null;
	}

	public IAgentState Update()
	{
		IAgentState transition = GetTransition();
		if(transition != null)
			return transition;

		// Get random point on map to walk to
		if(!Agent.HasValidPath() || Agent.HasReachedPathGoal())
		{
			Vector2 goal = Pathfinding.GetRandomDestination(Agent.transform.position, 10.0f);
			Agent.UpdatePathTo(goal);
		}
		// Move to next point of path
		else
		{
			Agent.MoveAlongCurrentPath();
		}

		Agent.RotateView(Agent.WalkDirection);

		return null;
	}

	public void Exit()
	{

	}

	public IAgentState GetTransition()
	{
		// Attack player in view
		if(Agent.GetPlayersInView().Length > 0)
		{
			Agent.Log("Transitioning from " + Name + ": enemy in view.");
			return new AIState_Attack(Agent);
		}

		// Get youngest unknown trail in view
		Trail trail = Agent.WORLD.GetYoungestTrailInView(Agent, Agent.ViewAngle, Agent.ViewDistance);
		if(trail != null)
		{
			// Add all visible trails to memory
			Agent.MEMORY.AddTrails(Agent.WORLD.GetTrailsInView(Agent, Agent.ViewAngle, Agent.ViewDistance));
			
			// Follow trail if it hasn't gone cold
			if(trail.GetAge() < 30.0f)
			{
				Agent.Log("Transitioning from " + Name + ": found a trail.");
				return new AIState_FollowTrail(Agent, trail);
			}
		}

		return null;
	}
}