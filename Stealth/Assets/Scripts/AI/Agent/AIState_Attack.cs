﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AIState_Attack : IAgentState
{
	public AIAgent Agent
	{
		get;
		private set;
	}

	public string Name
	{
		get { return "Attack"; }
	}

	public string Info
	{
		get 
		{
			if(Agent.TARGET != null)
			{
				if(Vector2.Distance(Agent.transform.position, Agent.TARGET.transform.position) > Agent.AttackRange)
					return "Moving to target " + Agent.TARGET;
				else
					return "Hitting target " + Agent.TARGET;
			}
			else
				return "Missing target.";
		}
	}

	public AIState_Attack(AIAgent _Agent)
	{
		Agent = _Agent;
	}

	public IAgentState Enter()
	{
		return null;
	}

	public IAgentState Update()
	{
		IAgentState transition = GetTransition();
		if(transition != null)
			return transition;

		if(!Agent.HasValidPath() || Vector2.Distance(Agent.GetPathGoalPoint(), Agent.TARGET.transform.position) > Agent.AttackRange)
		{
			Agent.UpdatePathTo(Agent.TARGET.transform.position);
		}

		if(Vector2.Distance(Agent.transform.position, Agent.TARGET.transform.position) > Agent.AttackRange)
		{
			Agent.MoveAlongCurrentPath();
		}
		else
		{
			Agent.WalkDirection = (Agent.TARGET.transform.position - Agent.transform.position).normalized;
			Agent.INPUT.SetAxis("Horizontal", 0.0f);
			Agent.INPUT.SetAxis("Vertical", 0.0f);

			Agent.PressButton("Action1", Agent.ReactionTime);
		}

		Agent.RotateView((Agent.TARGET.transform.position - Agent.transform.position).normalized);

		return null;
	}

	public void Exit()
	{

	}

	private IAgentState GetTransition()
	{
		// Get closest player in sight
		Agent.TargetClosestPlayer();

		// TODO: Flee from target that is too far away

		// Start wandering if no target found
		if(Agent.TARGET == null)
		{
			Agent.Log("Transitioning from " + Name + ": lost target.");
			return new AIState_Wander(Agent);
		}

		return null;
	}
}