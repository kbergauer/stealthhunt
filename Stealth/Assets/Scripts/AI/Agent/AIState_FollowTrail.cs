﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class AIState_FollowTrail : IAgentState
{
	public AIAgent Agent
	{
		get;
		private set;
	}

	public string Name
	{
		get { return "Follow Trail"; }
	}

	public string Info
	{
		get
		{
			if(trail != null)
			{
				return "Moving to trail point " + detectedPoint + " of " + trail.GetLength() + " of age " + trail.GetAge();
			}
			else
			{
				return "Missing valid trail.";
			}
		}
	}

	private Trail trail = null;
	private Vector2[] path = null;
	private int detectedPoint = 0;
	private int currentLastPoint = 0;
	private bool reachedTrail = false;

	public AIState_FollowTrail(AIAgent _Agent, Trail _Trail)
	{
		Agent = _Agent;
		trail = _Trail;
	}

	public IAgentState Enter()
	{
		// Get furthest visible point in trail
		detectedPoint = Agent.WORLD.GetLastSeenPointInTrail(trail, Agent.transform.position, Agent.LookDirection, Agent.ViewAngle, Agent.ViewDistance);
		if(detectedPoint < 0 || detectedPoint == trail.GetLength() - 1)
			return new AIState_Wander(Agent);
		
		currentLastPoint = detectedPoint;

		// Move agent to detected point
		Agent.UpdatePathTo(trail.GetTrail()[detectedPoint]);

		return null;
	}

	public IAgentState Update()
	{
		// Check transitions
		IAgentState transition = GetTransition();
		if(transition != null)
			return transition;

		// Move to detected trail point
		if(!reachedTrail && Agent.HasReachedPathGoal())
		{
			// Set path to trail
			reachedTrail = true;
			Agent.SetPath(path);
		}
		else
		{
			// Get last seen point in trail
			currentLastPoint = Agent.WORLD.GetLastSeenPointInTrail(trail, Agent.transform.position, Agent.LookDirection, Agent.ViewAngle, Agent.ViewDistance);

			// Stop following the trail if the end is seen
			if(currentLastPoint == trail.GetLength() - 1 || currentLastPoint < detectedPoint)
			{
				Agent.Log("Transitioning from " + Name + ": reached the end of the trail.");
				return new AIState_Wander(Agent);
			}
			// Update the path to the last point in view
			else if(currentLastPoint > 0 && currentLastPoint > detectedPoint)
			{
				detectedPoint = currentLastPoint;
				Agent.UpdatePathTo(trail.GetTrail()[detectedPoint]);
			}
		}

		// Move and look along path
		Agent.MoveAlongCurrentPath();
		Agent.RotateView(Agent.WalkDirection);

		return null;
	}

	public void Exit()
	{

	}

	public IAgentState GetTransition()
	{
		// Attack enemy in sight
		if(Agent.GetPlayersInView().Length > 0)
		{
			Agent.Log("Transitioning from " + Name + ": enemy in view.");
			return new AIState_Attack(Agent);
		}

		// Follow younger trail in view
		Trail youngest = Agent.WORLD.GetYoungestTrailInView(Agent, Agent.ViewAngle, Agent.ViewDistance);
		if(youngest != null && youngest.GetAge() < 30.0f && trail != youngest)
		{
			Agent.Log("Transitioning from " + Name + ": found a younger trail: originator=" + youngest.GetOriginator() + ", start=" + youngest.GetTrail()[0] + ", length=" + youngest.GetLength() + ", age=" + youngest.GetAge());
			return new AIState_FollowTrail(Agent, youngest);
		}

		// Start wandering if trail end reached
		if((reachedTrail && Agent.HasReachedPathGoal()) || trail == null)
		{
			Agent.Log("Transitioning from " + Name + ": " + (trail == null ? "lost the trail." : "reached the end of the trail."));
			return new AIState_Wander(Agent);
		}

		return null;
	}
}