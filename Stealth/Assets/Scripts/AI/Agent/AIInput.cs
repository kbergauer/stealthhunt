﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class used to fake input for AI control
/// </summary>
public class AIInput : MonoBehaviour, IInput 
{
	public float ButtonReleaseTime = 0.1f;



	private PlayerInput.EDevice device = PlayerInput.EDevice.Keyboard;

	private bool ignoreAxes = false;
	private bool ignoreButtons = false;

	private List<string> lockedAxes = new List<string>();



	// AI input storage
	private List<string> pressedButtons = new List<string>();
	private Dictionary<string, float> axes = new Dictionary<string, float>();



	public PlayerInput.EDevice Device
	{
		get { return device; }
		set { device = value; }
	}



	public void Lock()
	{
		LockAllAxes();
		LockAllButtons();
	}

	public void Unlock()
	{
		UnlockAllAxes();
		UnlockAllButtons();
	}

	public void LockAxes(params string[] _Axes)
	{
		for(int i = 0; i < _Axes.Length; i++)
		{
			if(!lockedAxes.Contains(_Axes[i]))
				lockedAxes.Add(_Axes[i]);
		}
	}

	public void LockAllAxes()
	{
		ignoreAxes = true;
	}

	public void UnlockAxes(params string[] _Axes)
	{
		for(int i = 0; i < _Axes.Length; i++)
		{
			if(lockedAxes.Contains(_Axes[i]))
				lockedAxes.Remove(_Axes[i]);
		}
	}

	public void UnlockAllAxes()
	{
		ignoreAxes = false;
		lockedAxes.Clear();
	}

	public void LockAllButtons()
	{
		ignoreButtons = true;
	}

	public void UnlockAllButtons()
	{
		ignoreButtons = false;
	}

	public float GetAxis(string _Axis)
	{
		float axis = 0.0f;

		if(!ignoreAxes)
		{
			if(axes.ContainsKey(_Axis) && !lockedAxes.Contains(_Axis))
				axis = axes[_Axis];
		}

		return axis;
	}

	public float GetAxisRaw(string _Axis)
	{
		/*
		float axis = GetAxis(_Axis);

		if(axis != 0.0f)
			axis = Mathf.Sign(axis);

		return axis;
		*/

		return GetAxis(_Axis);
	}

	public bool GetButton(string _Button)
	{
		if(!ignoreButtons && pressedButtons.Contains(_Button))
			return true;

		return false;
	}

	public bool GetButtonDown(string _Button)
	{
		return GetButton(_Button);
	}

	public bool GetButtonUp(string _Button)
	{
		return GetButton(_Button);
	}

	public Vector2 GetMousePosition()
	{
		return Vector2.zero;
	}



	public void PressButton(string _Button)
	{
		if(!pressedButtons.Contains(_Button))
		{
			pressedButtons.Add(_Button);
			StartCoroutine(DelayedButtonRelease(_Button));
		}
	}

	public void ReleaseButton(string _Button)
	{
		if(pressedButtons.Contains(_Button))
			pressedButtons.Remove(_Button);
	}

	public void SetAxis(string _Name, float _Value)
	{
		if(!axes.ContainsKey(_Name))
		{
			axes.Add(_Name, _Value);
		}
		else
		{
			axes[_Name] = _Value;
		}
	}

	public void Reset()
	{
		axes.Clear();
		pressedButtons.Clear();
	}

	private IEnumerator DelayedButtonRelease(string _Button)
	{
		yield return new WaitForSeconds(ButtonReleaseTime);
		ReleaseButton(_Button);
	}
}