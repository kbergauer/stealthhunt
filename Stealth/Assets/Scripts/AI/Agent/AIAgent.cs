﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AIAgent : MonoBehaviour 
{
	[Header("Movement")]
	public float DistanceThreshold = 0.25f;
	public bool UseSimplifiedPath = true;

	[Header("Vision")]
	public float ViewDistance = 15.0f;
	public float ViewAngle = 120.0f;

	[Header("Combat")]
	public float ReactionTime = 0.25f;
	public float AttackRange = 1.0f;

	[Header("Debug Gizmos")]
	public bool ShowPathGizmo = false;
	public bool ShowFOVGizmo = false;
	public bool ShowInSightGizmo = false;
	public bool ShowLogs = false;

	private Vector2 mapSize = Vector2.one;
	private Vector2[] path = null;
	private int currentNode = 0;

	private Vector2 lookDirection = Vector2.zero;

	private Player[] players;

	private float colliderRadius = 0.51f;

	private IAgentState currentState;



	public AIInput INPUT
	{
		get;
		private set;
	}

	public Player PLAYER
	{
		get;
		private set;
	}

	public Player TARGET
	{
		get;
		private set;
	}

	public AIMemory MEMORY
	{
		get;
		private set;
	}

	public AIWorldState WORLD
	{
		get;
		private set;
	}

	public Vector2 WalkDirection
	{
		get;
		set;
	}

	public Vector2 LookDirection
	{
		get { return lookDirection; }
	}



	void OnDrawGizmos()
	{
		if(PLAYER != null)
			Gizmos.color = PLAYER.PersonalColor;
		else
			Gizmos.color = Color.white;

		// Visualize current path
		if(ShowPathGizmo)
		{
			if(path != null && path.Length > 1)
			{
				for(int i = 0; i < path.Length - 1; i++)
				{
					Gizmos.DrawLine(path[i], path[i + 1]);
				}

				if(currentNode < path.Length)
				{
					Gizmos.DrawSphere(path[currentNode], 0.1f);
					if(currentNode > 0)
						Gizmos.DrawSphere(path[currentNode - 1], 0.1f);
				}
			}
		}

		// Visualize field of view
		if(ShowFOVGizmo)
		{
			Gizmos.DrawLine(transform.position, transform.position + new Vector3(lookDirection.x, lookDirection.y) * ViewDistance);
			Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(-ViewAngle / 2.0f, Vector3.forward) * new Vector3(lookDirection.x, lookDirection.y) * ViewDistance);
			Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(ViewAngle / 2.0f, Vector3.forward) * new Vector3(lookDirection.x, lookDirection.y) * ViewDistance);
			Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(-ViewAngle / 4.0f, Vector3.forward) * new Vector3(lookDirection.x, lookDirection.y) * ViewDistance);
			Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(ViewAngle / 4.0f, Vector3.forward) * new Vector3(lookDirection.x, lookDirection.y) * ViewDistance);
		}

		// Visualize player sight lines
		if(ShowInSightGizmo)
		{
			for(int i = 0; i < players.Length; i++)
			{
				if(IsPlayerInView(players[i]))
				{
					Gizmos.color = Color.green;
				}
				else
				{
					Gizmos.color = Color.red;
				}

				Gizmos.DrawLine(transform.position, players[i].transform.position);
			}
		}
	}

	void Awake()
	{
		INPUT = gameObject.AddComponent<AIInput>();
		INPUT.Device = PlayerInput.EDevice.Any;

		PLAYER = GetComponent<Player>();

		CircleCollider2D circle = GetComponent<CircleCollider2D>();
		colliderRadius = circle.radius + 0.01f;

		MapGenerator generator = FindObjectOfType<MapGenerator>();
		if(generator != null)
		{
			mapSize.x = generator.Width - 1.0f;
			mapSize.y = generator.Height - 1.0f;
		}

		TARGET = null;
		MEMORY = new AIMemory();
		WORLD = AIWorldState.GetInstance();
	}

	void Start()
	{
		List<Player> all = new List<Player>(FindObjectsOfType<Player>());
		all.Remove(PLAYER);
		players = all.ToArray();
	}

	void OnEnable()
	{
		currentState = new AIState_Wander(this);
		Log("[OnEnable] Entering state " + currentState.Name);
		currentState.Enter();
	}

	void OnDisable()
	{
		// Reset instance
		path = null;
		currentNode = 0;

		TARGET = null;

		INPUT.Reset();
	}

	void Update()
	{
		/*
		TargetClosestPlayer();

		if(Target == null)
			Wander();
		else
			Attack();

		RotateView(WalkDirection);
		*/

		MEMORY.Update();

		//Log("Updating state " + currentState.Name + ": " + currentState.Info);
		IAgentState nextState = currentState.Update();
		while(nextState != null)
		{
			Log("Exiting state " + currentState.Name);
			currentState.Exit();
			currentState = nextState;
			Log("Entering state " + currentState.Name);
			nextState = currentState.Enter();
		}
	}



	public Player[] GetPlayersInView()
	{
		List<Player> inView = new List<Player>();

		for(int i = 0; i < players.Length; i++)
		{
			if(!players[i].gameObject.activeInHierarchy)
				continue;

			if(IsPlayerInView(players[i]))
				inView.Add(players[i]);
		}

		return inView.ToArray();
	}

	public Player GetClosestPlayerInView()
	{
		Player[] inView = GetPlayersInView();
		if(inView == null || inView.Length < 1)
			return null;

		Player closest = null;
		float distance = 0.0f;
		float minDistance = float.PositiveInfinity;

		for(int i = 0; i < inView.Length; i++)
		{
			distance = Vector2.Distance(transform.position, inView[i].transform.position);
			if(distance < minDistance)
			{
				closest = inView[i];
				minDistance = distance;
			}
		}

		return closest;
	}

	public bool IsPlayerInView(Player _Player)
	{
		if(_Player == null || !_Player.gameObject.activeInHierarchy)
			return false;

		Vector2 direction = (_Player.transform.position - transform.position).normalized;
		if(Vector2.Angle(direction, lookDirection) > ViewAngle / 2.0f)
			return false;

		RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(direction.x, direction.y) * colliderRadius, direction, ViewDistance);
		//Debug.DrawRay(transform.position + new Vector3(direction.x, direction.y) * colliderRadius, direction * ViewDistance, Color.yellow);
		if(hit.collider != null && hit.collider.gameObject == _Player.gameObject)
			return true;

		return false;
	}

	public void TargetClosestPlayer()
	{
		// Get closest target
		Player closest = GetClosestPlayerInView();

		// Switch if closest is not current target
		if(closest != TARGET)
		{
			TARGET = closest;
			path = null;
		}
	}

	public void RotateView(Vector2 _Direction)
	{
		// Rotate view to walk direction
		lookDirection.x = Mathf.Lerp(lookDirection.x, _Direction.x, 0.1f);
		lookDirection.y = Mathf.Lerp(lookDirection.y, _Direction.y, 0.1f);
		INPUT.SetAxis("Horizontal2", lookDirection.x);
		INPUT.SetAxis("Vertical2", lookDirection.y);
	}

	public void MoveAlongCurrentPath()
	{
		if(path == null || currentNode >= path.Length)
			return;

		// Move to next node if current has been reached
		if(Vector2.Distance(transform.position, path[currentNode]) <= DistanceThreshold)
		{
			currentNode++;
		}

		if(currentNode < path.Length)
		{
			// Get direction to current path node
			WalkDirection = (path[currentNode] - new Vector2(transform.position.x, transform.position.y)).normalized;
			INPUT.SetAxis("Horizontal", WalkDirection.x);
			INPUT.SetAxis("Vertical", WalkDirection.y);
		}
		else
		{
			INPUT.SetAxis("Horizontal", 0.0f);
			INPUT.SetAxis("Vertical", 0.0f);
		}
	}

	/*
	private void Wander()
	{
		// Get random point on map to walk to
		if(path == null || currentNode >= path.Length)
		{
			Vector2 goal = Pathfinding.GetRandomDestination();
			path = Pathfinding.GetPath(transform.position, goal);
			currentNode = 0;
		}
		// Move to next point of path
		else
		{
			MoveAlongCurrentPath();		
		}
	}

	private void Attack()
	{
		if(path == null || path.Length < 1 || Vector2.Distance(path[path.Length - 1], Target.transform.position) > AttackRange)
		{
			UpdatePathTo(Target.transform.position);
		}

		if(Vector2.Distance(transform.position, Target.transform.position) > AttackRange)
		{
			MoveAlongCurrentPath();
		}
		else
		{
			WalkDirection = (Target.transform.position - transform.position).normalized;
			Input.SetAxis("Horizontal", 0.0f);
			Input.SetAxis("Vertical", 0.0f);

			PressButton("Action1", ReactionTime);
		}
	}
	*/

	public void PressButton(string _Button)
	{
		INPUT.PressButton(_Button);
	}

	public void PressButton(string _Button, float _Delay)
	{
		StartCoroutine(DelayedButtonPress(_Button, ReactionTime));
	}

	private IEnumerator DelayedButtonPress(string _Button, float _Delay)
	{
		yield return new WaitForSeconds(_Delay);
		PressButton(_Button);
	}

	public void UpdatePathTo(Vector2 _Goal)
	{
		if(Pathfinding.IsReady())
		{
			if(UseSimplifiedPath)
				path = Pathfinding.GetSimplifiedPath(transform.position, _Goal, 2, colliderRadius);
			else
				path = Pathfinding.GetPath(transform.position, _Goal);

			currentNode = 0;
		}
	}

	public void SetPath(Vector2[] _Path)
	{
		path = _Path;
	}

	public bool HasValidPath()
	{
		return path != null && path.Length > 1;
	}

	public bool HasReachedPathGoal()
	{
		return path == null || currentNode >= path.Length;
	}

	public Vector2 GetPathPoint(int _Index)
	{
		if(_Index >= 0 && _Index < path.Length)
			return path[_Index];

		return Vector2.zero;
	}

	public Vector2 GetPathGoalPoint()
	{
		return GetPathPoint(path.Length - 1);
	}



	public void Log(string _Message)
	{
		if(!ShowLogs)
			return;

		Debug.Log("<" + this.name + "> " + _Message);
	}
}