﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class for playing one shot clips with custom settings.
/// TODO: Add object pool for audio sources.
/// </summary>
static public class SoundUtility 
{
	static public float Z = -10;
	static public bool Flat = false;

	static public void PlayAt(AudioClip _Clip, Vector3 _Position)
	{
		if(_Clip == null)
			return;

		float volume = PlayerPrefs.GetFloat("SoundVolume");
		if(volume <= 0.0f)
			return;

		GameObject obj = new GameObject(_Clip.name + "SingleSource");
		AudioSource sound = obj.AddComponent<AudioSource>();

		if(!Flat)
		{
			_Position.z = Z;
			obj.transform.position = _Position;
		}

		sound.clip = _Clip;
		sound.volume = volume;
		sound.loop = false;
		sound.maxDistance = 50.0f;
		sound.spatialBlend = 1.0f;
		sound.Play();

		GameObject.Destroy(obj, _Clip.length);
	}

	static public AudioSource LoopAt(AudioClip _Clip, Vector3 _Position)
	{
		if(_Clip == null)
			return null;

		float volume = PlayerPrefs.GetFloat("SoundVolume");
		if(volume <= 0.0f)
			return null;

		GameObject obj = new GameObject(_Clip.name + "LoopingSource");
		AudioSource sound = obj.AddComponent<AudioSource>();

		if(!Flat)
		{
			_Position.z = Z;
			obj.transform.position = _Position;
		}

		sound.clip = _Clip;
		sound.volume = volume;
		sound.loop = true;
		sound.Play();

		return sound;
	}
}