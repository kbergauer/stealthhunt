﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ObjectPool 
{
	private Transform parent;

	private GameObject[] prefabs;
	private int capacity;
	private List<IPoolable> pool;



	public ObjectPool()
	{
		capacity = 0;
		prefabs = null;
		pool = null;
	}

	public ObjectPool(int _Capacity, GameObject _Prefab, string _Name = "Pool", Transform _Parent = null)
	{
		// Save parameters
		capacity = Mathf.Max(0, _Capacity);
		prefabs = new GameObject[] { _Prefab };
		pool = new List<IPoolable>(capacity);

		// Create parent object
		GameObject obj = new GameObject(_Name);
		parent = obj.transform;
		parent.SetParent(_Parent);

		// Fill pool with objects
		obj = null;
		for(int i = 0; i < capacity; i++)
		{
			obj = GameObject.Instantiate(prefabs[0]);
			pool.Add(obj.GetInterface<IPoolable>());
			obj.transform.SetParent(parent);
			obj.SetActive(false);
		}
	}

	public ObjectPool(int _Capacity, GameObject[] _Prefabs, string _Name = "Pool", Transform _Parent = null)
	{
		// Save parameters
		capacity = Mathf.Max(0, _Capacity);
		prefabs = _Prefabs;
		pool = new List<IPoolable>(capacity);

		// Create parent object
		GameObject obj = new GameObject(_Name);
		parent = obj.transform;
		parent.SetParent(_Parent);

		// Fill pool with objects
		for(int i = 0; i < capacity; i++)
		{
			if(prefabs.Length == 1)
			{
				pool.Add(GameObject.Instantiate(prefabs[0]).GetInterface<IPoolable>());
			}
			else
			{
				pool.Add(GameObject.Instantiate(prefabs[Random.Range(0, prefabs.Length)]).GetInterface<IPoolable>());
			}
			pool[i].Reference.transform.SetParent(parent);
			pool[i].Reference.SetActive(false);
		}
	}



	public bool Usable
	{
		get { return prefabs != null && prefabs.Length > 0 && pool != null; }
	}



	public void DestroyPool()
	{
		if(!Usable)
			return;

		if(parent.gameObject != null)
			GameObject.Destroy(parent.gameObject);
		parent = null;

		pool.Clear();
		pool = null;

		prefabs = null;
	}

	public void DeactivateObject(IPoolable _Object)
	{
		if(!Usable)
			return;

		if(pool.Contains(_Object))
		{
			_Object.Deactivate();
		}
	}

	public void DeactivateAll()
	{
		if(!Usable)
			return;

		for(int i = 0; i < pool.Count; i++)
		{
			if(pool[i].Reference.activeInHierarchy)
			{
				pool[i].Deactivate();
			}
		}
	}

	public GameObject Instantiate()
	{
		if(!Usable)
			return null;

		// Get inactive object to use
		for(int i = 0; i < pool.Count; i++)
		{
			if(!pool[i].Reference.activeInHierarchy)
			{
				pool[i].Activate();
				return pool[i].Reference;
			}
		}

		// Create new instance to return
		if(prefabs.Length == 1)
		{
			pool.Add(GameObject.Instantiate(prefabs[0]).GetInterface<IPoolable>());
		}
		else
		{
			pool.Add(GameObject.Instantiate(prefabs[Random.Range(0, prefabs.Length)]).GetInterface<IPoolable>());
		}
		pool[pool.Count - 1].Activate();
		pool[pool.Count - 1].Reference.transform.SetParent(parent);
		return pool[pool.Count - 1].Reference;
	}
}