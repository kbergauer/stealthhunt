﻿using UnityEngine;
using System.Collections;

public class MirrorPosition : MonoBehaviour 
{
	public Transform Target;
	public bool IgnoreX = false;
	public bool IgnoreY = false;
	public bool IgnoreZ = false;

	private Vector3 position;

	void OnEnable()
	{
		position = transform.position;
	}

	void FixedUpdate()
	{
		if(Target == null)
		{
			enabled = false;
			return;
		}

		if(!IgnoreX)
			position.x = Target.position.x;
		if(!IgnoreY)
			position.y = Target.position.y;
		if(!IgnoreZ)
			position.z = Target.position.z;

		transform.position = position;
	}
}