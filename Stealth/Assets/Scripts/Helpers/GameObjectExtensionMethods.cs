﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Class containing generic extension methods for the GameObject class.
/// </summary>
public static class GameObjectExtensionMethods
{
	/// <summary>
	/// Returns the first interface of type T found on object _GameObject.
	/// <para>Equally heavy on performance as GetComponents; avoid extensive use.</para>
	/// </summary>
	/// <typeparam name="T">The interface type to return.</typeparam>
	/// <param name="_GameObject">The object to get the interface of.</param>
	/// <returns>The first interface of type T on object _GameObject.</returns>
	public static T GetInterface<T>(this GameObject _GameObject) where T : class
	{
		if(!typeof(T).IsInterface)
		{
			Debug.LogError(typeof(T).ToString() + " is not an interface! GetInterface<T> does not work with other types.");
			return null;
		}

		return _GameObject.GetComponents<Component>().OfType<T>().FirstOrDefault();
	}

	/// <summary>
	/// Returns the all interfaces of type T found on object _GameObject.
	/// <para>Equally heavy on performance as GetComponents; avoid extensive use.</para>
	/// </summary>
	/// <typeparam name="T">The interface type to return.</typeparam>
	/// <param name="_GameObject">The object to get the interfaces of.</param>
	/// <returns>Collection of all interfaces of type T on object _GameObject.</returns>
	public static T[] GetInterfaces<T>(this GameObject _GameObject) where T : class
	{
		if(!typeof(T).IsInterface)
		{
			Debug.LogError(typeof(T).ToString() + " is not an interface! GetInterfaces<T> does not work with other types.");
			return null;
		}

		return _GameObject.GetComponents<Component>().OfType<T>().ToArray<T>();
	}
}