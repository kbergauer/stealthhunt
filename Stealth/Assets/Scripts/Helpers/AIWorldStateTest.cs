﻿using UnityEngine;
using System.Collections.Generic;

public class AIWorldStateTest : MonoBehaviour 
{
	public float ViewAngle = 120.0f;
	public float ViewDistance = 15.0f;

	private Vector3 lookDirection = Vector3.zero;

	private AIAgent agent = null;
	private AIWorldState world = null;
	private Chest[] chestsInView = null;
	private Trail[] trailsInView = null;

	void OnDrawGizmos()
	{
		// Visualize field of view
		Gizmos.color = Color.white;
		Gizmos.DrawSphere(transform.position, 0.5f);
		Gizmos.DrawLine(transform.position, transform.position + lookDirection * ViewDistance);
		Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(-ViewAngle / 2.0f, Vector3.forward) * lookDirection * ViewDistance);
		Gizmos.DrawLine(transform.position, transform.position + Quaternion.AngleAxis(ViewAngle / 2.0f, Vector3.forward) * lookDirection * ViewDistance);
		Gizmos.DrawLine(transform.position + lookDirection * ViewDistance, transform.position + Quaternion.AngleAxis(-ViewAngle / 2.0f, Vector3.forward) * lookDirection * ViewDistance);
		Gizmos.DrawLine(transform.position + lookDirection * ViewDistance, transform.position + Quaternion.AngleAxis(ViewAngle / 2.0f, Vector3.forward) * lookDirection * ViewDistance);

		// Highlight chests in view
		if(chestsInView != null)
		{
			Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 0.5f);
			for(int i = 0; i < chestsInView.Length; i++)
			{
				Gizmos.DrawSphere(chestsInView[i].transform.position, 1.0f);
			}
		}

		// Highlight trails in view
		if(trailsInView != null)
		{
			Gizmos.color = Color.magenta;
			List<Vector2> path = null;
			for(int i = 0; i < trailsInView.Length; i++)
			{
				path = trailsInView[i].GetTrail();
				for(int node = 0; node < path.Count - 1; node++)
				{
					Gizmos.DrawLine(path[node], path[node + 1]);
				}
			}
		}
	}

	void Start()
	{
		agent = GetComponent<AIAgent>();
		world = AIWorldState.GetInstance();
	}

	void Update()
	{
		// Get look direction
		lookDirection = new Vector3(transform.up.x, transform.up.y, 0.0f);
		lookDirection.Normalize();

		// Get chests in view
		chestsInView = world.GetAllChestsInView(transform.position, lookDirection, ViewAngle, ViewDistance);

		// Get trails in view
		if(agent != null)
		{
			trailsInView = world.GetTrailsInView(agent, ViewAngle, ViewDistance);
		}
	}
}