﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class ScreenshotUtility : MonoBehaviour 
{
	void Awake()
	{
		DontDestroyOnLoad(gameObject);

		if(!Directory.Exists(Application.dataPath + "/../Screenshots/"))
			Directory.CreateDirectory(Application.dataPath + "/../Screenshots/");
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.F11) || Input.GetMouseButtonDown(2))
		{
			Application.CaptureScreenshot(Application.dataPath + "/../Screenshots/screenshot_" + DateTime.Now.Hour.ToString("00") + "-" + DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00") + ".png");
		}
	}
}