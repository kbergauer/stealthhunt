﻿using UnityEngine;
using System.Collections;

public class RandomHue : MonoBehaviour 
{
	void Awake()
	{
		SpriteRenderer rend = GetComponent<SpriteRenderer>();
		rend.color = HSV.ToRGB(Random.Range(0.0f, 360.0f), 0.5f, 1.0f);
	}
}