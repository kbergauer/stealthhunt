﻿using UnityEngine;
using System.Collections;

public class CopyHue : MonoBehaviour 
{
	public SpriteRenderer Target;
	public float Value = 1.0f;

	void Start()
	{
		SpriteRenderer self = GetComponent<SpriteRenderer>();
		if(self == null || Target == null)
			return;

		HSV color = HSV.FromRGB(Target.color);
		color.Value = Value;

		self.color = color.ToColor();
	}
}