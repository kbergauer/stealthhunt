﻿using UnityEngine;
using System.Collections;

public class HSV 
{
	public float Hue = 0.0f;
	public float Saturation = 1.0f;
	public float Value = 1.0f;
	public float Alpha = 1.0f;

	public Color ToColor()
	{
		return HSV.ToRGB(Hue, Saturation, Value, Alpha);
	}



	static public Color ToRGB(float _H, float _S, float _V, float _A = 1.0f)
	{
		Color result = new Color();
		result.a = _A;

		if(_S == 0.0f)
		{
			result.r = _V;
			result.g = _V;
			result.b = _V;
			return result;
		}

		int i;
		float f;
		float p;
		float q;
		float t;

		_H /= 60.0f;
		i = Mathf.FloorToInt(_H);
		f = _H - i;
		p = _V * (1 - _S);
		q = _V * (1 - _S * f);
		t = _V * (1 - _S * (1 - f));

		switch(i)
		{
			case 0:
				{
					result.r = _V;
					result.g = t;
					result.b = p;
				} break;

			case 1:
				{
					result.r = q;
					result.g = _V;
					result.b = p;
				} break;

			case 2:
				{
					result.r = p;
					result.g = _V;
					result.b = t;
				} break;

			case 3:
				{
					result.r = p;
					result.g = q;
					result.b = _V;
				} break;

			case 4:
				{
					result.r = t;
					result.g = p;
					result.b = _V;
				} break;

			default:
				{
					result.r = _V;
					result.g = p;
					result.b = q;
				} break;
		}

		return result;
	}

	static public Color ToRGB(HSV _HSV)
	{
		return ToRGB(_HSV.Hue, _HSV.Saturation, _HSV.Value, _HSV.Alpha);
	}



	static public HSV FromRGB(float _R, float _G, float _B, float _A = 1.0f)
	{
		HSV result = new HSV();
		result.Alpha = _A;

		float min = Mathf.Min(_R, _G, _B);
		float max = Mathf.Max(_R, _G, _B);
		float delta = max - min;

		if(max != 0)
		{
			result.Saturation = delta / max;
		}
		else
		{
			result.Saturation = 0.0f;
			result.Hue = -1.0f;
			return result;
		}

		if(_R == max)
		{
			result.Hue = (_G - _B) / delta;
		}
		else if(_G == max)
		{
			result.Hue = 2 + (_B - _R) / delta;
		}
		else
		{
			result.Hue = 4 + (_R - _G) / delta;
		}

		result.Hue *= 60.0f;
		if(result.Hue < 0.0f)
			result.Hue += 360.0f;

		return result;
	}

	static public HSV FromRGB(Color _RGB)
	{
		return HSV.FromRGB(_RGB.r, _RGB.g, _RGB.b, _RGB.a);
	}
}