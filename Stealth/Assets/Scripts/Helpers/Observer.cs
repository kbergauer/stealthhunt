﻿using UnityEngine;
using System.Collections;

public class Observer : MonoBehaviour 
{
	private Camera cam;
	private MirrorPosition mirror;
	private Player[] players;

	private int observed = -1;

	void Start()
	{
		cam = GetComponent<Camera>();
		mirror = GetComponent<MirrorPosition>();
		players = FindObjectsOfType<Player>();
	}

	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			observed++;
			if(observed > players.Length - 1)
				observed = 0;
		}
		if(Input.GetMouseButtonDown(1))
		{
			observed--;
			if(observed < 0)
				observed = players.Length - 1;
		}
		if(Input.GetKeyDown(KeyCode.Space))
		{
			observed = -1;
		}

		if(observed < 0 && mirror.Target != null)
		{
			mirror.Target = null;
			transform.position = new Vector3(0.0f, 0.0f, transform.position.z);
			cam.orthographicSize = 25;
		}
		else if(observed >= 0 && mirror.Target != players[observed].transform)
		{
			mirror.Target = players[observed].transform;
			mirror.enabled = true;
			cam.orthographicSize = 10;
		}
	}
}