﻿using UnityEngine;
using System.Collections.Generic;

public class FootprintTrailCreator : MonoBehaviour
{
	public bool Cleanable = false;
	public int Priority = 0;
	public GameObject LeftFootprint;
	public GameObject RightFootprint;
	public float Duration = 1.0f;

	private Rigidbody2D target;
	private Player player;
	private Vector3 direction = Vector3.zero;
	private float currentDuration = 0.0f;

	private Trail trail;

	void Start()
	{
		trail = new Trail(GetComponent<Player>());
		AIWorldState.GetInstance().AddTrail(trail);

		target = GetComponent<Rigidbody2D>();
		if(target == null)
		{
			Destroy(this);
			return;
		}

		player = GetComponent<Player>();
		player.OnLeftStep += OnLeftStep;
		player.OnRightStep += OnRightStep;
		Destroy(this, Duration);
	}

	void OnDestroy()
	{
		if(player != null)
		{
			player.OnLeftStep -= OnLeftStep;
			player.OnRightStep -= OnRightStep;
		}
	}

	void Update()
	{
		if(target == null)
			Destroy(this);

		currentDuration += GameManager.deltaTime;
	}

	private void OnLeftStep()
	{
		UpdateDirection();
		GameObject step = PoolManager.LeftFootprints.Instantiate();
		step.transform.position = transform.position + Quaternion.AngleAxis(90.0f, Vector3.forward) * direction * 0.3f;
		SetupFootprint(step);

		trail.Add(transform.position);
	}

	private void OnRightStep()
	{
		UpdateDirection();
		GameObject step = PoolManager.RightFootprints.Instantiate();
		step.transform.position = transform.position + Quaternion.AngleAxis(-90.0f, Vector3.forward) * direction * 0.3f;
		SetupFootprint(step);

		trail.Add(transform.position);
	}

	private void SetupFootprint(GameObject _Step)
	{
		_Step.transform.rotation = GetStepRotation();

		SpriteRenderer sprite = _Step.GetComponent<SpriteRenderer>();
		if(sprite != null)
		{
			Color color = sprite.color;
			color.a = 1.0f - (currentDuration / Duration);
			sprite.color = color;
		}
	}

	private Quaternion GetStepRotation()
	{
		float angle = Vector2.Angle(Vector3.up, direction);
		if((transform.position + direction).x > transform.position.x)
			angle = -angle;
		return Quaternion.AngleAxis(angle, Vector3.forward);
	}

	private void UpdateDirection()
	{
		if(target.velocity.x == 0.0f && target.velocity.y == 0.0f)
			return;

		direction.x = target.velocity.x;
		direction.y = target.velocity.y;
		direction.Normalize();
	}
}