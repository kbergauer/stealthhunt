﻿using UnityEngine;
using System.Collections;

public class CorpseDecay : MonoBehaviour, IPoolable
{
	public float Delay = 60.0f;
	public float Speed = 1.0f;

	public BloodSplatter Blood;

	private SpriteRenderer[] renderers;



	public GameObject Reference
	{
		get { return gameObject; }
	}



	void Awake()
	{
		renderers = GetComponentsInChildren<SpriteRenderer>();
	}

	public void Activate()
	{
		Color c;
		for(int i = 0; i < renderers.Length; i++)
		{
			c = renderers[i].color;
			c.a = 1.0f;
			renderers[i].color = c;
		}

		gameObject.SetActive(true);
		StartCoroutine(DelayedDecay());

		if(Blood != null)
			Blood.Activate();
	}

	public void Deactivate()
	{
		if(Blood != null)
			Blood.Deactivate();

		StopAllCoroutines();
		gameObject.SetActive(false);
	}

	private IEnumerator DelayedDecay()
	{
		yield return new WaitForSeconds(Delay);
		StartCoroutine(Decay());
	}

	private IEnumerator Decay()
	{
		float alpha = renderers[0].color.a;
		Color color;

		while(alpha > 0.0f)
		{
			alpha -= GameManager.deltaTime * Speed;

			for(int i = 0; i < renderers.Length; i++)
			{
				color = renderers[i].color;
				color.a = alpha;
				renderers[i].color = color;
			}

			yield return new WaitForEndOfFrame();
		}

		Deactivate();
	}
}