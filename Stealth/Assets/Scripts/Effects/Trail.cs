﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Trail 
{
	private Player originator = null;
	private float startTime = 0.0f;
	private List<Vector2> trail;



	public Trail(Player _Originator)
	{
		originator = _Originator;
		startTime = GameManager.time;
		trail = new List<Vector2>();
	}



	public Player GetOriginator()
	{
		return originator;
	}

	public float GetAge()
	{
		return GameManager.time - startTime;
	}

	public List<Vector2> GetTrail()
	{
		return trail;
	}

	public int GetLength()
	{
		return trail.Count;
	}

	public void Add(Vector2 _Point)
	{
		trail.Add(_Point);
	}
}