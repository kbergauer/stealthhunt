﻿using UnityEngine;
using System.Collections;

public class DespawnAfterTime : MonoBehaviour 
{
	public float DespawnTime = 1.0f;
	public float DespawnSpeed = 1.0f;

	private SpriteRenderer[] renderers;

	void Start()
	{
		renderers = GetComponentsInChildren<SpriteRenderer>();

		StartCoroutine(WaitForDespawn());
	}

	private IEnumerator WaitForDespawn()
	{
		yield return new WaitForSeconds(DespawnTime);
		StartCoroutine(Despawn());
	}

	private IEnumerator Despawn()
	{
		float alpha = 1.0f;
		Color temp;
		while(alpha > 0.0f)
		{
			alpha -= GameManager.deltaTime * DespawnSpeed;
			for(int i = 0; i < renderers.Length; i++)
			{
				temp = renderers[i].color;
				temp.a = alpha;
				renderers[i].color = temp;
			}

			yield return new WaitForEndOfFrame();
		}

		Destroy(gameObject);
	}
}