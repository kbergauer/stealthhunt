﻿using UnityEngine;
using System.Collections;

public class BloodSplatter : MonoBehaviour, IPoolable
{
	[Header("Drying")]
	public float ColorTarget = 0.25f;
	public float DrySpeed = 1.0f;
	public float DestructionTime = 0.0f;

	[Header("Footprints")]
	public bool HasTrail = false;
	public GameObject LeftFootprint;
	public GameObject RightFootprint;
	public float TrailDuration = 5.0f;
	public int TrailPriority = 10;

	private SpriteRenderer srend;
	private Color startColor;
	private Color currentColor;
	private bool dried = false;
	private bool removing = false;

	private Collider2D coll;



	public GameObject Reference
	{
		get { return gameObject; }
	}

	public void Activate()
	{
		removing = false;
		dried = false;

		currentColor = startColor;
		srend.color = currentColor;

		gameObject.SetActive(true);

		if(DestructionTime > 0.0f)
			StartCoroutine(RemoveSelf());
	}

	public void Deactivate()
	{
		StopAllCoroutines();
		gameObject.SetActive(false);

		if(coll != null)
			coll.enabled = true;
	}



	void Awake()
	{
		srend = GetComponent<SpriteRenderer>();
		startColor = srend.color;
		currentColor = startColor;

		coll = GetComponent<Collider2D>();
	}

	void Update()
	{
		if(removing || dried)
			return;

		// Stop logic when color target is met
		if(srend.color.r <= ColorTarget)
		{
			srend.color = new Color(ColorTarget, ColorTarget, ColorTarget, srend.color.a);

			if(coll != null && coll.enabled)
				coll.enabled = false;

			dried = true;
			return;
		}

		// Darken sprite tint
		currentColor.r -= GameManager.deltaTime * DrySpeed;
		currentColor.g = currentColor.r;
		currentColor.b = currentColor.r;
		currentColor.a = srend.color.a;
		srend.color = currentColor;
	}

	void OnTriggerEnter2D(Collider2D _Target)
	{
		// Ignore collision if splatter creates no trail
		if(!HasTrail)
			return;

		// Ignore everything that is not a player
		if(!_Target.CompareTag("Player"))
			return;

		// Abort if footprint pools are not usable
		if(!PoolManager.LeftFootprints.Usable || !PoolManager.RightFootprints.Usable)
			return;

		// Check for existing trail
		FootprintTrailCreator trail = _Target.GetComponent<FootprintTrailCreator>();
		if(trail != null)
		{
			// Ignore new trail if priority is lower
			if(trail.Priority > TrailPriority)
				return;
			// Destroy old trail with lower priority
			else
			{
				trail.enabled = false;
				Destroy(trail);
			}
		}
		
		// Create trail
		trail = _Target.gameObject.AddComponent<FootprintTrailCreator>();
		trail.Duration = TrailDuration;
		trail.Priority = TrailPriority;
		trail.Cleanable = true;
	}

	private IEnumerator RemoveSelf()
	{
		yield return new WaitForSeconds(DestructionTime);

		currentColor = srend.color;

		while(currentColor.a > 0.0f)
		{
			currentColor.a -= GameManager.deltaTime * DrySpeed * 2.0f;
			srend.color = currentColor;
			yield return new WaitForEndOfFrame();
		}

		Deactivate();
	}
}
