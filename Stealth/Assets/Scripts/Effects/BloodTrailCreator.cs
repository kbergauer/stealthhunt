﻿using UnityEngine;
using System.Collections.Generic;

public class BloodTrailCreator : MonoBehaviour
{
	public float Duration = 1.0f;
	public Vector2 DripIntervall = new Vector2(0.25f, 0.5f);

	private float currentDuration = 0.0f;
	private float intervall = 0.0f;
	private float timer = 0.0f;
	private Vector3 scale = Vector3.one;

	private Trail trail;



	void Awake()
	{
		trail = new Trail(GetComponent<Player>());
		AIWorldState.GetInstance().AddTrail(trail);
	}

	void Update()
	{
		currentDuration += GameManager.deltaTime;
		if(Duration <= currentDuration)
			Destroy(this);

		timer += GameManager.deltaTime;
		if(timer >= intervall)
		{
			// Drip blood
			DripBlood();

			// Reset timer
			timer = 0.0f;
			intervall = Random.Range(DripIntervall.x, DripIntervall.y);
		}
	}

	private void DripBlood()
	{
		GameObject blood = PoolManager.Blood.Instantiate();
		blood.transform.position = transform.position;
		blood.transform.rotation = Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.forward);
		scale.x = 1.2f - (currentDuration / Duration);
		scale.y = scale.x;
		blood.transform.localScale = scale;

		trail.Add(blood.transform.position);
	}
}