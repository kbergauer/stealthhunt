﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LocalLobby : MonoBehaviour 
{
	[Header("Scene Change")]
	public Image Fade;
	public AudioSource Music;

	[Header("General")]
	public PlayerSlot[] Slots;
	public string GameScene = "House";

	[Header("Settings")]
	public Toggle KillLimit;
	public Toggle TimeLimit;
	public Slider BotCount;
	public Button StartButton;

	private bool[] joined = new bool[] { false, false, false, false };
	private bool[] ready = new bool[] { false, false, false, false };
	private PlayerInput.EDevice[] players = new PlayerInput.EDevice[4];
	private List<PlayerInput.EDevice> available = new List<PlayerInput.EDevice>();

	void Start()
	{
		// Destroy old match setup if one exists
		GameObject obj = GameObject.Find("MatchSetup");
		if(obj != null)
			Destroy(obj);

		for(int i = 1; i <= 5; i++)
			available.Add((PlayerInput.EDevice)i);

		for(int i = 0; i < Slots.Length; i++)
			Slots[i].Panel.color = new Color(0.2f, 0.2f, 0.2f);
	}

	void Update()
	{
		// TODO: Remove debug keys for adding players
		if(Input.GetKeyDown(KeyCode.F1))
		{
			if(available.Contains(PlayerInput.EDevice.Controller1))
				AddPlayer(PlayerInput.EDevice.Controller1);
			else
				RemovePlayer(PlayerInput.EDevice.Controller1);
		}
		if(Input.GetKeyDown(KeyCode.F2))
		{
			if(available.Contains(PlayerInput.EDevice.Controller2))
				AddPlayer(PlayerInput.EDevice.Controller2);
			else
				RemovePlayer(PlayerInput.EDevice.Controller2);
		}
		if(Input.GetKeyDown(KeyCode.F3))
		{
			if(available.Contains(PlayerInput.EDevice.Controller3))
				AddPlayer(PlayerInput.EDevice.Controller3);
			else
				RemovePlayer(PlayerInput.EDevice.Controller3);
		}
		if(Input.GetKeyDown(KeyCode.F4))
		{
			if(available.Contains(PlayerInput.EDevice.Controller4))
				AddPlayer(PlayerInput.EDevice.Controller4);
			else
				RemovePlayer(PlayerInput.EDevice.Controller4);
		}

		if(CountJoinedPlayers() == CountReadyPlayers() && (CountJoinedPlayers() > 0 || BotCount.value > 0))
		{
			if((KillLimit != null && !KillLimit.isOn) && (TimeLimit != null && !TimeLimit.isOn))
			{
				if(StartButton != null)
				{
					StartButton.interactable = false;
				}
			}
			else if(StartButton != null)
			{
				StartButton.interactable = true;
			}
		}
		else if(StartButton != null)
		{
			StartButton.interactable = false;
		}

		if(Input.GetButtonDown("KB_Start"))
		{
			if(available.Contains(PlayerInput.EDevice.Keyboard))
				AddPlayer(PlayerInput.EDevice.Keyboard);
			else
				RemovePlayer(PlayerInput.EDevice.Keyboard);
		}
		else
		{
			for(int i = 1; i <= 4; i++)
			{
				if(Input.GetButtonDown("C" + i + "_Start"))
				{
					if(available.Contains((PlayerInput.EDevice)(i + 1)))
						AddPlayer((PlayerInput.EDevice)(i + 1));
					else
						RemovePlayer((PlayerInput.EDevice)(i + 1));
				}
			}
		}
	}

	private int GetPlayerIndex(PlayerInput.EDevice _Device)
	{
		for(int i = 0; i < players.Length; i++)
		{
			if(players[i] == _Device)
			{
				return i;
			}
		}

		return -1;
	}

	private void AddPlayer(PlayerInput.EDevice _Device)
	{
		for(int i = 0; i < 4; i++)
		{
			if(!joined[i])
			{
				players[i] = _Device;
				joined[i] = true;
				ready[i] = true;
				//ready[i] = false;
				Slots[i].Panel.color = Color.red;
				//Slots[i].Panel.color = Color.yellow;
				Slots[i].Headline.text = _Device.ToString();
				Slots[i].Subline.text = "";
				Slots[i].Subline.text = "Ready";
				//Slots[i].Subline.text = "Not Ready";

				available.Remove(_Device);
				break;
			}
		}
	}

	private void RemovePlayer(PlayerInput.EDevice _Device)
	{
		int index = GetPlayerIndex(_Device);
		if(index < 0 || index > 3)
			return;

		available.Add(players[index]);
		joined[index] = false;
		ready[index] = false;
		Slots[index].Panel.color = new Color(0.2f, 0.2f, 0.2f);
		Slots[index].Headline.text = "Join";
		Slots[index].Subline.text = "Space / Start";
	}

	private void ReadyPlayer(PlayerInput.EDevice _Device)
	{
		int index = GetPlayerIndex(_Device);
		if(index < 0 || index > 3)
			return;

		ready[index] = !ready[index];
		Slots[index].Panel.color = ready[index] ? Color.green : Color.yellow;
		Slots[index].Subline.text = ready[index] ? "Ready" : "Not Ready";
	}

	public void StartGame()
	{
		int joinedCount = 0;
		for(int i = 0; i < joined.Length; i++)
			joinedCount += joined[i] ? 1 : 0;

		int readyCount = 0;
		for(int i = 0; i < ready.Length; i++)
			readyCount += ready[i] ? 1 : 0;

		if(readyCount == joinedCount && (joinedCount > 0 || BotCount.value > 0))
		{
			StartCoroutine(LoadScene(GameScene));
		}
	}

	private IEnumerator LoadScene(string _Scene, float _Speed = 1.0f)
	{
		// Fade camera to black
		if(Fade != null)
		{
			Color color = Fade.color;
			while(color.a <= 1.0f)
			{
				yield return new WaitForEndOfFrame();
				color.a += Time.deltaTime * _Speed;
				Fade.color = color;

				if(Music != null)
					Music.volume = 1.0f - color.a;
			}

			color.a = 1.0f;
			Fade.color = color;
		}

		if(Music != null)
			Music.enabled = false;

		// Create match settings
		MatchSettings settings = new MatchSettings();
		Slider s = null;
		if(KillLimit != null && KillLimit.isOn)
		{
			s = KillLimit.transform.parent.GetComponentInChildren<Slider>();
			if(s != null)
				settings.KillLimit = (int)s.value;
		}
		if(TimeLimit != null && TimeLimit.isOn)
		{
			s = TimeLimit.transform.parent.GetComponentInChildren<Slider>();
			if(s != null)
				settings.TimeLimit = s.value;
		}
		if(BotCount != null)
			settings.BotCount = (int)BotCount.value;

		// Instantiate level setup component
		GameObject obj = new GameObject("MatchSetup");
		DontDestroyOnLoad(obj);
		MatchSetup setup = obj.AddComponent<MatchSetup>();
		setup.Settings = settings;
		for(int i = 0; i < joined.Length; i++)
		{
			if(joined[i])
				setup.JoinedDevices.Add(players[i]);
		}

		// Flag for execution
		setup.DoExecute = true;

		// Load new scene
		Application.LoadLevel(_Scene);
	}

	private int CountJoinedPlayers()
	{
		int count = 0;

		for(int i = 0; i < joined.Length; i++)
		{
			if(joined[i])
				count++;
		}

		return count;
	}

	private int CountReadyPlayers()
	{
		int count = 0;

		for(int i = 0; i < ready.Length; i++)
		{
			if(ready[i])
				count++;
		}

		return count;
	}
}