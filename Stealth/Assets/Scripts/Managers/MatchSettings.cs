﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MatchSettings 
{
	public int KillLimit = 1;
	public float TimeLimit = 1;

	public int BotCount = 0;

	public MatchSettings()
	{
		KillLimit = 0;
		TimeLimit = 0.0f;
		BotCount = 0;
	}

	public MatchSettings(int _Kills, float _Time, int _BotCount)
	{
		KillLimit = _Kills;
		TimeLimit = _Time;
		BotCount = _BotCount;
	}
}