﻿using UnityEngine;
using System.Collections;

public class PoolManager : MonoBehaviour 
{
	static private PoolManager instance;

	static public bool Usable
	{
		get { return instance != null; }
	}

	static public void Create()
	{
		if(Usable)
			return;

		GameObject go = new GameObject("PoolManager");
		instance = go.AddComponent<PoolManager>();
		instance.Initialize();
	}



	private GameObject[] CorpsePrefabs;
	static public ObjectPool Corpses;

	private GameObject LeftFootprintPrefab;
	static public ObjectPool LeftFootprints;

	private GameObject RightFootprintPrefab;
	static public ObjectPool RightFootprints;

	private GameObject ShurikenPrefab;
	static public ObjectPool Shuriken;

	private GameObject CaltropPrefab;
	static public ObjectPool Caltrops;

	private GameObject RagPrefab;
	static public ObjectPool Rags;

	private GameObject[] BloodPrefabs;
	static public ObjectPool Blood;



	private void Initialize()
	{
		DontDestroyOnLoad(gameObject);

		// Load prefabs
		CorpsePrefabs = Resources.LoadAll<GameObject>("Pools/Corpses");
		LeftFootprintPrefab = Resources.Load<GameObject>("Pools/BloodFootprint_Left");
		RightFootprintPrefab = Resources.Load<GameObject>("Pools/BloodFootprint_Right");
		ShurikenPrefab = Resources.Load<GameObject>("Pools/Items/Shuriken");
		CaltropPrefab = Resources.Load<GameObject>("Pools/Items/Caltrop");
		RagPrefab = Resources.Load<GameObject>("Pools/Items/Rag");
		BloodPrefabs = Resources.LoadAll<GameObject>("Pools/Blood");

		// Create object pools
		Corpses = new ObjectPool(10, CorpsePrefabs, "Corpses", transform);
		LeftFootprints = new ObjectPool(50, LeftFootprintPrefab, "LeftFootprints", transform);
		RightFootprints = new ObjectPool(50, RightFootprintPrefab, "RightFootprints", transform);
		Shuriken = new ObjectPool(10, ShurikenPrefab, "Shuriken", transform);
		Caltrops = new ObjectPool(30, CaltropPrefab, "Caltrops", transform);
		Rags = new ObjectPool(10, RagPrefab, "Rags", transform);
		Blood = new ObjectPool(30, BloodPrefabs, "Blood", transform);
	}

	void OnLevelWasLoaded(int _Level)
	{
		// Hide all objects in pool
		Corpses.DeactivateAll();
		LeftFootprints.DeactivateAll();
		RightFootprints.DeactivateAll();
		Shuriken.DeactivateAll();
		Caltrops.DeactivateAll();
		Rags.DeactivateAll();
		Blood.DeactivateAll();
	}

	void Destroy()
	{
		// Release object pools
		Corpses.DestroyPool();
		LeftFootprints.DestroyPool();
		RightFootprints.DestroyPool();
		Shuriken.DestroyPool();
		Caltrops.DestroyPool();
		Rags.DestroyPool();
		Blood.DestroyPool();
	}
}