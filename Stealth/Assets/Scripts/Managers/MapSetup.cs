﻿using UnityEngine;
using System.Collections;

public class MapSetup : MonoBehaviour 
{
	public GameObject GenerationUI;

	private MapGenerator generator;
	private MatchSetup setup;

	void Start()
	{
		bool observe = false;
		bool navmesh = false;
		float players = 0;

		setup = FindObjectOfType<MatchSetup>();
		if(setup != null)
		{
			observe = setup.JoinedDevices.Count == 0;
			navmesh = setup.Settings.BotCount > 0;
			players = setup.JoinedDevices.Count + setup.Settings.BotCount;
		}

		generator = GetComponent<MapGenerator>();
		if(generator != null)
		{
			if(players > 2.0f)
			{
				players -= 2.0f;
				generator.Width += 10 * (int)(players / 2.0f);
				generator.Height += 10 * (int)(players / 2.0f);
				generator.HallwayCount += (int)(players / 4.0f);

				//Debug.Log("width=" + generator.Width + ", height=" + generator.Height + ", count=" + generator.HallwayCount);
			}
			generator.CreateDarkLayout = !observe;
			generator.CreateNavMesh = navmesh;
			generator.Generate();
		}
	}

	void Update()
	{
		if(generator.Generating)
			return;

		if(setup != null)
			setup.Execute();

		Destroy(GenerationUI);
		Destroy(gameObject);
	}
}