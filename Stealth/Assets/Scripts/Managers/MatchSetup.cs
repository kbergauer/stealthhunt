﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MatchSetup : MonoBehaviour 
{
	public PlayerInput PlayerPrefab;
	public CameraController CameraPrefab;
	public Canvas HUDPrefab;

	public Player BotPrefab;

	public MatchSettings Settings;
	public List<PlayerInput.EDevice> JoinedDevices = new List<PlayerInput.EDevice>();
	public List<GameObject> SpawnPoints;

	public bool DoExecute = false;

	private float hue = 0.0f;
	private int playerCount = 0;

	void OnLevelWasLoaded(int _Index)
	{
		if(!DoExecute)
			return;

		if(FindObjectOfType<MapSetup>() == null)
			Execute();
	}

	public void Execute()
	{
		PlayerPrefab = Resources.Load<PlayerInput>("MatchSetup/Player");
		CameraPrefab = Resources.Load<CameraController>("MatchSetup/CameraSet");
		HUDPrefab = Resources.Load<Canvas>("MatchSetup/HUDCanvas");
		BotPrefab = Resources.Load<Player>("MatchSetup/AIPlayer");

		if(JoinedDevices.Count == 0)
		{
			/*
			Debug.LogError("No devices joined the game; unable to setup players!");
			return;
			*/

			InitializeObserver();
		}

		// Count joined devices
		playerCount = JoinedDevices.Count + Settings.BotCount;

		// Get spawn points
		GameObject[] objs = GameObject.FindGameObjectsWithTag("Respawn");
		SpawnPoints = new List<GameObject>(objs);

		// Initialize players
		hue = Random.Range(0.0f, 360.0f);
		for(int i = 0; i < JoinedDevices.Count; i++)
		{
			InitializePlayer(i, JoinedDevices[i]);
		}

		// Initialize bots
		for(int i = JoinedDevices.Count; i < JoinedDevices.Count + Settings.BotCount; i++)
		{
			InitializeBot(i);
		}

		// Setup audio listener for multiplayer
		if(JoinedDevices.Count > 1 || JoinedDevices.Count == 0)
		{
			GameObject obj = new GameObject("Audio Listener");
			obj.transform.position = Vector3.zero;
			obj.AddComponent<AudioListener>();

			SoundUtility.Flat = true;
		}
		else
		{
			SoundUtility.Flat = false;
		}

		// Setup viewport separation
		if(JoinedDevices.Count > 1)
		{
			GameObject.Find("SingleTimeDisplay").SetActive(false);

			if(JoinedDevices.Count % 2 != 0)
			{
				GameObject.Find("BotHor_Separator").SetActive(false);
				if(JoinedDevices.Count == 1)
				{
					GameObject.Find("TopHor_Separator").SetActive(false);
				}
			}
			if(JoinedDevices.Count < 3)
			{
				GameObject.Find("LeftVert_Separator").SetActive(false);
				GameObject.Find("RightVert_Separator").SetActive(false);
			}
		}
		else
		{
			if(Settings.TimeLimit <= 0)
				GameObject.Find("SingleTimeDisplay").SetActive(false);

			GameObject.Find("BotHor_Separator").SetActive(false);
			GameObject.Find("TopHor_Separator").SetActive(false);
			GameObject.Find("LeftVert_Separator").SetActive(false);
			GameObject.Find("RightVert_Separator").SetActive(false);
		}

		GameManager.StartMatch(Settings);
	}

	private void InitializePlayer(int _Index, PlayerInput.EDevice _Device)
	{
		// Create parent object
		GameObject parent = new GameObject("Player" + (_Index + 1) + "Group");

		// Get random spawn point and remove it from the list
		Transform spawn = SpawnPoints[Random.Range(0, SpawnPoints.Count)].transform;
		SpawnPoints.Remove(spawn.gameObject);

		// Instantiate and setup player from prefab
		PlayerInput input = Instantiate(PlayerPrefab, spawn.position, spawn.rotation) as PlayerInput;
		input.name = "Player" + (_Index + 1);
		input.gameObject.layer = LayerMask.NameToLayer("Hidden" + (_Index + 1));
		input.Device = _Device;
		input.transform.SetParent(parent.transform);
		SetLayerRecursively(input.transform.FindChild("PlayerSprite"), LayerMask.NameToLayer("Hidden" + (_Index + 1)));
		input.transform.FindChild("PlayerVision").gameObject.layer = LayerMask.NameToLayer("ShadowLayer" + (_Index + 1));
		Player player = input.GetComponent<Player>();
		player.PersonalColor = HSV.ToRGB((hue + _Index * (360.0f / playerCount + 1)) % 360.0f, 1.0f, 1.0f);

		// Instantiate and link camera from prefab
		CameraController camera = Instantiate(CameraPrefab) as CameraController;
		camera.name = "Player" + (_Index + 1) + "CameraSet";
		camera.transform.SetParent(parent.transform);
		PlayerController controller = input.GetComponent<PlayerController>();
		camera.Target = controller;
		controller.BaseCamera = camera.transform.FindChild("Base Camera").GetComponent<Camera>();

		// Setup camera viewport
		Rect viewport = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
		if(JoinedDevices.Count > 1)	// Don't adjust viewport for single player
		{
			if(JoinedDevices.Count % 2 == 0 || _Index < 2)	// Divide viewport width for 2 or 4 players or player index below 3
				viewport.width /= 2.0f;

			if(JoinedDevices.Count > 2)	// Divide viewport height for 3 and 4 players
				viewport.height /= 2.0f;

			// Adjust viewport top-left corner position
			viewport.x = _Index % 2 * 0.5f;
			viewport.y = JoinedDevices.Count > 2 && _Index < 2 ? 0.5f : 0.0f;
		}
		camera.SetViewport(viewport);

		// Setup depth camera to render own hidden objects and lights
		Camera depth = camera.transform.FindChild("Depth Camera").GetComponent<Camera>();
		depth.cullingMask |= (1<<LayerMask.NameToLayer("Hidden" + (_Index + 1)));
		depth.cullingMask |= (1<<LayerMask.NameToLayer("ShadowLayer" + (_Index + 1)));

		// Instantiate heads-up-display
		Canvas canvas = Instantiate(HUDPrefab) as Canvas;
		canvas.name = "Player" + (_Index + 1) + "Canvas";
		canvas.transform.SetParent(parent.transform);
		canvas.worldCamera = depth;
		PlayerHUD hud = canvas.GetComponent<PlayerHUD>();
		hud.SetTarget(input.GetComponent<Player>());

		// Setup single audio listener
		if(JoinedDevices.Count == 1)
		{
			camera.transform.FindChild("Base Camera").gameObject.AddComponent<AudioListener>();
		}
	}

	private void InitializeBot(int _Index)
	{
		// Create parent object
		GameObject parent = new GameObject("Player" + (_Index + 1) + "Group");

		// Get random spawn point and remove it from the list
		Transform spawn = SpawnPoints[Random.Range(0, SpawnPoints.Count)].transform;
		SpawnPoints.Remove(spawn.gameObject);

		// Instantiate and setup player from prefab
		Player player = Instantiate(BotPrefab, spawn.position, spawn.rotation) as Player;
		player.name = "Player" + (_Index + 1) + " [Bot]";
		player.gameObject.layer = LayerMask.NameToLayer("HiddenNoRaycast");
		player.transform.SetParent(parent.transform);
		SetLayerRecursively(player.transform.FindChild("PlayerSprite"), LayerMask.NameToLayer("Hidden"));
		player.PersonalColor = HSV.ToRGB((hue + _Index * (360.0f / playerCount + 1)) % 360.0f, 1.0f, 1.0f);
	}

	private void InitializeObserver()
	{
		Instantiate(Resources.Load<GameObject>("MatchSetup/ObserverCamera"));
	}

	private void SetLayerRecursively(Transform _Target, int _Layer)
	{
		_Target.gameObject.layer = _Layer;
		foreach(Transform child in _Target)
			SetLayerRecursively(child, _Layer);
	}
}