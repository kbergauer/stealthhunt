﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour 
{
	static private GameManager inst = null;
	static public GameManager Instance
	{
		get
		{
			if(inst == null)
			{
				GameObject go = new GameObject("GameManager");
				inst = go.AddComponent<GameManager>();
			}

			return inst;
		}
	}

	static public float timeScale = 1.0f;
	static public float time = 0.0f;
	static public float deltaTime = 0.0f;



	private MatchSettings settings;
	private bool isRunning = false;
	private float respawnTime = 5.0f;
	private float invincibilityDuration = 2.0f;
	private GameObject[] spawnPoints;
	private Player[] players;
	private Dictionary<Player, int> playerKills;
	private Dictionary<Player, int> playerDeaths;
	private Dictionary<Player, PlayerHUD> playerHUDs;

	private AIWorldState aiWorld;

	void Update()
	{
		if(timeScale > 0.0f)
		{
			deltaTime = Time.deltaTime * timeScale;
			time += deltaTime;
		}
		else
		{
			deltaTime = 0.0f;
		}
	}

	private void Initialize()
	{
		spawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
		playerHUDs = new Dictionary<Player, PlayerHUD>();

		aiWorld = AIWorldState.GetInstance();
	}

	private IEnumerator Respawn(Player _Player)
	{
		if(_Player.HUD != null)
			_Player.HUD.ShowRespawn();

		float time = respawnTime;
		while(time > 0.0f)
		{
			time = Mathf.Max(0.0f, time - GameManager.deltaTime);
			if(_Player.HUD != null)
				_Player.HUD.UpdateRespawn(time);

			yield return new WaitForEndOfFrame();
		}

		if(_Player.HUD != null)
			_Player.HUD.UpdateRespawn(0.0f);

		Transform spawn = spawnPoints[Random.Range(0, spawnPoints.Length)].transform;
		_Player.transform.position = spawn.position;
		_Player.transform.rotation = spawn.rotation;

		_Player.gameObject.SetActive(true);
	}

	private void PrepareMatch()
	{
		// Start music
		StartCoroutine(StartMusic(1.0f));

		// Create pool manager if necessary
		PoolManager.Create();

		// Get all players
		players = FindObjectsOfType<Player>();

		// Lock input controls
		for(int i = 0; i < players.Length; i++)
		{
			players[i].gameObject.GetInterface<IInput>().Lock();
		}

		// Initialize kill and death counts
		playerKills = new Dictionary<Player, int>();
		playerDeaths = new Dictionary<Player, int>();
		for(int i = 0; i < players.Length; i++)
		{
			playerKills.Add(players[i], 0);
			playerDeaths.Add(players[i], 0);
		}

		// Get countdown
		GameObject go = GameObject.Find("Countdown");
		Text text = null;
		if(go != null)
		{
			text = go.GetComponent<Text>();
		}
		go = GameObject.Find("CountdownBackground");
		CanvasGroup group = null;
		if(go != null)
		{
			group = go.GetComponent<CanvasGroup>();
		}

		// Initiate match
		StartCoroutine(MatchCountdown(text, group));
	}

	private IEnumerator MatchCountdown(Text _Countdown, CanvasGroup _Group)
	{
		if(_Countdown != null)
			_Countdown.text = "Get ready!";

		// Fade in camera
		GameObject fade = GameObject.FindGameObjectWithTag("Fade");
		if(fade != null)
		{
			CanvasGroup group = fade.GetComponent<CanvasGroup>();
			while(group.alpha > 0.0f)
			{
				group.alpha -= GameManager.deltaTime * 2.0f;
				yield return new WaitForEndOfFrame();
			}
		}

		// Count down to zero
		if(_Countdown != null)
		{
			int count = 3;
			while(count > 0)
			{
				_Countdown.text = count.ToString();
				count--;
				yield return new WaitForSeconds(1.0f);
			}

			_Countdown.text = "Hunt!";
		}
		
		// Initiate the match
		InitiateMatch();

		// Fade out the countdown
		if(_Group != null)
		{
			while(_Group.alpha > 0.0f)
			{
				_Group.alpha -= GameManager.deltaTime * 0.5f;
				yield return new WaitForEndOfFrame();
			}

			_Group.alpha = 0.0f;
		}
	}

	private void InitiateMatch()
	{
		// Set flag
		isRunning = true;

		// Release input controls
		for(int i = 0; i < players.Length; i++)
		{
			players[i].INPUT.Unlock();
		}

		// Start game timer
		if(settings.TimeLimit > 0.0f)
		{
			//Debug.Log("Starting time routine.");
			StartCoroutine(GameTimer());
		}
	}

	private IEnumerator GameTimer()
	{
		//Debug.Log("Inside time routine.");

		TimeBar[] bars = FindObjectsOfType<TimeBar>();

		float seconds = 0.0f;
		float totalSeconds = settings.TimeLimit * 60.0f;
		while(seconds < totalSeconds)
		{
			seconds += Time.deltaTime;
			for(int i = 0; i < bars.Length; i++)
				bars[i].transform.localScale = Vector3.one - bars[i].Direction * (seconds / totalSeconds);
			
			yield return new WaitForEndOfFrame();
		}

		if(isRunning)
			EndMatch();
	}

	private void EndMatch()
	{
		isRunning = false;
		StopAllCoroutines();
		StartCoroutine(StopMusic(1.0f));

		IInput input = null;

		var ordered = from pair in playerKills orderby pair.Value descending select pair;
		int top = -1;
		foreach(KeyValuePair<Player, int> entry in ordered)
		{
			if(top < 0)
				top = entry.Value;

			// Ignore if player is null
			if(entry.Key == null)
				continue;

			// Tint HUD
			if(playerHUDs.ContainsKey(entry.Key) && playerHUDs[entry.Key] != null)
				playerHUDs[entry.Key].ShowOverlay(entry.Value == top && top > 0 ? Color.green : Color.red);

			// Lock input
			input = entry.Key.gameObject.GetInterface<IInput>();
			if(input != null)
				input.Lock();
		}

		// Enable game over menu
		GameObject obj = GameObject.FindGameObjectWithTag("GameOver");
		if(obj != null)
		{
			Scoreboard go = obj.GetComponent<Scoreboard>();
			if(go != null)
			{
				go.UpdateStatistics();
				go.Show();
			}
		}
	}

	private IEnumerator LoadScene(string _Scene, float _Speed)
	{
		GameObject fade = GameObject.FindGameObjectWithTag("Fade");
		if(fade == null)
			yield break;

		CanvasGroup group = fade.GetComponent<CanvasGroup>();
		while(group.alpha < 1.0f)
		{
			group.alpha += Time.deltaTime * _Speed;
			yield return new WaitForEndOfFrame();
		}

		GameManager.timeScale = 1.0f;
		Application.LoadLevel(_Scene);
	}

	private void ShowMessage(Player _Target, string _Message, Color _Color)
	{
		if(!playerHUDs.ContainsKey(_Target))
		{
			playerHUDs.Add(_Target, _Target.HUD);
		}

		if(playerHUDs[_Target] != null)
			playerHUDs[_Target].ShowMessage(_Message, _Color);
	}

	private IEnumerator StartMusic(float _Speed)
	{
		GameObject obj = GameObject.Find("Music");
		if(obj == null)
			yield break;

		AudioSource music = obj.GetComponent<AudioSource>();
		float volume = PlayerPrefs.GetFloat("MusicVolume");

		if(music != null)
		{
			music.Play();

			while(music.volume < volume)
			{
				music.volume += Time.deltaTime * _Speed;
				yield return new WaitForEndOfFrame();
			}

			music.volume = volume;
		}
	}

	private IEnumerator StopMusic(float _Speed)
	{
		GameObject obj = GameObject.Find("Music");
		if(obj == null)
			yield break;

		AudioSource music = obj.GetComponent<AudioSource>();
		if(music != null)
		{
			while(music.volume > 0.0f)
			{
				music.volume -= Time.deltaTime * _Speed;
				yield return new WaitForEndOfFrame();
			}

			music.volume = 0.0f;
			music.Stop();
		}
	}



	static public bool IsRunning
	{
		get { return Instance.isRunning; }
	}

	static public void StartMatch(MatchSettings _Settings)
	{
		if(!Instance.isRunning)
		{
			Instance.Initialize();

			Instance.settings = _Settings;
			Instance.PrepareMatch();
		}
	}

	static public void RequestRespawn(Player _Player)
	{
		if(IsRunning)
			Instance.StartCoroutine(Instance.Respawn(_Player));
	}

	static public void RegisterKill(Player _Killer, Player _Victim)
	{
		if(!IsRunning)
			return;

		// Add kill for killer
		if(!Instance.playerKills.ContainsKey(_Killer))
			Instance.playerKills.Add(_Killer, 1);
		else
			Instance.playerKills[_Killer]++;

		// Add death for victim
		if(!Instance.playerDeaths.ContainsKey(_Victim))
			Instance.playerDeaths.Add(_Victim, 1);
		else
			Instance.playerDeaths[_Victim]++;

		// Show kill message to killer
		Instance.ShowMessage(_Killer, "You killed\n" + _Victim.name, Color.green);

		// Show kill message to victim
		Instance.ShowMessage(_Victim, "You were killed by\n" + _Killer.name, Color.red);

		// Update world state
		Instance.aiWorld.RemoveTrails(_Victim);

		// Check game over
		if(Instance.settings.KillLimit > 0 && Instance.playerKills[_Killer] >= Instance.settings.KillLimit)
		{
			Instance.EndMatch();
		}
		// Request respawn
		else
		{
			RequestRespawn(_Victim);
		}
	}

	static public int GetKills(Player _Player)
	{
		if(!Instance.isRunning || !Instance.playerKills.ContainsKey(_Player))
			return 0;
		else
			return Instance.playerKills[_Player];
	}

	static public Dictionary<string, int> GetAllKills()
	{
		Dictionary<string, int> kills = new Dictionary<string, int>();

		List<Player> players = new List<Player>(Instance.playerKills.Keys);
		for(int i = 0; i < players.Count; i++)
		{
			if(players[i] != null)
				kills.Add(players[i].name, Instance.playerKills[players[i]]);
		}

		return kills;
	}

	static public int GetDeaths(Player _Player)
	{
		if(!Instance.isRunning || !Instance.playerDeaths.ContainsKey(_Player))
			return 0;
		else
			return Instance.playerDeaths[_Player];
	}

	static public Dictionary<string, int> GetAllDeaths()
	{
		Dictionary<string, int> deaths = new Dictionary<string, int>();

		List<Player> players = new List<Player>(Instance.playerDeaths.Keys);
		for(int i = 0; i < players.Count; i++)
		{
			if(players[i] != null)
				deaths.Add(players[i].name, Instance.playerDeaths[players[i]]);
		}

		return deaths;
	}

	static public void SwitchScene(string _Scene)
	{
		Instance.StartCoroutine(Instance.LoadScene(_Scene, 1.0f));
	}

	static public void LoadMenu()
	{
		GameObject obj = GameObject.Find("MatchSetup");
		if(obj != null)
			Destroy(obj);

		Instance.StartCoroutine(Instance.LoadScene("Menu", 1.0f));
	}

	static public float RespawnTime
	{
		get { return Instance.respawnTime; }
	}

	static public float InvincibilityDuration
	{
		get { return Instance.invincibilityDuration; }
	}
}