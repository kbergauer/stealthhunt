﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RoomTemplate
{
	public string Tag = "Room Template";
	public int[] Objects = new int[System.Enum.GetNames(typeof(Furniture.ESize)).Length];

	public RoomTemplate()
	{
		Tag = "Room Template";
		Objects = new int[System.Enum.GetNames(typeof(Furniture.ESize)).Length];
	}

	public RoomTemplate(string _Tag, int[] _Objects)
	{
		Tag = _Tag;
		Objects = _Objects;
	}
}