﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Coordinate 
{
	public int x;
	public int y;

	public Coordinate(int _X, int _Y)
	{
		x = _X;
		y = _Y;
	}

	public override string ToString()
	{
		return "(" + x + ", " + y + ")";
	}

	public static int HDistance(Coordinate _Start, Coordinate _End)
	{
		return Mathf.Abs(_Start.x - _End.x);
	}

	public static int VDistance(Coordinate _Start, Coordinate _End)
	{
		return Mathf.Abs(_Start.y - _End.y);
	}
}