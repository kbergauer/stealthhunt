﻿using UnityEngine;
using System.Collections;

public class Furniture : MonoBehaviour 
{
	public enum ESize
	{
		Small,
		Medium,
		Large
	}

	[Header("Type Data")]
	public ESize Size = ESize.Medium;
	public string[] Tags = null;
	public float Chance = 1.0f;

	[Header("Spatial Data")]
	public Rect Bounds = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
	public bool AlignToEdge = false;
	
	[Header("Debug")]
	public bool VisualizeSize = true;

	void OnDrawGizmos()
	{
		if(VisualizeSize)
		{
			Vector3 size = Bounds.size;
			if(Mathf.Abs(transform.rotation.eulerAngles.z - 90.0f) < 1.0f || Mathf.Abs(transform.rotation.eulerAngles.z - 270.0f) < 1.0f)
			{
				float temp = size.x;
				size.x = size.y;
				size.y = temp;
			}
			Gizmos.DrawWireCube(transform.position, size);
		}
	}

	public bool HasTag(string _Tag)
	{
		if(string.IsNullOrEmpty(_Tag))
			return true;

		for(int i = 0; i < Tags.Length; i++)
		{
			if(Tags[i] == _Tag)
				return true;
		}

		return false;
	}
}