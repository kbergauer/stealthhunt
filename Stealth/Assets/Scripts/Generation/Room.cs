﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room 
{
	public enum EEdge
	{
		Top,
		Bottom,
		Left,
		Right
	}

	public Rect Bounds;

	private float mapWidth;
	private float mapHeight;

	private Rect[] innerEdges;
	private Rect[] hallwayEdges;

	private List<Rect> occupiedSpots;
	private int lastRemoteSpot = 0;
	private List<Furniture> furniture;


	public string Tag
	{
		get;
		set;
	}


	public Room(Rect _Bounds, float _Width, float _Height)
	{
		Bounds = _Bounds;
		mapWidth = _Width;
		mapHeight = _Height;

		innerEdges = new Rect[4];
		hallwayEdges = null;

		occupiedSpots = new List<Rect>();
		furniture = new List<Furniture>();
		Tag = "";
	}

	public void Destroy()
	{
		innerEdges = null;
		hallwayEdges = null;

		occupiedSpots.Clear();
		occupiedSpots = null;

		for(int i = 0; i < furniture.Count; i++)
		{
			GameObject.Destroy(furniture[i]);
		}
		furniture.Clear();
		furniture = null;
	}

	public void SetInnerEdge(Rect _Edge, EEdge _Direction)
	{
		innerEdges[(int)_Direction] = _Edge;
	}

	public void SetHallwayEdges(Rect[] _Edges)
	{
		hallwayEdges = _Edges;
	}

	public void CorrectBounds()
	{
		if(hallwayEdges != null && hallwayEdges.Length > 0)
		{
			for(int i = 0; i < hallwayEdges.Length; i++)
			{
				if(hallwayEdges[i].xMin == Bounds.xMin)
				{
					Bounds.xMin++;
				}
				else if(hallwayEdges[i].xMax == Bounds.xMax)
				{
					Bounds.xMax--;
				}
				else if(hallwayEdges[i].yMin == Bounds.yMin)
				{
					Bounds.yMin++;
				}
				else if(hallwayEdges[i].yMax == Bounds.yMax)
				{
					Bounds.yMax--;
				}
			}
		}
	}

	public Rect[] GetOccupiedSpots()
	{
		return occupiedSpots.ToArray();
	}

	public Rect[] GetWalkableAreas()
	{
		// Remove all furniture spots
		occupiedSpots.RemoveRange(lastRemoteSpot, occupiedSpots.Count - lastRemoteSpot);

		// Add furniture colliders to occupied spots
		Collider2D coll = null;
		Rect spot = new Rect();
		for(int i = 0; i < furniture.Count; i++)
		{
			coll = furniture[i].GetComponent<Collider2D>();
			if(coll != null)
			{
				spot.width = coll.bounds.size.x;
				spot.height = coll.bounds.size.y;
				spot.x = coll.bounds.center.x - coll.bounds.extents.x;
				spot.y = coll.bounds.center.y - coll.bounds.extents.y;

				spot.x -= 0.5f;
				spot.y -= 0.5f;
				spot.width += 1.0f;
				spot.height += 1.0f;

				occupiedSpots.Add(spot);
			}
		}

		List<Rect> areas = new List<Rect>();
		List<Rect> newAreas = new List<Rect>();
		Rect[] range = null;
		Rect start = new Rect(Bounds);

		// Move to correct position
		start.x -= mapWidth / 2.0f;
		start.y -= mapHeight / 2.0f;

		// Shrink to accommodate player size
		start.x += 0.5f;
		start.y += 0.5f;
		start.width -= 1.0f;
		start.height -= 1.0f;

		areas.Add(start);

		// Cutout occupied spots
		int ignore = 0;
		while(occupiedSpots.Count > 0)
		{
			ignore = 0;
			newAreas.Clear();

			while(areas.Count > ignore)
			{
				range = SplitRoom(areas[ignore], occupiedSpots[0]);
				if(range != null)
				{
					newAreas.AddRange(range);
					areas.RemoveAt(ignore);
				}
				else
				{
					ignore++;
				}
			}

			areas.AddRange(newAreas);
			occupiedSpots.RemoveAt(0);
		}

		return areas.ToArray();
	}



	public Rect GetInnerEdge(EEdge _Direction)
	{
		if(innerEdges.Length < (int)_Direction)
			return new Rect();

		return innerEdges[(int)_Direction];
	}

	public Rect[] GetHallwayEdges()
	{
		return hallwayEdges;
	}

	public Rect GetFreeSpaceAlongEdge(EEdge _Direction, Rect _Size)
	{
		Rect space = new Rect(_Size);
		Rect edge = GetInnerEdge(_Direction);

		switch(_Direction)
		{
			// Top
			case EEdge.Top:
				{
					space.x = Random.Range(edge.xMin, edge.xMax - space.width);
					space.y = edge.yMax - space.height;
				} break;

			// Bottom
			case EEdge.Bottom:
				{
					space.x = Random.Range(edge.xMin, edge.xMax - space.width);
					space.y = edge.yMin;
				} break;

			// Left
			case EEdge.Left:
				{
					space.x = edge.xMin;
					space.y = Random.Range(edge.yMin, edge.yMax - space.height);
				} break;

			// Right
			case EEdge.Right:
				{
					space.x = edge.xMax - space.width;
					space.y = Random.Range(edge.yMin, edge.yMax - space.height);
				} break;
		}

		return space;
	}

	public void AddOccupiedSpot(Rect _Spot)
	{
		if(_Spot.Overlaps(Bounds))
		{
			occupiedSpots.Add(_Spot);
			lastRemoteSpot = occupiedSpots.Count - 1;
		}
	}

	public bool AddFurniture(Furniture _Prefab, Transform _Parent, int _MaximumTries)
	{
		if(!_Prefab.HasTag(Tag))
			return false;

		Furniture instance = null;
		Rect size = new Rect(_Prefab.Bounds);
		Rect spot = new Rect(size);
		EEdge direction = EEdge.Top;
		Vector2 position = Vector2.zero;
		Quaternion rotation = Quaternion.identity;

		// Get placement direction
		direction = (EEdge)Random.Range(0, 4);

		// Get prefab rotation
		switch(direction)
		{
			// Top
			case EEdge.Top:
				{
					rotation = Quaternion.identity;
				} break;

			// Bottom
			case EEdge.Bottom:
				{
					rotation = Quaternion.AngleAxis(180.0f, Vector3.forward);
				} break;

			// Left
			case EEdge.Left:
				{
					rotation = Quaternion.AngleAxis(90.0f, Vector3.forward);
				} break;

			// Right
			case EEdge.Right:
				{
					rotation = Quaternion.AngleAxis(270.0f, Vector3.forward);
				} break;
		}

		// Correct furniture space occupation
		if(direction == EEdge.Left || direction == EEdge.Right)
		{
			float temp = size.width;
			size.width = size.height;
			size.height = temp;

			spot = new Rect(size);
		}

		// Get random space to place furniture in
		int tries = 0;
		bool occupied = true;
		while(occupied)
		{
			tries++;
			if(tries > _MaximumTries)
				break;

			// Get space on edge
			if(_Prefab.AlignToEdge || Random.Range(0.0f, 1.0f) < 0.5f)
				spot = GetFreeSpaceAlongEdge(direction, size);
			else
			{
				spot.x = Random.Range(Bounds.xMin, Bounds.xMax - size.width);
				spot.y = Random.Range(Bounds.yMin, Bounds.yMax - size.height);
			}

			// Check space occupation
			occupied = false;
			for(int j = 0; j < occupiedSpots.Count; j++)
			{
				if(occupiedSpots[j].Overlaps(spot))
				{
					occupied = true;
					continue;
				}
			}
		}

		if(tries > _MaximumTries)
		{
			//Debug.LogWarning("Unable to find free spot to place " + prefab.name);
			return false;
		}

		// Set furniture position
		position = spot.center;
		position.x -= mapWidth / 2.0f;
		position.y -= mapHeight / 2.0f;

		// Instantiate object
		instance = GameObject.Instantiate(_Prefab, position, rotation) as Furniture;
		instance.name = _Prefab.name;
		instance.transform.SetParent(_Parent);

		// Save occupied spot and furniture
		occupiedSpots.Add(spot);
		furniture.Add(instance);

		return true;
	}



	private Rect[] SplitRoom(Rect _Room, Rect _Cutout)
	{
		if(!_Room.Overlaps(_Cutout))
			return null;

		// Create new rooms
		List<Rect> split = new List<Rect>();

		// Get cutout corners
		bool topLeft = _Room.Contains(new Vector2(_Cutout.xMin, _Cutout.yMax));
		bool topRight = _Room.Contains(new Vector2(_Cutout.xMax, _Cutout.yMax));
		bool bottomLeft = _Room.Contains(new Vector2(_Cutout.xMin, _Cutout.yMin));
		bool bottomRight = _Room.Contains(new Vector2(_Cutout.xMax, _Cutout.yMin));

		// Check overlap without corner contained within room
		if(!topLeft && !topRight && !bottomLeft && !bottomRight)
		{
			if(_Cutout.yMax < _Room.yMax)
			{
				split.Add(new Rect(_Room.xMin, _Cutout.yMax, _Room.xMax - _Room.xMin, _Room.yMax - _Cutout.yMax));
			}
			if(_Cutout.xMax < _Room.xMax)
			{
				split.Add(new Rect(_Cutout.xMax, _Room.yMin, _Room.xMax - _Cutout.xMax, _Room.yMax - _Room.yMin));
			}
			if(_Cutout.yMin > _Room.yMin)
			{
				split.Add(new Rect(_Room.xMin, _Room.yMin, _Room.xMax - _Room.xMin, _Cutout.yMin - _Room.yMin));
			}
			if(_Cutout.xMin > _Room.xMin)
			{
				split.Add(new Rect(_Room.xMin, _Room.yMin, _Cutout.xMin - _Room.xMin, _Room.yMax - _Room.yMin));
			}
		}
		else
		{
			// Check top
			if(_Room.yMax != _Cutout.yMax)
			{
				if(topLeft)
				{
					split.Add(new Rect(_Cutout.xMin, _Cutout.yMax, _Room.xMax - _Cutout.xMin, _Room.yMax - _Cutout.yMax));
				}
				else if(topRight)
				{
					split.Add(new Rect(_Room.xMin, _Cutout.yMax, _Room.xMax - _Room.xMin, _Room.yMax - _Cutout.yMax));
				}
			}

			// Check right
			if(_Room.xMax != _Cutout.xMax)
			{
				if(topRight)
				{
					split.Add(new Rect(_Cutout.xMax, _Room.yMin, _Room.xMax - _Cutout.xMax, _Cutout.yMax - _Room.yMin));
				}
				else if(bottomRight)
				{
					split.Add(new Rect(_Cutout.xMax, _Room.yMin, _Room.xMax - _Cutout.xMax, _Room.yMax - _Room.yMin));
				}
			}

			// Check bottom
			if(_Room.yMin != _Cutout.yMin)
			{
				if(bottomRight)
				{
					split.Add(new Rect(_Room.xMin, _Room.yMin, _Cutout.xMax - _Room.xMin, _Cutout.yMin - _Room.yMin));
				}
				else if(bottomLeft)
				{
					split.Add(new Rect(_Room.xMin, _Room.yMin, _Room.xMax - _Room.xMin, _Cutout.yMin - _Room.yMin));
				}
			}

			// Check left
			if(_Room.xMin != _Cutout.xMin)
			{
				if(bottomLeft)
				{
					split.Add(new Rect(_Room.xMin, _Cutout.yMin, _Cutout.xMin - _Room.xMin, _Room.yMax - _Cutout.yMin));
				}
				else if(topLeft)
				{
					split.Add(new Rect(_Room.xMin, _Room.yMin, _Cutout.xMin - _Room.xMin, _Room.yMax - _Room.yMin));
				}
			}
		}

		// Add new rooms to list
		return split.ToArray();
	}
}
