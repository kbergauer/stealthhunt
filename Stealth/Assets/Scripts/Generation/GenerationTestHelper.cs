﻿using UnityEngine;
using System.Collections;

public class GenerationTestHelper : MonoBehaviour 
{
	public void Regenerate()
	{
		GameObject obj;

		obj = GameObject.Find("Map");
		if(obj != null)
			Destroy(obj);

		obj = GameObject.Find("NavMesh");
		if(obj != null)
			Destroy(obj);

		MapGenerator gen = FindObjectOfType<MapGenerator>();
		gen.Generate();
	}
}