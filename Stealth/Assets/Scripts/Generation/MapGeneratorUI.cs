﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapGeneratorUI : MonoBehaviour 
{
	public MapGenerator generator;

	public Slider ProgressBar;

	void Update()
	{
		if(generator.Generating)
		{
			if(!ProgressBar.gameObject.activeInHierarchy)
				ProgressBar.gameObject.SetActive(true);

			ProgressBar.value = generator.Progress;
		}
		else
		{
			if(ProgressBar.gameObject.activeInHierarchy)
				ProgressBar.gameObject.SetActive(false);
		}
	}
}