﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MapGenerator : MonoBehaviour 
{
	[Header("Dimensions")]
	public int Width = 30;
	public int Height = 30;

	[Header("Layout")]
	public int HallwayCount = 4;
	public int HallwayWidth = 3;
	public int HallwayMargin = 7;
	public int RoomMargin = 6;

	[Header("Texture Size")]
	public int PixelsPerUnit = 1;

	[Header("Light Texture")]
	public bool CreateLightLayout = true;
	public Color HallColor = Color.white;
	public Color FloorColor = Color.blue;
	public Color WallColor = Color.black;
	public Color DoorColor = Color.green;

	[Header("Dark Texture")]
	public bool CreateDarkLayout = true;
	public Color DarkHallColor = Color.white;
	public Color DarkFloorColor = Color.blue;
	public Color DarkWallColor = Color.black;
	public Color DarkDoorColor = Color.green;

	[Header("Objects")]
	public bool CreateWallCollision = true;
	public GameObject DoorPrefab;
	public Furniture[] FurniturePrefabs;
	public RoomTemplate[] Templates;

	[Header("AI")]
	public bool CreateNavMesh = false;

	[Header("Debug")]
	public bool LogSteps = false;
	public bool VisualizeSteps = false;
	public float VisualizeSpeed = 0.1f;



	public bool Generating
	{
		get;
		private set;
	}
	public float Progress
	{
		get;
		private set;
	}

	private enum ETileType
	{
		Hall,
		Wall,
		Floor,
		Door
	};

	private int visualization = 0;
	private Texture2D lastMap = null;

	private int[,] map;
	private List<Rect> hallwayList;
	private List<Rect> chunkList;
	private List<Rect> roomList;
	private List<Rect> doorList;
	private List<Rect> hallwayDoorList;

	private List<Room> roomInstances;

	private GameObject parent = null;



	#region GENERATION

	public void Generate()
	{
		if(Generating)
		{
			Debug.LogError(this.GetType() + ": Generation already in progress!");
			return;
		}

		StopAllCoroutines();
		StartCoroutine(RunGeneration());
	}

	public IEnumerator RunGeneration()
	{
		float total = Time.time;
		float intervall = 0.0f;

		if(Generating)
		{
			Debug.LogError(this.GetType() + ": Generation already in progress!");
			yield break;
		}

		Generating = true;
		Progress = 0.0f;



		// Initialize map
		map = new int[Width, Height];



		// Generate house
		intervall = Time.time;
		GenerateHouse();
		Progress += 0.05f;
		if(LogSteps)
			Debug.Log("House done. (" + (Time.time - intervall) + " secs)");



		// Generate hallways
		intervall = Time.time;
		if(VisualizeSteps)
			parent = new GameObject("GenerateHallways");
		yield return StartCoroutine(GenerateHallways(HallwayCount, HallwayMargin, HallwayWidth));
		if(LogSteps)
			Debug.Log("Hallways done. (" + (Time.time - intervall) + " secs)");
		yield return new WaitForEndOfFrame();



		// Generate rooms with walls and doors
		intervall = Time.time;
		if(VisualizeSteps)
			parent = new GameObject("GenerateRooms");
		yield return StartCoroutine(GenerateRooms());
		if(LogSteps)
			Debug.Log("Rooms done. (" + (Time.time - intervall) + " secs)");



		// Visualize generated map (light and dark)
		intervall = Time.time;
		parent = new GameObject("Map");
		GameObject textureParent = new GameObject("Layout");
		textureParent.transform.SetParent(parent.transform);
		GameObject layout;
		if(CreateLightLayout)
		{
			layout = Visualize("LightLayout", CreateTexture(map), textureParent);
			layout.transform.position = new Vector3(0.0f, 0.0f, 3.0f);
			layout.layer = LayerMask.NameToLayer("LitBackground");
		}
		if(CreateDarkLayout)
		{
			layout = Visualize("DarkLayout", CreateTexture(map, true), textureParent);
			layout.transform.position = new Vector3(0.0f, 0.0f, 2.0f);
		}

		Progress += 0.05f;
		if(LogSteps)
			Debug.Log("Texture done. (" + (Time.time - intervall) + " secs)");



		// Instantiate door prefabs
		GameObject propParent = new GameObject("Props");
		propParent.transform.SetParent(parent.transform);
		if(DoorPrefab != null)
		{
			intervall = Time.time;
			GameObject doorParent = new GameObject("Doors");
			doorParent.transform.SetParent(propParent.transform);
			yield return StartCoroutine(InstantiateDoors(doorParent));
			if(LogSteps)
				Debug.Log("Doors done. (" + (Time.time - intervall) + " secs)");
		}
		else if(LogSteps)
			Debug.Log("No door prefab assigned; skipping.");
		Progress += 0.1f;



		// Populate rooms with furniture
		if(FurniturePrefabs != null)
		{
			PrepareFurnitureArray();
			if(FurniturePrefabs.Length > 0)
			{
				intervall = Time.time;
				GameObject furnitureParent = new GameObject("Furniture");
				furnitureParent.transform.SetParent(propParent.transform);
				yield return StartCoroutine(InstantiateFurniture(furnitureParent));
				if(LogSteps)
					Debug.Log("Furniture done. (" + (Time.time - intervall) + " secs)");
			}
		}
		else if(LogSteps)
			Debug.Log("No furniture prefabs assigned; skipping.");
		Progress += 0.05f;



		// Create spawn point for each room
		intervall = Time.time;
		GameObject spawnParent = new GameObject("SpawnPoints");
		spawnParent.transform.SetParent(parent.transform);
		GameObject point = null;
		for(int i = 0; i < roomList.Count; i++)
		{
			point = new GameObject("Spawn" + i);
			point.transform.SetParent(spawnParent.transform);
			point.transform.position = new Vector2(roomList[i].center.x - (Width / 2.0f), roomList[i].center.y - (Height / 2.0f));
			point.tag = "Respawn";
		}
		if(LogSteps)
			Debug.Log("Spawns done. (" + (Time.time - intervall) + " secs)");

		Progress += 0.05f;



		// Generate navigation mesh
		if(CreateNavMesh)
		{
			intervall = Time.time;

			GenerateNavMesh();

			if(LogSteps)
				Debug.Log("NavMesh done. (" + (Time.time - intervall) + " secs)");
		}



		// Create collision for walls
		intervall = Time.time;
		GameObject colliderParent = new GameObject("Collision");
		colliderParent.transform.SetParent(parent.transform);
		if(CreateWallCollision)
			yield return StartCoroutine(CreateCollision(colliderParent, map));

		Progress += 0.05f;
		if(LogSteps)
			Debug.Log("Collision done. (" + (Time.time - intervall) + " secs)");



		Progress += 0.1f;

		CleanUp();

		Progress = 1.0f;
		Generating = false;

		if(LogSteps)
			Debug.Log("Generation done. (" + (Time.time - total) + " secs)");
	}

	private void GenerateHouse()
	{
		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{
				map[x, y] = (int)ETileType.Wall;
			}
		}

		if(VisualizeSteps)
		{
			Visualize("House", CreateTextureOfArea(new Rect(0.0f, 0.0f, Width, Height), WallColor));
		}
	}

	private IEnumerator GenerateHallways(int _Count, int _Margin, int _Width)
	{
		hallwayList = new List<Rect>();
		chunkList = new List<Rect>();
		chunkList.Add(new Rect(1, 1, Width - 2, Height - 2));
		List<Rect> done = new List<Rect>();

		Vector2 direction;
		if(CoinFlip())
			direction = Vector2.right;
		else
			direction = Vector2.up;

		int tooSmall = 0;
		float startProgress = Progress;

		int yieldCount = 0;

		// Generate hallways
		// TODO: Split all current chunks with same direction, rotate and split the newly created ones 
		// to ensure all hallways are connected
		for(int i = 0; i < _Count; i++)
		{
			// Prevent freezing the application
			yieldCount++;
			if(yieldCount >= 10000)
			{
				break;
			}

			// Abort if no chunks are left
			if(chunkList.Count == 0)
			{
				chunkList.AddRange(done);
				done.Clear();

				// Rotate edge by 90 degrees
				if(direction == Vector2.right)
					direction = Vector2.up;
				else
					direction = Vector2.right;
			}

			
			//Debug.Log("Splitting in direction: " + (direction.x > 0.0f ? "vertical " : "horizontal ") + direction);

			// Get random chunk to split
			Rect chunk = chunkList[Random.Range(0, chunkList.Count)];
			//Debug.Log("Selected chunk: " + chunk);

			// Continue if chunk is too small to split
			if((direction.y == 0.0f && chunk.width <= _Margin * 2.0f + _Width)
				|| (direction.x == 0.0f && chunk.height <= _Margin * 2.0f + _Width))
			{
				tooSmall++;
				i--;
				//Debug.LogWarning("Chunk too small to split.");

				if(chunkList.Count == 1 || chunkList.Count == tooSmall)
				{
					//Debug.LogError("No other chunks available; stopping hallway creation.");
					break;
				}
				else
				{
					continue;
				}
			}

			//Remove chunk from the list
			chunkList.Remove(chunk);

			// Get random point on chunk edge
			Vector2 start = new Vector2(Random.Range((int)chunk.xMin + _Margin, (int)chunk.xMax - _Margin - _Width) * direction.x, Random.Range((int)chunk.yMin + _Margin, (int)chunk.yMax - _Margin - _Width) * direction.y);
			//Debug.Log("Getting edge (" + ((int)chunk.xMin + Margin) + "-" + ((int)chunk.xMax - Margin) + ", " + ((int)chunk.yMin + Margin) + "-" + ((int)chunk.yMax - Margin) + "): " + start);

			// Get hallway along edge
			Rect hallway;
			if(start.x == 0.0f)
				hallway = new Rect(chunk.x, start.y, chunk.width, _Width);
			else
				hallway = new Rect(start.x, chunk.y, _Width, chunk.height);
			hallwayList.Add(hallway);
			//Debug.Log("Created hallway: " + hallway);

			if(VisualizeSteps)
			{
				Visualize("Hallway" + (hallwayList.Count - 1), CreateTextureOfArea(hallway, HallColor), parent);
				yield return new WaitForSeconds(VisualizeSpeed);
			}
			
			// Split chunk in two new separate ones along edge
			Rect first;
			Rect second;
			if(start.x == 0.0f)	// Horizontal
			{
				first = new Rect(chunk.x, chunk.y, chunk.width, start.y - chunk.y);
				second = new Rect(chunk.x, start.y + _Width, chunk.width, chunk.height - first.height - _Width);
			}
			else // Vertical
			{
				first = new Rect(chunk.x, chunk.y, start.x - chunk.x, chunk.height);
				second = new Rect(start.x + _Width, chunk.y, chunk.width - first.width - _Width, chunk.height);
			}
			//Debug.Log("Created chunks: " + first + ", " + second);

			// Add new chunks to list
			done.Add(first);
			done.Add(second);

			Progress = startProgress + ((float)i / (float)HallwayCount) * 0.05f;
		}

		chunkList.AddRange(done);

		yield return new WaitForEndOfFrame();

		// Update map with hallways
		for(int i = 0; i < hallwayList.Count; i++)
		{
			for(int x = (int)hallwayList[i].xMin; x <  (int)hallwayList[i].xMax; x++)
			{
				for(int y = (int)hallwayList[i].yMin; y < (int)hallwayList[i].yMax; y++)
				{
					map[x, y] = (int)ETileType.Hall;
				}
			}
			//Visualize("Hallway" + i, CreateTextureOfArea(hallwayList[i], HallColor));
		}

		// Update map with chunk walls
		for(int i = 0; i < chunkList.Count; i++)
		{
			for(int x = (int)chunkList[i].xMin; x < (int)chunkList[i].xMax; x++)
			{
				for(int y = (int)chunkList[i].yMin; y < (int)chunkList[i].yMax; y++)
				{
					if(map[x - 1, y] == (int)ETileType.Hall
						|| map[x + 1, y] == (int)ETileType.Hall
						|| map[x, y - 1] == (int)ETileType.Hall
						|| map[x, y + 1] == (int)ETileType.Hall)
					{
						map[x, y] = (int)ETileType.Wall;
					}
					else
					{
						map[x, y] = (int)ETileType.Floor;
					}
				}
			}

			if(VisualizeSteps)
			{
				Visualize("Chunk" + i, CreateTextureOfArea(chunkList[i], FloorColor), parent);
				yield return new WaitForSeconds(VisualizeSpeed);
			}
		}

		Progress = startProgress + 0.05f;
	}

	private IEnumerator GenerateRooms()
	{
		roomList = new List<Rect>();
		doorList = new List<Rect>();
		hallwayDoorList = new List<Rect>();

		roomInstances = new List<Room>();

		float startProgress = Progress; // 0.3

		float area = 0.0f;
		int roomCount = 0;
		for(int i = 0; i < chunkList.Count; i++)
		{
			area = chunkList[i].width * chunkList[i].height;
			roomCount = (int)area / 2 + Random.Range(0, 5);
			//Debug.Log("Room #" + i + ": area=" + area + ", rooms=" + roomCount);
			yield return StartCoroutine(GenerateRoomsForChunk(chunkList[i], roomCount, RoomMargin, 1));
			//Debug.Log("Room #" + i + " done.");
			yield return new WaitForEndOfFrame();

			Progress = startProgress + ((float)i / (float)HallwayCount) * 0.6f;
		}

		Progress = startProgress + 0.6f;
	}

	private IEnumerator GenerateRoomsForChunk(Rect _Chunk, int _Count, int _Margin, int _Width)
	{
		List<Rect> wallList = new List<Rect>();
		List<Rect> chunkRooms = new List<Rect>();
		chunkRooms.Add(_Chunk);

		List<Rect> finishedRooms = new List<Rect>();

		Vector2 direction;
		float area = 0.0f;

		int yieldCount = 0;
		GameObject wallParent = null;
		if(VisualizeSteps)
		{
			wallParent = new GameObject("GeneratedWalls");
			if(parent != null)
				wallParent.transform.SetParent(parent.transform);
		}

		// Generate walls
		// TODO: Leave some rooms larger at random
		while(chunkRooms.Count > 0)
		{
			// Prevent freezing the application
			yieldCount++;
			if(yieldCount >= 10000)
			{
				yieldCount = 0;
				yield return new WaitForEndOfFrame();
			}

			// Abort if no rooms are left
			if(chunkRooms.Count == 0)
				break;

			// Sort rooms by size
			chunkRooms.Sort((one, two) =>
				{
					float areaOne = one.width * one.height;
					float areaTwo = two.width * two.height;

					if(areaOne > areaTwo)
						return -1;
					else if(areaOne < areaTwo)
						return 1;
					else
						return 0;
				}
				);

			// Get random room to split
			Rect room = chunkRooms[0];
			//Debug.Log("Selected room: " + room);

			// Remove chunk from list
			chunkRooms.Remove(room);

			// Finish this room if it is too small to split
			area = room.width * room.height;
			if(area <= _Margin * _Margin || (room.width < _Margin * 2.0f && room.height < _Margin * 2.0f))
			{
				finishedRooms.Add(room);
				continue;
			}

			// Split longer edge
			if(room.width > room.height)
				direction = Vector2.right;
			else
				direction = Vector2.up;
			//Debug.Log("Splitting edge: " + (direction == Vector2.up ? "vertical" : "horizontal"));

			// Get possible edge ranges
			Coordinate xRange = new Coordinate((int)room.xMin + _Margin, (int)room.xMax - _Margin);
			Coordinate yRange = new Coordinate((int)room.yMin + _Margin, (int)room.yMax - _Margin);

			// Get random point on room edge
			Vector2 start = new Vector2(Random.Range(xRange.x, xRange.y) * direction.x, Random.Range(yRange.x, yRange.y) * direction.y);
			//Debug.Log("Getting edge (" + ((int)room.xMin + _Margin) + "-" + ((int)room.xMax - _Margin) + ", " + ((int)room.yMin + _Margin) + "-" + ((int)room.yMax - _Margin) + "): " + start);

			// Get wall along edge
			Rect wall;
			if(start.x == 0.0f)
				wall = new Rect(room.x, start.y, room.width, _Width);
			else
				wall = new Rect(start.x, room.y, _Width, room.height);
			wallList.Add(wall);
			//Debug.Log("Created wall: " + wall);

			if(VisualizeSteps)
			{
				Visualize("Wall" + (wallList.Count - 1), CreateTextureOfArea(wall, WallColor), wallParent);
				yield return new WaitForSeconds(VisualizeSpeed);
			}

			// Split room in two new separate ones along edge
			Rect first;
			Rect second;
			if(start.x == 0.0f)	// Horizontal
			{
				first = new Rect(room.x, room.y, room.width, start.y - room.y);
				second = new Rect(room.x, start.y + _Width, room.width, room.height - first.height - _Width);
			}
			else // Vertical
			{
				first = new Rect(room.x, room.y, start.x - room.x, room.height);
				second = new Rect(start.x + _Width, room.y, room.width - first.width - _Width, room.height);
			}
			//Debug.Log("Created rooms: " + first + ", " + second);

			// Add new rooms to list
			chunkRooms.Add(first);
			chunkRooms.Add(second);
		}

		// Add created rooms to complete room list
		roomList.AddRange(finishedRooms);

		// Create room instances for later reference
		Room instance;
		for(int i = 0; i < finishedRooms.Count; i++)
		{
			instance = new Room(finishedRooms[i], Width, Height);
			for(int j = 0; j < 4; j++)
				instance.SetInnerEdge(GetInnerRoomEdge(instance.Bounds, j), (Room.EEdge)j);
			instance.SetHallwayEdges(GetNeighboringHallwayEdges(instance.Bounds).ToArray());
			instance.CorrectBounds();
			roomInstances.Add(instance);
		}
		
		// Update map with walls
		for(int i = 0; i < wallList.Count; i++)
		{
			for(int x = (int)wallList[i].xMin; x < (int)wallList[i].xMax; x++)
			{
				for(int y = (int)wallList[i].yMin; y < (int)wallList[i].yMax; y++)
				{
					map[x, y] = (int)ETileType.Wall;
				}
			}
		}

		GameObject doors = null;
		if(VisualizeSteps)
		{
			doors = new GameObject("GenerateDoors");
			if(parent != null)
				doors.transform.SetParent(parent.transform);
		}
		yield return StartCoroutine(GenerateDoorsForChunk(finishedRooms, doors));
	}

	private IEnumerator GenerateDoorsForChunk(List<Rect> _Rooms, GameObject _Parent)
	{
		List<Rect> doors = new List<Rect>();

		// Get all edges at hallways
		List<Rect> edges = new List<Rect>();
		for(int i = 0; i < _Rooms.Count; i++)
		{
			List<Rect> e = GetNeighboringHallwayEdges(_Rooms[i]);
			edges.AddRange(e);

			if(VisualizeSteps)
			{
				if(e.Count > 0)
					Visualize("RoomByHallway" + i, CreateTextureOfArea(_Rooms[i], Color.green), _Parent);
				else
					Visualize("Room" + i, CreateTextureOfArea(_Rooms[i], Color.red), _Parent);

				yield return new WaitForSeconds(VisualizeSpeed);
			}
		}

		// Remove edges that are too short
		for(int i = 0; i < edges.Count; i++)
		{
			if((edges[i].width == 1 && edges[i].height < 3)
				|| (edges[i].height == 1 && edges[i].width < 3))
			{
				if(VisualizeSteps)
				{
					Visualize("InvalidEdge" + i, CreateTextureOfArea(edges[i], Color.red * Color.gray), _Parent);
					yield return new WaitForSeconds(VisualizeSpeed);
				}
				edges.RemoveAt(i);
				i--;
			}
			else
			{
				if(VisualizeSteps)
				{
					Visualize("ValidEdge" + i, CreateTextureOfArea(edges[i], Color.green * Color.gray), _Parent);
					yield return new WaitForSeconds(VisualizeSpeed);
				}
			}
		}

		// Generate at least one door to connect the hallway
		Rect door;
		Rect edge;
		if(edges.Count > 0)
		{
			int edgeDoors = Random.Range(Mathf.Max(1, edges.Count / 3), edges.Count);
			for(int i = 0; i < edgeDoors; i++)
			{
				// Get random edge and remove it from the list
				edge = edges[Random.Range(0, edges.Count)];
				edges.Remove(edge);

				// Get random point on the edge
				if(edge.width == 1)
				{
					door = new Rect(edge.x, Random.Range((int)edge.yMin, (int)edge.yMax - 3), 1, 3);
				}
				else
				{
					door = new Rect(Random.Range((int)edge.xMin, (int)edge.xMax - 3), edge.y, 3, 1);
				}

				if(VisualizeSteps)
				{
					Visualize("Door" + i, CreateTextureOfArea(door, Color.blue), _Parent);
					yield return new WaitForSeconds(VisualizeSpeed);
				}

				doors.Add(door);
			}

			hallwayDoorList.AddRange(doors);
		}

		if(_Rooms.Count > 1)
		{
			// Connect all rooms with doors
			List<Rect> active = new List<Rect>(_Rooms);
			List<Rect> finished = new List<Rect>();
			List<Rect> neighbors = new List<Rect>();
			Rect current = active[Random.Range(0, active.Count)];
			Rect next = new Rect();
			edge = new Rect();
			List<Rect> takenEdges = new List<Rect>();

			while(active.Count > 0)
			{
				// Get neighbors
				neighbors = GetNeighboringRooms(current, new List<Rect>(active));

				// Start new line if a dead end is reached
				if(neighbors.Count < 1)
				{
					active.Remove(current);

					if(VisualizeSteps)
					{
						Visualize("DeadEndRoom", CreateTextureOfArea(current, new Color(0.0f, 1.0f, 1.0f, 0.5f)), _Parent);
						yield return new WaitForSeconds(VisualizeSpeed);
					}
					
					for(int i = 0; i < finished.Count; i++)
					{
						neighbors = GetNeighboringRooms(finished[i], new List<Rect>(active));
						if(neighbors.Count > 0)
						{
							if(!finished.Contains(current))
								finished.Add(current);
							current = finished[i];

							if(VisualizeSteps)
							{
								Visualize("NewStartRoom", CreateTextureOfArea(current, new Color(1.0f, 0.0f, 1.0f, 0.5f)), _Parent);
								yield return new WaitForSeconds(VisualizeSpeed);
							}

							break;
						}
					}
				}
				else
				{
					if(VisualizeSteps)
					{
						Visualize("RoomInLine", CreateTextureOfArea(current, new Color(0.0f, 0.0f, 0.0f, 0.5f)), _Parent);
						yield return new WaitForSeconds(VisualizeSpeed);
					}

					// Create door with random neighbor
					if(neighbors.Count > 0)
					{
						if(VisualizeSteps)
						{
							for(int i = 0; i < neighbors.Count; i++)
							{
								Visualize("NeighboringRoom", CreateTextureOfArea(neighbors[i], new Color(1.0f, 1.0f, 0.0f, 0.25f)), _Parent);
								yield return new WaitForSeconds(VisualizeSpeed);
							}
						}

						// Get random neighbor and shared edge
						for(int i = 0; i < neighbors.Count; i++)
						{
							next = neighbors[Random.Range(0, neighbors.Count)];
							edge = GetSharedEdge(current, next);

							if(takenEdges.Contains(edge))
							{
								neighbors.Remove(next);
								i--;
								continue;
							}
							else
							{
								takenEdges.Add(edge);

								if(VisualizeSteps)
								{
									Visualize("SharedEdge", CreateTextureOfArea(edge, new Color(1.0f, 1.0f, 1.0f, 0.25f)), _Parent);
									yield return new WaitForSeconds(VisualizeSpeed);
								}

								door = AddDoorOnEdge(edge);

								if(VisualizeSteps)
								{
									if(door.width == 1)
										Visualize("VerticalInternalDoor", CreateTextureOfArea(door, Color.blue), _Parent);
									else
										Visualize("HorizontalInternalDoor", CreateTextureOfArea(door, Color.blue), _Parent);
									yield return new WaitForSeconds(VisualizeSpeed);
								}

								doors.Add(door);
								break;
							}
						}
					}

					// Remove processed room from active list and get next room's neighbors
					active.Remove(current);
					if(!finished.Contains(current))
						finished.Add(current);
					if(active.Count > 0)
						current = next;
				}
			}

			active.Clear();
			finished.Clear();
			neighbors.Clear();

			// Add additional doors randomly
			active = new List<Rect>(_Rooms);
			int doorCount = Random.Range(Mathf.Max(0, (int)(active.Count * 0.1f)), Mathf.Max(0, (int)(active.Count * 0.25f)));
			for(int i = 0; i < doorCount; i++)
			{
				if(active.Count < 1)
					break;

				current = active[Random.Range(0, active.Count)];
				neighbors = GetNeighboringRooms(current, new List<Rect>(_Rooms));

				if(VisualizeSteps)
				{
					Visualize("RandomRoom", CreateTextureOfArea(current, new Color(0.0f, 0.0f, 0.0f, 1.0f)), _Parent);
					yield return new WaitForSeconds(VisualizeSpeed);
				}

				// Create door with random neighbor
				if(VisualizeSteps)
				{
					for(int j = 0; j < neighbors.Count; j++)
					{
						Visualize("NeighboringRoom", CreateTextureOfArea(neighbors[j], new Color(1.0f, 1.0f, 0.0f, 0.75f)), _Parent);
						yield return new WaitForSeconds(VisualizeSpeed);
					}
				}

				// Get random neighbor and shared edge
				for(int j = 0; j < neighbors.Count; j++)
				{
					next = neighbors[Random.Range(0, neighbors.Count)];
					edge = GetSharedEdge(current, next);

					if(takenEdges.Contains(edge))
					{
						neighbors.Remove(next);
						if(neighbors.Count < 1)
						{
							active.Remove(current);
							i--;
							break;
						}

						j--;
						continue;
					}
					else
					{
						takenEdges.Add(edge);

						if(VisualizeSteps)
						{
							Visualize("SharedEdge", CreateTextureOfArea(edge, new Color(1.0f, 1.0f, 1.0f, 0.25f)), _Parent);
							yield return new WaitForSeconds(VisualizeSpeed);
						}

						door = AddDoorOnEdge(edge);

						if(VisualizeSteps)
						{
							if(door.width == 1)
								Visualize("VerticalInternalDoor", CreateTextureOfArea(door, Color.blue), _Parent);
							else
								Visualize("HorizontalInternalDoor", CreateTextureOfArea(door, Color.blue), _Parent);
							yield return new WaitForSeconds(VisualizeSpeed);
						}

						doors.Add(door);
						break;
					}
				}
			}
		}

		// Save door references for instantiation
		doorList.AddRange(doors);

		// Update map with doors
		for(int i = 0; i < doors.Count; i++)
		{
			// Horizontal door
			if(doors[i].width > doors[i].height)
			{
				for(int x = (int)doors[i].xMin; x < (int)doors[i].xMax; x++)
				{
					map[x, (int)doors[i].y] = (int)ETileType.Door;
				}
			}
			// Vertical door
			else
			{
				for(int y = (int)doors[i].yMin; y < (int)doors[i].yMax; y++)
				{
					map[(int)doors[i].x, y] = (int)ETileType.Door;
				}
			}
		}
	}

	#endregion



	#region CREATION

	private Texture2D CreateTexture(int[,] _Map, bool _Dark = false)
	{
		Texture2D texture = new Texture2D(Width, Height, TextureFormat.RGB24, false);
		texture.filterMode = FilterMode.Point;

		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{
				if(_Map[x, y] == (int)ETileType.Hall)
					texture.SetPixel(x, y, _Dark ? DarkHallColor : HallColor);
				else if(_Map[x, y] == (int)ETileType.Floor)
					texture.SetPixel(x, y, _Dark ? DarkFloorColor : FloorColor);
				else if(_Map[x, y] == (int)ETileType.Wall)
					texture.SetPixel(x, y, _Dark ? DarkWallColor : WallColor);
				else if(_Map[x, y] == (int)ETileType.Door)
					texture.SetPixel(x, y, _Dark ? DarkDoorColor : DoorColor);
			}
		}

		texture.Apply();

		//Debug.Log(this.GetType() + ": Created texture.");
		lastMap = texture;
		return texture;
	}

	private Texture2D CreateTexture(int[,] _Map, Color _Multiplier)
	{
		Texture2D texture = new Texture2D(Width, Height, TextureFormat.RGB24, false);
		texture.filterMode = FilterMode.Point;

		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{
				if(_Map[x, y] == (int)ETileType.Hall)
					texture.SetPixel(x, y, HallColor * _Multiplier);
				else if(_Map[x, y] == (int)ETileType.Floor)
					texture.SetPixel(x, y, FloorColor * _Multiplier);
				else if(_Map[x, y] == (int)ETileType.Wall)
					texture.SetPixel(x, y, WallColor * _Multiplier);
				else if(_Map[x, y] == (int)ETileType.Door)
					texture.SetPixel(x, y, DoorColor * _Multiplier);
			}
		}

		texture.Apply();

		//Debug.Log(this.GetType() + ": Created texture.");
		lastMap = texture;
		return texture;
	}

	private Texture2D CreateTextureOfArea(Rect _Area, Color _Color)
	{
		Color transparent = new Color(1.0f, 1.0f, 1.0f, 0.0f);

		Texture2D texture = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
		texture.filterMode = FilterMode.Point;

		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{
				if(_Area.Contains(new Vector2(x, y)))
					texture.SetPixel(x, y, _Color);
				else
					texture.SetPixel(x, y, transparent);
			}
		}

		texture.Apply();

		//Debug.Log(this.GetType() + ": Created texture.");
		return texture;
	}

	private IEnumerator CreateCollision(GameObject _Parent, int[,] _Map)
	{
		Coordinate horizontal = null;
		Coordinate vertical = null;

		Coordinate first = GetFirstWallIndex(_Map);
		Coordinate last = null;

		GameObject collider = null;

		int yieldCount = 0;

		while(first != null)
		{
			// Wait until next frame
			yieldCount++;
			if(yieldCount > 50)
			{
				yield return new WaitForEndOfFrame();
				yieldCount = 0;
			}

			//Debug.Log("Current start: " + first.ToString());

			// Get longest horizontal and vertical lines
			horizontal = GetHorizontalLine(first, _Map);
			vertical = GetVerticalLine(first, _Map);

			//Debug.Log("Horizontal from " + Coordinate(first) + " to " + Coordinate(horizontal) + " [" + (horizontalx - firstx) + "]");
			//Debug.Log("Vertical from " + Coordinate(first) + " to " + Coordinate(vertical) + " [" + (vertical.y - first.y) + "]");

			// Choose longest line
			if(horizontal.x - first.x >= vertical.y - first.y)
			{
				last = horizontal;
				//Debug.Log("Horizontal line chosen.");
			}
			else
			{
				last = vertical;
				//Debug.Log("Vertical line chosen.");
			}

			// Create collider for line
			collider = CreateColliderFromLine(first, last);
			collider.transform.SetParent(_Parent.transform);
			collider.layer = LayerMask.NameToLayer("ShadowLayer");

			// Remove wall from map
			RemoveLineFromMap(first, last, ref _Map);

			// Get next coordinates
			first = GetFirstWallIndex(_Map);
		}

		//Debug.Log(this.GetType() + ": Created collision.");
	}

	private GameObject CreateColliderFromLine(Coordinate first, Coordinate last)
	{
		GameObject obj = new GameObject("Collider");
		obj.transform.position = new Vector3(0.5f - (Width / 2.0f), 0.5f - (Height / 2.0f), 0.0f);
		PolygonCollider2D collider = obj.AddComponent<PolygonCollider2D>();

		Vector2[] points = new Vector2[] {
			new Vector2(first.x - 0.5f, first.y - 0.5f),
			new Vector2(last.x + 0.5f, first.y - 0.5f),
			new Vector2(last.x + 0.5f, last.y + 0.5f),
			new Vector2(first.x - 0.5f, last.y + 0.5f)
		};

		collider.SetPath(0, points);

		return obj;
	}

	private IEnumerator InstantiateDoors(GameObject _Parent)
	{
		List<Rect> doorsToInstantiate = new List<Rect>(doorList);
		GameObject instance = null;
		HingeJoint2D hinge = null;
		Vector3 position;
		Quaternion rotation;
		int yieldCount = 0;
		while(doorsToInstantiate.Count > 0)
		{
			// Yield after a few iterations to avoid freezing the game
			yieldCount++;
			if(yieldCount > 20)
			{
				yieldCount = 0;
				yield return new WaitForEndOfFrame();
			}

			// Get position and rotation
			// Horizontal
			if(doorsToInstantiate[0].height == 1)
			{
				if(CoinFlip())
				{
					position = new Vector3(doorsToInstantiate[0].xMax, doorsToInstantiate[0].y + 0.5f, 0.0f);
					rotation = Quaternion.AngleAxis(180.0f, Vector3.forward);
				}
				else
				{
					position = new Vector3(doorsToInstantiate[0].xMin, doorsToInstantiate[0].y + 0.5f, 0.0f);
					rotation = Quaternion.identity;
				}
			}
			// Vertical
			else
			{
				if(CoinFlip())
				{
					position = new Vector3(doorsToInstantiate[0].x + 0.5f, doorsToInstantiate[0].yMin, 0.0f);
					rotation = Quaternion.AngleAxis(90.0f, Vector3.forward);
				}
				else
				{
					position = new Vector3(doorsToInstantiate[0].x + 0.5f, doorsToInstantiate[0].yMax, 0.0f);
					rotation = Quaternion.AngleAxis(270.0f, Vector3.forward);
				}
			}

			position.x -= Width / 2.0f;
			position.y -= Height / 2.0f;

			// Instantiate door prefab
			instance = Instantiate<GameObject>(DoorPrefab);
			instance.name = "Door";
			if(_Parent != null)
				instance.transform.SetParent(_Parent.transform);

			// Apply position and rotation
			instance.transform.position = position;
			instance.transform.rotation = rotation;

			// Apply hinge anchors
			hinge = instance.GetComponent<HingeJoint2D>();
			if(hinge != null)
			{
				hinge.connectedBody = null;
				hinge.connectedAnchor = instance.transform.position;
				//hinge.referenceAngle = -transform.eulerAngles.z;
			}

			// Remove processed door
			doorsToInstantiate.RemoveAt(0);
		}
	}

	private IEnumerator InstantiateFurniture(GameObject _Parent)
	{
		List<Rect> occupiedSpots = new List<Rect>();

		// Add all door spaces to taken spot list
		for(int i = 0; i < doorList.Count; i++)
		{
			if(doorList[i].width == 1)
			{
				occupiedSpots.Add(new Rect(doorList[i].x - 3.0f, doorList[i].y, 7.0f, doorList[i].height));
			}
			else if(doorList[i].height == 1)
			{
				occupiedSpots.Add(new Rect(doorList[i].x, doorList[i].y - 3.0f, doorList[i].width, 7.0f));
			}
		}

		if(VisualizeSteps)
		{
			GameObject go = new GameObject("DoorSpaceOccupation");
			for(int i = 0; i < occupiedSpots.Count; i++)
			{
				Visualize("OccupiedSpot" + i, CreateTextureOfArea(occupiedSpots[i], Color.yellow), go);
				yield return new WaitForSeconds(VisualizeSpeed);
			}
		}

		GameObject currentRoomObj = null;

		List<Furniture> possiblePrefabs = new List<Furniture>();
		int objectSizes = System.Enum.GetNames(typeof(Furniture.ESize)).Length;
		int[] objectAmounts;

		// Process every room
		float chanceTotal = 0.0f;
		float chanceIntervall = 0.0f;
		float chance = 0.0f;
		Furniture prefab = null;
		for(int room = 0; room < roomInstances.Count; room++)
		{
			possiblePrefabs.Clear();

			// Update occupied spots
			for(int i = 0; i < occupiedSpots.Count; i++)
			{
				roomInstances[room].AddOccupiedSpot(occupiedSpots[i]);
			}

			// Set tag randomly
			if(Templates.Length > 0)
			{
				RoomTemplate template = Templates[Random.Range(0, Templates.Length)];
				roomInstances[room].Tag = template.Tag;
				objectAmounts = template.Objects;

				// Get possible furniture
				for(int i = 0; i < FurniturePrefabs.Length; i++)
				{
					if(FurniturePrefabs[i].HasTag(template.Tag))
						possiblePrefabs.Add(FurniturePrefabs[i]);
				}
			}
			else
			{
				roomInstances[room].Tag = "";
				objectAmounts = new int[objectSizes];
				for(int i = 0; i < objectAmounts.Length; i++)
					objectAmounts[i] = 4;

				possiblePrefabs.AddRange(FurniturePrefabs);
			}

			if(VisualizeSteps)
			{
				currentRoomObj = new GameObject("Room" + room + "SpaceOccupation");
				Color c = Color.gray;
				if(roomInstances[room].Tag == "Bedroom")
					c = Color.cyan;
				else if(roomInstances[room].Tag == "Living Room")
					c = Color.blue;
				Visualize("Bounds", CreateTextureOfArea(roomInstances[room].Bounds, c), currentRoomObj);
			}

			// Iterate all object sizes
			for(int size = objectSizes - 1; size >= 0; size--)
			{
				// Calculate total prefab chance
				chanceTotal = 0.0f;
				for(int i = 0; i < FurniturePrefabs.Length; i++)
				{
					if(FurniturePrefabs[i].Size == (Furniture.ESize)size)
						chanceTotal += FurniturePrefabs[i].Chance;
				}

				// Continue if no prefabs of the correct size exist
				if(chanceTotal == 0.0f)
					continue;

				// Try placing the specified amount of objects
				for(int count = 0; count < objectAmounts[size]; count++)
				{
					// Choose random prefab by roulette wheel method
					chance = Random.Range(0.0f, chanceTotal);
					chanceIntervall = 0.0f;
					for(int i = 0; i < FurniturePrefabs.Length; i++)
					{
						if(FurniturePrefabs[i].Size != (Furniture.ESize)size)
							continue;

						chanceIntervall += FurniturePrefabs[i].Chance;
						if(chance <= chanceIntervall)
						{
							prefab = FurniturePrefabs[i];
							break;
						}
					}

					// Try adding furniture to the room
					roomInstances[room].AddFurniture(prefab, _Parent.transform, 200);
				}
			}

			yield return new WaitForEndOfFrame();

			if(VisualizeSteps)
			{
				Rect[] occupied = roomInstances[room].GetOccupiedSpots();
				for(int i = 0; i < occupied.Length; i++)
				{
					Visualize("OccupiedSpot" + i, CreateTextureOfArea(occupied[i], Color.magenta), currentRoomObj);
					yield return new WaitForSeconds(VisualizeSpeed);
				}
			}
		}
	}

	private void GenerateNavMesh()
	{
		GameObject obj = new GameObject("NavMesh");
		NavMesh mesh = obj.AddComponent<NavMesh>();

		List<NavMeshNode> nodes = new List<NavMeshNode>();
		NavMeshNode node;

		// Add hallway nodes
		for(int i = 0; i < hallwayList.Count; i++)
		{
			// Create node
			node = new NavMeshNode(hallwayList[i]);

			// Shrink to accommodate player size
			if(node.Bounds.xMin > 1.0f && node.Bounds.xMax == Width - 1.0f)
				node.Bounds.xMin -= 1.0f;
			else if(node.Bounds.xMin == 1.0f && node.Bounds.xMax < Width - 1.0f)
				node.Bounds.xMax += 1.0f;
			else if(node.Bounds.yMin > 1.0f && node.Bounds.yMax == Height - 1.0f)
				node.Bounds.yMin -= 1.0f;
			else if(node.Bounds.yMin == 1.0f && node.Bounds.yMax < Height - 1.0f)
				node.Bounds.yMax += 1.0f;

			node.Bounds.x += 0.5f;
			node.Bounds.y += 0.5f;
			node.Bounds.width -= 1.0f;
			node.Bounds.height -= 1.0f;

			node.Bounds.x -= Width / 2.0f;
			node.Bounds.y -= Height / 2.0f;

			// Add node to list
			nodes.Add(node);
		}

		// Add room nodes
		for(int i = 0; i < roomInstances.Count; i++)
		{
			// Get walkable areas
			Rect[] walkable = roomInstances[i].GetWalkableAreas();

			// Create node
			for(int j = 0; j < walkable.Length; j++)
			{
				node = new NavMeshNode(walkable[j]);

				// Add node to list
				nodes.Add(node);
			}
		}

		// Add door nodes
		for(int i = 0; i < doorList.Count; i++)
		{
			// Create node
			node = new NavMeshNode(doorList[i]);

			if(node.Bounds.width == 1.0f)
			{
				node.Bounds.xMin -= 0.5f;
				node.Bounds.xMax += 0.5f;

				node.Bounds.yMin += 0.5f;
				node.Bounds.yMax -= 0.5f;
			}
			else if(node.Bounds.height == 1.0f)
			{
				node.Bounds.yMin -= 0.5f;
				node.Bounds.yMax += 0.5f;

				node.Bounds.xMin += 0.5f;
				node.Bounds.xMax -= 0.5f;
			}

			node.Bounds.x -= Width / 2.0f;
			node.Bounds.y -= Height / 2.0f;

			// Add node to list
			nodes.Add(node);
		}

		mesh.SetNodes(nodes);
		mesh.UpdateNodeNeighbors();
	}

	#endregion



	#region HELPERS

	private bool CoinFlip()
	{
		return Random.Range(0.0f, 1.0f) < 0.5f;
	}

	private GameObject Visualize(string _Name, Texture2D _Texture, GameObject _Parent)
	{
		GameObject obj = Visualize(_Name, _Texture);
		if(_Parent != null)
			obj.transform.SetParent(_Parent.transform);

		return obj;
	}

	private GameObject Visualize(string _Name, Texture2D _Texture)
	{
		GameObject obj = new GameObject(_Name);
		SpriteRenderer target = obj.AddComponent<SpriteRenderer>();

		if(VisualizeSteps)
		{
			target.sortingOrder = visualization;
			visualization++;
		}

		Sprite sprite = Sprite.Create(_Texture, new Rect(0.0f, 0.0f, Width, Height), new Vector2(0.5f, 0.5f), PixelsPerUnit);
		sprite.name = _Name + "_Sprite";
		target.sprite = sprite;
		target.material.mainTexture = _Texture as Texture;
		target.material.shader = Shader.Find("Sprites/Default");

		return obj;
	}

	private void SaveAsPNG(string _Path, Texture2D _Texture)
	{
		byte[] png = _Texture.EncodeToPNG();
		File.WriteAllBytes(Application.dataPath + "/" + _Path + ".png", png);
	}

	public void SaveLastAsPNG(string _Path)
	{
		if(!Generating && lastMap != null)
			SaveAsPNG(_Path, lastMap);
	}

	private Coordinate GetFirstWallIndex(int[,] _Map)
	{
		for(int x = 0; x < _Map.GetLength(0); x++)
		{
			for(int y = 0; y < _Map.GetLength(1); y++)
			{
				if(_Map[x, y] == (int)ETileType.Wall)
				{
					//Debug.Log(this.GetType() + ": Found wall at (" + x + ", " + y + ")");
					return new Coordinate(x, y);
				}
			}
		}

		//Debug.Log(this.GetType() + ": Found no more walls.");
		return null;
	}

	private Coordinate GetHorizontalLine(Coordinate first, int[,] _Map)
	{
		Coordinate last = new Coordinate(first.x, first.y);

		for(int i = first.x; i < _Map.GetLength(0); i++)
		{
			if(i + 1 >= _Map.GetLength(0) || _Map[i + 1, first.y] != (int)ETileType.Wall)
			{
				last.x = i;
				break;
			}
		}

		return last;
	}

	private Coordinate GetVerticalLine(Coordinate first, int[,] _Map)
	{
		Coordinate last = new Coordinate(first.x, first.y);

		for(int i = first.y; i < _Map.GetLength(1); i++)
		{
			if(i + 1 >= _Map.GetLength(1) || _Map[first.x, i + 1] != (int)ETileType.Wall)
			{
				last.y = i;
				break;
			}
		}

		return last;
	}

	private void RemoveLineFromMap(Coordinate first, Coordinate last, ref int[,] _Map)
	{
		// Line length 1
		if(first.x == last.x && first.y == last.y)
		{
			_Map[first.x, first.y] = (int)ETileType.Hall;
		}
		else
		{
			bool isHorizontal = first.x - last.x != 0;
			int length = isHorizontal ? last.x : last.y;
			int i = isHorizontal ? first.x : first.y;
			//Debug.Log("Removing " + (isHorizontal ? "horizontal" : "vertical") + " line from " + Coordinate(first) + " to " + Coordinate(last) + "; i=" + i + ", length=" + length);
			for(; i <= length; i++)
			{
				if(isHorizontal)
				{
					_Map[i, first.y] = (int)ETileType.Hall;
					//Debug.Log("Removed wall at (" + i + ", " + first.y + ") from the map.");
				}
				else
				{
					_Map[first.x, i] = (int)ETileType.Hall;
					//Debug.Log("Removed wall at (" + firstx + ", " + i + ") from the map.");
				}
			}
		}
	}

	private List<Rect> GetNeighboringHallwayEdges(Rect _Area)
	{
		List<Rect> edges = new List<Rect>();

		// Top
		if(map[(int)_Area.xMin + (int)_Area.width / 2, (int)_Area.yMax] == (int)ETileType.Hall)
		{
			edges.Add(new Rect(_Area.x + 1, _Area.yMax - 1, _Area.width - 2, 1));
		}

		// Bottom
		if(map[(int)_Area.xMin + (int)_Area.width / 2, (int)_Area.yMin - 1] == (int)ETileType.Hall)
		{
			edges.Add(new Rect(_Area.x + 1, _Area.yMin, _Area.width - 2, 1));
		}

		// Left
		if(map[(int)_Area.xMin - 1, (int)_Area.yMin + (int)_Area.height / 2] == (int)ETileType.Hall)
		{
			edges.Add(new Rect(_Area.xMin, _Area.y + 1, 1, _Area.height - 2));
		}

		// Right
		if(map[(int)_Area.xMax, (int)_Area.yMin + (int)_Area.height / 2] == (int)ETileType.Hall)
		{
			edges.Add(new Rect(_Area.xMax - 1, _Area.y + 1, 1, _Area.height - 2));
		}

		//Debug.Log("Found " + edges.Count + " neighboring edges for " + _Area);
		return edges;
	}

	private List<Rect> GetNeighboringRooms(Rect _Area, List<Rect> _Rooms)
	{
		// Remove self from rooms
		_Rooms.Remove(_Area);

		for(int i = 0; i < _Rooms.Count; i++)
		{
			// Ignore neighbor with short shared edge
			Rect edge = GetSharedEdge(_Area, _Rooms[i]);
			if((edge.width == 1 && edge.height < 2) 
				|| (edge.height == 1 && edge.width < 2))
			{
				_Rooms.RemoveAt(i);
				i--;

				continue;
			}

			if(edge.width != 1 && edge.height != 1)
			{
				_Rooms.RemoveAt(i);
				i--;

				continue;
			}
		}

		return _Rooms;
	}

	private Rect GetSharedEdge(Rect _First, Rect _Second)
	{
		Rect edge = new Rect();

		// Right edge
		if(_Second.xMin - _First.xMax == 1)
		{
			edge.x = _First.xMax;
			edge.y = Mathf.Max(_Second.yMin + 1, _First.yMin + 1);
			edge.width = _Second.xMin - _First.xMax;
			edge.height = Mathf.Min(_Second.yMax - 1, _First.yMax - 1) - edge.y;
		}
		// Left edge
		else if(_First.xMin - _Second.xMax == 1)
		{
			edge.x = _Second.xMax;
			edge.y = Mathf.Max(_Second.yMin + 1, _First.yMin + 1);
			edge.width = _First.xMin - _Second.xMax;
			edge.height = Mathf.Min(_Second.yMax - 1, _First.yMax - 1) - edge.y;
		}
		// Top edge
		else if(_Second.yMin - _First.yMax == 1)
		{
			edge.x = Mathf.Max(_Second.xMin + 1, _First.xMin + 1);
			edge.y = _First.yMax;
			edge.width = Mathf.Min(_Second.xMax - 1, _First.xMax - 1) - edge.x;
			edge.height = _Second.yMin - _First.yMax;
		}
		// Bottom edge
		else if(_First.yMin - _Second.yMax == 1)
		{
			edge.x = Mathf.Max(_Second.xMin + 1, _First.xMin + 1);
			edge.y = _Second.yMax;
			edge.width = Mathf.Min(_Second.xMax - 1, _First.xMax - 1) - edge.x;
			edge.height = _First.yMin - _Second.yMax;
		}

		//Debug.Log("width=" + edge.width + ", height=" + edge.height);

		return edge;
	}

	private Rect AddDoorBetween(Rect _First, Rect _Second)
	{
		// Add door along edge
		return AddDoorOnEdge(GetSharedEdge(_First, _Second));
	}

	private Rect AddDoorOnEdge(Rect _Edge)
	{
		Rect door;

		// Add door along edge
		if(_Edge.width == 1)
		{
			door = new Rect(_Edge.x, Random.Range((int)_Edge.yMin, (int)_Edge.yMax - 3), 1, 3);
		}
		else
		{
			door = new Rect(Random.Range((int)_Edge.xMin, (int)_Edge.xMax - 3), _Edge.y, 3, 1);
		}

		// Return door
		return door;
	}

	private Rect GetInnerRoomEdge(Rect _Room, int _Direction)
	{
		Rect edge = new Rect(_Room);

		switch(_Direction)
		{
			// Top
			case 0:
				{
					// Get edge
					edge = new Rect(_Room.xMin, _Room.yMax - 1, _Room.width, 1);

					// Correct edge where necessary
					for(int i = 0; i < hallwayList.Count; i++)
					{
						if(hallwayList[i].Contains(edge.center + Vector2.up))
						{
							edge.y -= 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.xMin - 1, edge.center.y)))
						{
							edge.x += 1.0f;
							edge.width -= 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.xMax + 1, edge.center.y)))
						{
							edge.width -= 1.0f;
						}
					}
				} break;

			// Bottom
			case 1:
				{
					// Get edge
					edge = new Rect(_Room.xMin, _Room.yMin, _Room.width, 1);

					// Correct edge where necessary
					for(int i = 0; i < hallwayList.Count; i++)
					{
						if(hallwayList[i].Contains(edge.center - Vector2.up))
						{
							edge.y += 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.xMin - 1, edge.center.y)))
						{
							edge.x += 1.0f;
							edge.width -= 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.xMax + 1, edge.center.y)))
						{
							edge.width -= 1.0f;
						}
					}
				} break;

			// Left
			case 2:
				{
					// Get edge
					edge = new Rect(_Room.xMin, _Room.yMin, 1, _Room.height);

					// Correct edge where necessary
					for(int i = 0; i < hallwayList.Count; i++)
					{
						if(hallwayList[i].Contains(edge.center - Vector2.right))
						{
							edge.x += 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.center.x, edge.yMin - 1)))
						{
							edge.y += 1.0f;
							edge.height -= 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.center.x, edge.yMax + 1)))
						{
							edge.height -= 1.0f;
						}
					}
				} break;

			// Right
			case 3:
				{
					// Get edge
					edge = new Rect(_Room.xMax - 1, _Room.yMin, 1, _Room.height);

					// Correct edge where necessary
					for(int i = 0; i < hallwayList.Count; i++)
					{
						if(hallwayList[i].Contains(edge.center + Vector2.right))
						{
							edge.x -= 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.center.x, edge.yMin - 1)))
						{
							edge.y += 1.0f;
							edge.height -= 1.0f;
						}
						if(hallwayList[i].Contains(new Vector2(edge.center.x, edge.yMax + 1)))
						{
							edge.height -= 1.0f;
						}
					}
				} break;
		}

		return edge;
	}

	private void CleanUp()
	{
		if(roomInstances != null)
		{
			for(int i = 0; i < roomInstances.Count; i++)
			{
				roomInstances[i].Destroy();
			}

			roomInstances.Clear();
			roomInstances = null;
		}
	}

	public void PrepareFurnitureArray()
	{
		List<Furniture> valid = new List<Furniture>(FurniturePrefabs);
		for(int i = 0; i < valid.Count; i++)
		{
			if(valid[i] == null)
			{
				valid.RemoveAt(i);
				i--;
			}
		}

		FurniturePrefabs = valid.ToArray();
	}

	#endregion
}