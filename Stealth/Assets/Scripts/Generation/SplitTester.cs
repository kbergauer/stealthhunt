﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplitTester : MonoBehaviour 
{
	public Rect Cutout = new Rect(0, 0, 1, 1);

	List<Rect> roomList = new List<Rect>();

	void Awake()
	{
		roomList.Add(new Rect(-5, -5, 10, 10));
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one * 10);

		Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
		Gizmos.DrawCube(Cutout.center, Cutout.size);
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(Cutout.center, Cutout.size);

		Gizmos.color = Color.green;
		for(int i = 0; i < roomList.Count; i++)
		{
			Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 0.5f);
			Gizmos.DrawCube(roomList[i].center, roomList[i].size);
			Gizmos.color = Color.green;
			Gizmos.DrawWireCube(roomList[i].center, roomList[i].size);
		}
	}

	public void Split()
	{
		SplitAllRooms();
	}

	private void SplitAllRooms()
	{
		Rect[] rooms = roomList.ToArray();
		for(int i = 0; i < rooms.Length; i++)
			SplitRoom(rooms[i], Cutout);
	}

	private void SplitRoom(Rect _Room, Rect _Cutout)
	{
		if(!roomList.Contains(_Room))
			return;

		if(!_Room.Overlaps(_Cutout))
			return;

		// Remove room from list
		roomList.Remove(_Room);

		// Create new rooms
		List<Rect> split = new List<Rect>();

		// Get cutout corners
		bool topLeft = _Room.Contains(new Vector2(_Cutout.xMin, _Cutout.yMax));
		bool topRight = _Room.Contains(new Vector2(_Cutout.xMax, _Cutout.yMax));
		bool bottomLeft = _Room.Contains(new Vector2(_Cutout.xMin, _Cutout.yMin));
		bool bottomRight = _Room.Contains(new Vector2(_Cutout.xMax, _Cutout.yMin));

		// Check overlap without corner contained within room
		if(!topLeft && !topRight && !bottomLeft && !bottomRight)
		{
			if(_Cutout.yMax < _Room.yMax)
			{
				split.Add(new Rect(_Room.xMin, _Cutout.yMax, _Room.xMax - _Room.xMin, _Room.yMax - _Cutout.yMax));
			}
			if(_Cutout.xMax < _Room.xMax)
			{
				split.Add(new Rect(_Cutout.xMax, _Room.yMin, _Room.xMax - _Cutout.xMax, _Room.yMax - _Room.yMin));
			}
			if(_Cutout.yMin > _Room.yMin)
			{
				split.Add(new Rect(_Room.xMin, _Room.yMin, _Room.xMax - _Room.xMin, _Cutout.yMin - _Room.yMin));
			}
			if(_Cutout.xMin > _Room.xMin)
			{
				split.Add(new Rect(_Room.xMin, _Room.yMin, _Cutout.xMin - _Room.xMin, _Room.yMax - _Room.yMin));
			}
		}
		else
		{
			// Check top
			if(_Room.yMax != _Cutout.yMax)
			{
				if(topLeft)
				{
					split.Add(new Rect(_Cutout.xMin, _Cutout.yMax, _Room.xMax - _Cutout.xMin, _Room.yMax - _Cutout.yMax));
				}
				else if(topRight)
				{
					split.Add(new Rect(_Room.xMin, _Cutout.yMax, _Room.xMax - _Room.xMin, _Room.yMax - _Cutout.yMax));
				}
			}

			// Check right
			if(_Room.xMax != _Cutout.xMax)
			{
				if(topRight)
				{
					split.Add(new Rect(_Cutout.xMax, _Room.yMin, _Room.xMax - _Cutout.xMax, _Cutout.yMax - _Room.yMin));
				}
				else if(bottomRight)
				{
					split.Add(new Rect(_Cutout.xMax, _Room.yMin, _Room.xMax - _Cutout.xMax, _Room.yMax - _Room.yMin));
				}
			}

			// Check bottom
			if(_Room.yMin != _Cutout.yMin)
			{
				if(bottomRight)
				{
					split.Add(new Rect(_Room.xMin, _Room.yMin, _Cutout.xMax - _Room.xMin, _Cutout.yMin - _Room.yMin));
				}
				else if(bottomLeft)
				{
					split.Add(new Rect(_Room.xMin, _Room.yMin, _Room.xMax - _Room.xMin, _Cutout.yMin - _Room.yMin));
				}
			}

			// Check left
			if(_Room.xMin != _Cutout.xMin)
			{
				if(bottomLeft)
				{
					split.Add(new Rect(_Room.xMin, _Cutout.yMin, _Cutout.xMin - _Room.xMin, _Room.yMax - _Cutout.yMin));
				}
				else if(topLeft)
				{
					split.Add(new Rect(_Room.xMin, _Room.yMin, _Cutout.xMin - _Room.xMin, _Room.yMax - _Room.yMin));
				}
			}
		}

		// Add new rooms to list
		roomList.AddRange(split);
	}
}