﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(RoomTemplate))]
public class RoomTemplateDrawer : PropertyDrawer 
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		if(property.isExpanded)
		{
			int length = System.Enum.GetNames(typeof(Furniture.ESize)).Length;
			float height = EditorGUIUtility.singleLineHeight * (length + 3);
			height += EditorGUIUtility.standardVerticalSpacing * (length + 2);
			return height;
		}
		else
			return EditorGUIUtility.singleLineHeight;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		SerializedProperty tag = property.FindPropertyRelative("Tag");

		position.height = EditorGUIUtility.singleLineHeight;
		property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, tag.stringValue);

		if(property.isExpanded)
		{
			EditorGUI.indentLevel++;

			position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
			tag.stringValue = EditorGUI.TextField(position, "Tag", tag.stringValue);

			position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
			EditorGUI.LabelField(position, "Amount of Furniture");

			EditorGUI.indentLevel++;

			string[] names = System.Enum.GetNames(typeof(Furniture.ESize));
			SerializedProperty array = property.FindPropertyRelative("Objects");
			if(array.arraySize < names.Length)
				array.arraySize = names.Length;
			
			SerializedProperty count;
			for(int i = 0; i < array.arraySize; i++)
			{
				position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
				count = array.GetArrayElementAtIndex(i);
				count.intValue = EditorGUI.IntField(position, names[i], count.intValue);
			}

			EditorGUI.indentLevel -= 2;
		}
	}
}