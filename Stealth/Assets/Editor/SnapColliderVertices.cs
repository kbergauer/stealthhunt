﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SnapColliderVertices : Editor 
{
	[MenuItem("CONTEXT/PolygonCollider2D/Snap")]
	static private void Snap(MenuCommand _Command)
	{
		PolygonCollider2D polygon = (PolygonCollider2D)_Command.context;

		Vector2[] points = polygon.points;
		for(int i = 0; i < points.Length; i++)
		{
			//Debug.Log(i + ": (" + points[i].x + ", " + points[i].y + ")");
			points[i] = new Vector2(Mathf.RoundToInt(points[i].x), Mathf.RoundToInt(points[i].y));
			//Debug.Log(i + ": (" + points[i].x + ", " + points[i].y + ")");
		}

		Undo.RecordObject(polygon, "Snap Vertices");
		polygon.points = points;
	}

	[MenuItem("CONTEXT/PolygonCollider2D/Squash Vertically", false)]
	static private void SquashVertically(MenuCommand _Command)
	{
		PolygonCollider2D polygon = (PolygonCollider2D)_Command.context;

		Vector2[] points = polygon.points;
		points[0].y = points[0].y - 0.4f;
		points[1].y = points[1].y + 0.4f;
		points[2].y = points[2].y + 0.4f;
		points[3].y = points[3].y - 0.4f;

		Undo.RecordObject(polygon, "Squash Vertically");
		polygon.points = points;
	}

	[MenuItem("CONTEXT/PolygonCollider2D/Squash Vertically", true)]
	static private bool HasOnlyFourVertices(MenuCommand _Command)
	{
		PolygonCollider2D polygon = (PolygonCollider2D)_Command.context;
		if(polygon.points.Length == 4)
			return true;

		return false;
	}
}