﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AIWorldStateInspector : EditorWindow 
{
	private AIWorldState state = null;
	private bool trailFoldout = false;
	private Vector2 trailScrolling = Vector2.zero;

	[MenuItem("Window/AI World State")]
	new static public void Show()
	{
		AIWorldStateInspector window = EditorWindow.GetWindow<AIWorldStateInspector>("AI World State");
		window.Init();
	}

	void Init()
	{
		state = AIWorldState.GetInstance();
	}

	void Update()
	{
		if(EditorApplication.isPlaying || EditorApplication.isPaused)
		{
			Repaint();
		}
	}

	void OnGUI()
	{
		if(EditorApplication.isPlaying || EditorApplication.isPaused)
		{
			if(state == null)
				state = AIWorldState.GetInstance();

			Trail[] trails = state.GetAllTrails();
			trailFoldout = EditorGUILayout.Foldout(trailFoldout, "Trails (" + (trails != null ? trails.Length.ToString() : "none") + ")");
			if(trailFoldout)
			{
				EditorGUI.indentLevel++;
				trailScrolling = EditorGUILayout.BeginScrollView(trailScrolling);

				for(int i = 0; i < trails.Length; i++)
				{
					EditorGUILayout.LabelField("Age: " + trails[i].GetAge());
					EditorGUILayout.LabelField("Length: " + trails[i].GetTrail().Count);
				}

				EditorGUILayout.EndScrollView();
				EditorGUI.indentLevel--;
			}
		}
		else
		{
			EditorGUILayout.LabelField("Only available in play mode.");
		}
	}
}