# README #

This project is a local multiplayer hide and seek game. Using a combination of utility items and a sparse attack ability, players have to try to stay alive while taking out their opponents in a randomly generated japanese mansion.

# COPYRIGHT #

The copyright lies with Korbinian Bergauer (http://kbergauer.de/). You may download, inspect and adjust the code base but NOT distribute it to anyone, paid or free, without my consent.